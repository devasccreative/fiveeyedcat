<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','user\HomeController@index');

Auth::routes();


//user's routes
Route::get('/home', 'user\HomeController@index')->name('user.home');
Route::get('/user/vfxtype/{id}', 'user\VFXTypeController@index')->name('user.vfxtype');
Route::get('/user/profile/{id}', 'user\HomeController@profile')->name('user.profile');
Route::post('/user/updateprofile/', 'user\HomeController@updateProfile')->name('user.profile.update');
Route::post('/user/delete', 'user\HomeController@deleteUser')->name('user.delete');
Route::post('/user/changepassword', 'user\HomeController@changePassword')->name('user.profile.changepassword');

Route::get('/user/vfxtypeform', 'user\VFXTypeController@vfxForm')->name('user.vfxtypeform');
Route::post('/user/vfxtypeform', 'user\VFXTypeController@addvfxForm')->name('user.vfxtypeform');
Route::post('/user/vfxgroup/autocomplete', 'user\VFXTypeController@getVFXGroup')->name('user.vfxgroup.autocomplete');

Route::post('/user/vfxtype/autocomplete', 'user\VFXTypeController@getVFX')->name('user.vfxtype.autocomplete');
Route::post('/user/screencredit/autocomplete', 'user\VFXTypeController@getCreditlist')->name('user.screencredit.autocomplete');
Route::post('/user/tag/autocomplete', 'user\VFXTypeController@getTag')->name('user.tag.autocomplete');
Route::post('/user/vfxtypelist', 'user\VFXTypeController@vfxList')->name('user.vfxtypelist');
Route::post('/user/savesearch','user\VFXTypeController@saveSearch')->name('user.savesearch');
Route::post('/user/savesearchresult','user\VFXTypeController@saveSearchResult')->name('user.savesearchresult');
Route::post('/user/contact/delete','user\VFXTypeController@deleteVfxContact')->name('user.contact.delete');

//Update Scene note
Route::post('/user/scenenote/update','user\VFXTypeController@updateSceneNote')->name('user.scenenote.update');
Route::post('/user/scenedid/update','user\VFXTypeController@updateSceneDid')->name('user.scenedid.update');


//user roles
Route::post('/user/rolelist','user\VFXTypeController@roleList')->name('user.rolelist');
Route::post('/roles','Auth\RegisterController@roleList')->name('register.rolelist');
Route::post('/user/scenerole/add','user\VFXTypeController@addSceneRole')->name('user.scenerole.add');

//tags routes
Route::post('/taglist','user\TagController@autoComplete')->name('user.taglist');
Route::post('/addtag','user\VFXTypeController@addTag')->name('user.addtag');

//save playlist
Route::post('/saveplaylist','user\VFXTypeController@savePlaylist')->name('user.saveplaylist');
 Route::post('/playlist/delete','user\PlayListController@deletePlaylist')->name('user.playlist.delete');
//playlist routes
Route::get('/playlist/', 'user\PlayListController@index')->name('user.playlist.index');
Route::post('/playlist/list','user\PlayListController@list')->name('user.playlist.list');
//vfx group's routes
Route::post('/vfxgroup','user\VFXTypeController@vfxGroup')->name('user.vfxgroup');


//vfx types routes
Route::post('/vfxslist/{id?}','user\VFXTypeController@vfxTypeList')->name('user.vfxslist');
Route::post('/projecttypelist','user\VFXTypeController@projectTypeList')->name('user.projecttypelist');
Route::post('/tagslist','user\VFXTypeController@tagsList')->name('user.tagslist');
Route::post('/scenetag/change','user\VFXTypeController@changeSceneTag')->name('user.scenetag.change');
Route::post('/scenetag/remove','user\VFXTypeController@removeSceneTag')->name('user.scenetag.remove');
Route::post('vfxgroup/focusout','user\VFXTypeController@focusoutVFXGroup')->name('user.vfxgroup.focusout');
Route::post('vfxtype/focusout','user\VFXTypeController@focusoutVFXType')->name('user.vfxtype.focusout');
Route::post('screencredit/focusout','user\VFXTypeController@focusoutScreenCredit')->name('user.screencredit.focusout');
Route::post('scenerole/change','user\VFXTypeController@ChangeSceneRole')->name('user.scenerole.change');
Route::post('projecttype/focusout','user\VFXTypeController@focusoutProjectType')->name('user.projecttype.focusout');
Route::post('project/focusout','user\VFXTypeController@focusoutProject')->name('user.project.focusout');
Route::post('year/focusout','user\VFXTypeController@focusoutYear')->name('user.year.focusout');
Route::post('link/focusout','user\VFXTypeController@focusoutLink')->name('user.link.focusout');
Route::post('rating/focusout','user\VFXTypeController@focusoutRating')->name('user.rating.focusout');
Route::post('/searchvfxtype','user\VFXTypeController@searchVFXType')->name('user.searchvfxtype');
Route::post('/searchvfxgroup','user\VFXTypeController@searchVFXGroup')->name('user.searchvfxgroup');
Route::post('/searchtag','user\VFXTypeController@searchTag')->name('user.searchtag');
//search routes
  Route::get('/search','user\VFXTypeController@viewSearch')->name('user.search'); 
  Route::post('/searchlist','user\VFXTypeController@searchList')->name('user.searchlist');
  
  Route::post('/deletesearch','user\VFXTypeController@deleteSearch')->name('user.deletesearch');
  Route::get('/searchresult/{id}','user\VFXTypeController@showSearch')->name('user.search.searchresult');
  Route::get('/playlist/{url}','user\MyPlaylistController@showPlaylist')->name('user.searchresult');
  Route::get('/myplaylist/','user\MyPlaylistController@myPlayList')->name('user.myplaylist');



  
//admin's routes
Route::get('/admin/', 'Auth\AdminController@showLoginForm');
Route::get('/admin/login', 'Auth\AdminController@showLoginForm');
Route::post('/admin/login','Auth\AdminController@login')->name('admin.login');
Route::post('/admin/logout','Auth\AdminController@logout')->name('admin.logout');
  Route::group(['prefix'=> 'admin', 'namespace'=> 'admin', 'middleware'=> 'auth:admin','as'=>'admin.'], function() {
  



//user's routes
    Route::get('user', 'UserController@index')->name('user');
    Route::post('userlist', 'UserController@getUserList')->name('userlist');
    Route::post('user/rolelist', 'UserController@roleList')->name('user.rolelist');

    Route::post('adduser','UserController@addUser')->name('user.add');
    Route::post('edituser','UserController@editUser')->name('user.edit');
    Route::post('edituserpassword','UserController@editUserPassword')->name('user.edituserpassword');
    Route::post('updateuser','UserController@updateUser')->name('user.update');
    Route::post('changeapproval', 'UserController@changeApproval')->name('changeapproval');
//vfx groups
    Route::get('vfxgrouptype', 'VFXtypeGroupController@index')->name('vfxgrouptype');
    Route::post('vfxgroup/list','VFXtypeGroupController@list')->name('vfxgroup.list');
    Route::post('vfxgroup/add','VFXtypeGroupController@add')->name('vfxgroup.add');
    Route::post('vfxgroup/delete','VFXtypeGroupController@delete')->name('vfxgroup.delete');
    Route::post('vfxgroup/edit','VFXtypeGroupController@edit')->name('vfxgroup.edit');
    Route::post('vfxgroup/update','VFXtypeGroupController@update')->name('vfxgroup.update');
    
//vfx type
    Route::get('vfxtype', 'VFXController@index')->name('vfxtype');
    Route::post('vfxtype/list','VFXController@list')->name('vfxtype.list');
    Route::post('vfxtype/add','VFXController@add')->name('vfxtype.add');
    Route::post('vfxtype/edit','VFXController@edit')->name('vfxtype.edit');
    Route::post('vfxtype/update','VFXController@update')->name('vfxtype.update');
    Route::post('vfxtype/delete','VFXController@delete')->name('vfxtype.delete');

    //vfx type
    Route::get('projecttype', 'ProjectTypeController@index')->name('projecttype');
    Route::post('projecttype/list','ProjectTypeController@list')->name('projecttype.list');
    Route::post('projecttype/add','ProjectTypeController@add')->name('projecttype.add');
    Route::post('projecttype/edit','ProjectTypeController@edit')->name('projecttype.edit');
    Route::post('projecttype/update','ProjectTypeController@update')->name('projecttype.update');
    Route::post('projecttype/delete','ProjectTypeController@delete')->name('projecttype.delete');

//tags
    Route::get('tag', 'TagController@index')->name('tag');
    Route::post('tag/list','TagController@list')->name('tag.list');
    Route::post('tag/add','TagController@add')->name('tag.add');
    Route::post('tag/edit','TagController@edit')->name('tag.edit');
    Route::post('tag/update','TagController@update')->name('tag.update');
    Route::post('tag/delete','TagController@delete')->name('tag.delete');
    

    Route::post('vfxgroup','VFXController@vfxGroup')->name('vfxtype.vfxgroup');

//admin role
  Route::get('role', 'RoleController@index')->name('role');  
  Route::post('role/list','RoleController@list')->name('role.list');
  Route::post('role/add','RoleController@add')->name('role.add');
  Route::post('role/delete','RoleController@delete')->name('role.delete');
  Route::post('role/edit','RoleController@edit')->name('role.edit');
  Route::post('role/update','RoleController@update')->name('role.update'); 
  });
