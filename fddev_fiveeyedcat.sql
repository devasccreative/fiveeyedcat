-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 13, 2020 at 04:40 AM
-- Server version: 5.7.29
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fddev_fiveeyedcat`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@fiveeyedcat.com', '$2y$10$RJ4f25bDrk20JRFNSh5vYuNlLXwFSNxZJYIWZcdvM70J.X3Lz0Ggy', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_12_23_061414_add_role_to_users', 1),
(5, '2019_12_23_095823_add_contact_permission_to_users', 2),
(6, '2019_12_24_061202_create_vfx_types_table', 3),
(7, '2019_12_24_095151_create_admins_table', 4),
(8, '2019_12_24_120022_add_approval_to_users', 5),
(9, '2019_12_26_053109_create_vfxs_table', 6),
(10, '2019_12_26_090621_create_tags_table', 7),
(11, '2019_12_26_090730_create_scene_tags_table', 7),
(12, '2019_12_26_091736_create_project_types_table', 8),
(13, '2019_12_26_104411_create_scene_images_table', 9),
(14, '2019_12_26_104909_add_project_type_id_and_vfx_type_id_to_vfx_types_table', 10),
(15, '2019_12_28_103129_create_save_searches_table', 11),
(16, '2019_12_28_111703_add_user_id_to_save_searches_table', 12),
(18, '2019_12_30_051937_create_vfx_groups_table', 13),
(19, '2019_12_30_052316_add_vfx_group_id_to_vfxs', 14),
(20, '2019_12_30_053711_add_user_id_to_vfx_types', 15),
(21, '2019_12_30_053843_add_vfx_group_id_to_vfx_types', 16),
(22, '2019_12_30_102048_add_vfx_group_to_save_searches', 17),
(23, '2019_12_31_072551_create_roles_table', 18),
(24, '2019_12_31_072624_create_scene_roles_table', 18),
(25, '2019_12_31_093812_add_scene_note_and_scene_did_to_vfx_types_table', 19),
(26, '2019_12_31_115857_create_scene_playlist_table', 20),
(27, '2020_01_02_121432_create_playlists_table', 21),
(28, '2020_01_03_091252_add_role_id_to_users', 22),
(29, '2020_01_07_102613_add_url_to_playlists_table', 23);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('karanharyani@yopmail.com', '$2y$10$ZvsaQ7ggGVrtvFPCwfBzJeaa4whfRTaxeFRI53el7eIEBkUEnPCAC', '2019-12-23 05:08:55'),
('john700@yopmail.com', '$2y$10$tM65DA2zhOwAOP3D7/2lHeO7wK3rc/vvUWCjQGWwmQGfEp1tE82IO', '2019-12-27 02:48:38'),
('kishan@yopmail.com', '$2y$10$cfxKjqllRqv3pkA4pbyyTeVuE1iDTlxERpEwpStfxCN0n62aGwl6u', '2020-01-01 06:25:35'),
('kishan123@yopmail.com', '$2y$10$ICJQ6dRLf8WD8cEJtNFXNOW0u8cYwyQzJmrF/GwRuca5dESVnELSO', '2020-01-06 05:26:53');

-- --------------------------------------------------------

--
-- Table structure for table `playlists`
--

CREATE TABLE `playlists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `scene_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `playlist` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `playlists`
--

INSERT INTO `playlists` (`id`, `scene_id`, `user_id`, `playlist`, `created_at`, `updated_at`, `url`) VALUES
(2, '18,19', 3, 'HasPlay', NULL, NULL, '010720201250065e147e7e08203HasPlay'),
(5, NULL, 7, 'Test', NULL, NULL, '011020200458595e1804938d78dTest'),
(6, '20', 7, 'myplayist', NULL, NULL, '011120200940095e1997f9027edmyplayist');

-- --------------------------------------------------------

--
-- Table structure for table `project_types`
--

CREATE TABLE `project_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_types`
--

INSERT INTO `project_types` (`id`, `type`, `created_at`, `updated_at`) VALUES
(1, 'TV Series', NULL, NULL),
(2, 'Mini Series', NULL, '2020-01-06 05:29:36'),
(3, 'Commercial', NULL, '2020-01-08 07:00:15'),
(4, 'Documentary', NULL, NULL),
(5, 'Ride Film', NULL, NULL),
(7, 'Feature', '2020-01-08 05:56:16', '2020-01-08 05:56:16'),
(8, 'Indie', '2020-01-08 05:56:29', '2020-01-08 05:56:29');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Director', NULL, NULL),
(2, 'Studio', NULL, NULL),
(3, 'VFX Supervisor', NULL, NULL),
(4, 'VFX Artist', NULL, NULL),
(6, 'VFX Co-Ordinator', '2020-01-03 07:17:05', '2020-01-03 07:17:05'),
(7, 'VFX Producer', '2020-01-03 07:51:35', '2020-01-03 07:51:35'),
(8, 'Comp Supervisor', '2020-01-03 07:51:43', '2020-01-03 07:51:43'),
(9, '3D Supervisor', '2020-01-03 07:51:52', '2020-01-03 07:51:52'),
(10, 'Lead 2D artist', '2020-01-03 07:52:01', '2020-01-03 07:52:01'),
(11, 'Lead 3D artist', '2020-01-03 07:52:09', '2020-01-03 07:52:09'),
(12, '2D Artist', '2020-01-03 07:52:18', '2020-01-03 07:52:18'),
(13, '3D Artist', '2020-01-03 07:52:24', '2020-01-03 07:52:24'),
(14, 'Layout', '2020-01-03 07:52:29', '2020-01-03 07:52:29'),
(15, 'Tracking', '2020-01-03 07:52:37', '2020-01-03 07:52:37'),
(16, 'Roto', '2020-01-03 07:52:50', '2020-01-03 07:52:50'),
(17, 'Plate Prep', '2020-01-03 07:52:59', '2020-01-03 07:52:59'),
(18, 'Production Designer', '2020-01-03 07:53:10', '2020-01-03 07:53:10'),
(19, 'Art Director', '2020-01-03 07:53:18', '2020-01-03 07:53:18'),
(20, 'Concept Artist', '2020-01-03 07:53:25', '2020-01-03 07:53:25'),
(21, 'Title Sequence Designer', '2020-01-03 07:53:31', '2020-01-03 07:53:31'),
(23, 'Producer', '2020-01-08 05:54:47', '2020-01-08 05:54:47');

-- --------------------------------------------------------

--
-- Table structure for table `save_searches`
--

CREATE TABLE `save_searches` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `search_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vfx_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `vfx_group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `save_searches`
--

INSERT INTO `save_searches` (`id`, `search_name`, `vfx_type`, `tags`, `created_at`, `updated_at`, `user_id`, `vfx_group`) VALUES
(11, 'Blood Search', NULL, 'Day', '2019-12-31 13:06:37', '2020-01-11 09:41:51', 7, 'blood');

-- --------------------------------------------------------

--
-- Table structure for table `scene_images`
--

CREATE TABLE `scene_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `scene_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scene_images`
--

INSERT INTO `scene_images` (`id`, `scene_id`, `image`, `created_at`, `updated_at`) VALUES
(22, 17, 'Chrysanthemum.jpg', NULL, NULL),
(23, 17, 'Desert.jpg', NULL, NULL),
(24, 18, 'Chrysanthemum.jpg', NULL, NULL),
(25, 18, 'Desert.jpg', NULL, NULL),
(26, 19, 'Chrysanthemum.jpg', NULL, NULL),
(27, 19, 'Desert.jpg', NULL, NULL),
(28, 20, 'Chrysanthemum - Copy - Copy.jpg', NULL, NULL),
(29, 20, 'Hydrangeas.jpg', NULL, NULL),
(30, 20, 'Tulips - Copy.jpg', NULL, NULL),
(31, 22, 'Hydrangeas.jpg', NULL, NULL),
(32, 22, 'Jellyfish.jpg', NULL, NULL),
(33, 22, 'Tulips - Copy.jpg', NULL, NULL),
(34, 24, 'Desert.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `scene_playlist`
--

CREATE TABLE `scene_playlist` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `scene_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `scene_roles`
--

CREATE TABLE `scene_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `screen_credit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scene_id` bigint(20) UNSIGNED DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scene_roles`
--

INSERT INTO `scene_roles` (`id`, `screen_credit`, `scene_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, '25', 13, 3, NULL, NULL),
(2, '85', 13, 4, NULL, NULL),
(3, '95', 13, 2, NULL, NULL),
(4, '45', 13, 4, NULL, NULL),
(5, '85', 13, 2, NULL, NULL),
(6, '65', 13, 4, NULL, NULL),
(7, '65', 13, 3, NULL, NULL),
(8, '5', 13, 3, NULL, NULL),
(9, '4', 13, 4, NULL, NULL),
(10, '4.5', 15, 3, NULL, NULL),
(11, '3.5', 15, 3, NULL, NULL),
(12, 'aas', 17, 4, NULL, NULL),
(13, 's', 17, NULL, NULL, '2020-01-06 05:39:28'),
(14, '12', 17, 3, NULL, NULL),
(15, '163', 17, 1, NULL, NULL),
(16, 'John Gajdecki', 17, 3, NULL, NULL),
(17, 'Tim Bramwell', 17, 1, NULL, NULL),
(18, 'Co ordiner', 17, 6, NULL, '2020-01-06 05:39:47'),
(19, 'Tim Bramwell', 17, 1, NULL, NULL),
(22, NULL, 17, 6, NULL, NULL),
(24, 'john doe', 20, 6, NULL, '2020-01-11 09:39:05'),
(25, 'john', 22, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `scene_tags`
--

CREATE TABLE `scene_tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tag_id` bigint(20) UNSIGNED DEFAULT NULL,
  `scene_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scene_tags`
--

INSERT INTO `scene_tags` (`id`, `tag_id`, `scene_id`, `created_at`, `updated_at`) VALUES
(14, 5, 13, NULL, NULL),
(15, 4, 14, NULL, NULL),
(16, 6, 15, NULL, NULL),
(17, 2, 16, NULL, NULL),
(18, 5, 16, NULL, NULL),
(19, 4, 16, NULL, NULL),
(20, 1, 17, NULL, NULL),
(21, 3, 17, NULL, NULL),
(22, 3, 18, NULL, NULL),
(23, 5, 18, NULL, NULL),
(24, 3, 19, NULL, NULL),
(25, 5, 19, NULL, NULL),
(26, 3, 20, NULL, NULL),
(28, 3, 21, NULL, NULL),
(29, 48, 21, NULL, NULL),
(30, 2, 20, '2020-01-08 06:50:01', '2020-01-08 06:50:01'),
(33, 3, 22, NULL, NULL),
(34, 2, 22, NULL, NULL),
(35, 3, 23, NULL, NULL),
(36, 2, 23, NULL, NULL),
(37, 6, 24, NULL, NULL),
(38, 7, 24, NULL, NULL),
(39, 9, 24, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `tag`, `created_at`, `updated_at`) VALUES
(1, 'Day', NULL, NULL),
(2, '2D Animation', NULL, '2020-01-08 08:46:30'),
(3, 'Sunrise', NULL, NULL),
(5, 'Alternate Versions', NULL, NULL),
(6, 'Artist\'s Favorite', NULL, NULL),
(7, 'All CG', NULL, NULL),
(8, 'Miniature', NULL, NULL),
(9, 'Motion Control', NULL, NULL),
(10, 'Guy in a Suit', NULL, '2020-01-06 05:21:03'),
(11, 'Optical Compositing', NULL, NULL),
(12, '2D Animation', NULL, NULL),
(13, 'Too Long', NULL, NULL),
(37, 'No Time', '2019-12-30 02:51:30', '2019-12-30 02:51:30'),
(49, 'Sunrise', '2020-01-08 08:46:27', '2020-01-08 08:46:27'),
(50, 'Artist\'s Favorite', '2020-01-08 18:39:17', '2020-01-08 18:39:17'),
(51, 'All CG', '2020-01-08 18:39:20', '2020-01-08 18:39:20'),
(52, 'Motion Control', '2020-01-08 18:39:36', '2020-01-08 18:39:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `referrer_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `short_bio` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_permission` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `approval` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `referrer_name`, `user_name`, `role`, `phone`, `role_id`, `short_bio`, `contact_permission`, `approval`) VALUES
(1, 'dev', 'developer.asccreative@gmail.com', NULL, '$2y$10$omKOuq5l80ifjjQMnhcruek.N.8tV01mCyUs9SQI4uQ6sIDutGI0m', 'uOb1Tcv2yFP2Oaq0sErLCoLDkkvenZxmFehuNdKPhP789I32OS5brGHXJsHu', '2019-12-31 12:09:20', '2019-12-31 15:21:45', NULL, 'asccreative', 'Manager', '7887841545', NULL, 'Short Bio', 'yes', 'yes'),
(2, 'kishan', 'kishan@yopmail.com', NULL, '$2y$10$RJ4f25bDrk20JRFNSh5vYuNlLXwFSNxZJYIWZcdvM70J.X3Lz0Ggy', NULL, '2019-12-31 12:30:09', '2019-12-31 12:42:19', NULL, 'kishan', 'Role', '778878778', NULL, 'Short Bio', 'yes', 'yes'),
(3, 'kishan123', 'kishan123@yopmail.com', NULL, '$2y$10$CCT//Ql4.dp0L9bgCZH1peyZaCpvZ1Tj9PnsEuIV4LZ6ZREAtMRMW', NULL, '2019-12-31 12:43:21', '2019-12-31 12:51:00', NULL, 'kishan123', 'Rolw', '123456', NULL, 'dahdasl', 'yes', 'yes'),
(4, '456', 'kishan456@yopmail.com', NULL, '$2y$10$6n0sobM2lIiIOQ4D4PbeuOvgqgGdcXnnEJc/vNkI4GA2x7UXiorYq', NULL, '2020-01-01 06:28:14', '2020-01-06 05:25:48', NULL, 'kishan456', NULL, '456456456', 2, '456456', 'yes', 'no'),
(5, 'Tim Bramwell', 'tim@asccreative.com', NULL, 'LZQPaqQSq0', NULL, '2020-01-03 07:19:37', '2020-01-03 07:19:46', NULL, 'tim.bramwell', 'Producer', '604 771 5001', NULL, NULL, 'no', 'yes'),
(6, 'john', 'john11@gmail.com', NULL, '$2y$10$erZCO1w5xLMs9f3vRkJ22eNoM9/Wm/6pH/nfQsKh4VqYytDrWAVFW', NULL, '2020-01-08 06:06:57', '2020-01-08 06:07:42', 'jordan', 'john11', NULL, '123456789', 3, 'hii this is a jordan', 'yes', 'yes'),
(7, 'karan', 'karan@yopmail.com', NULL, '$2y$10$BwRNSmXNe6PfypFXhwL5AeaaY4k0wbqn7KhUS9LLZUp/jpVlFCyHa', NULL, '2020-01-08 06:14:13', '2020-01-09 05:17:18', 'karan', 'karan', NULL, '123456789', 1, 'hii here is a karan12', 'yes', 'yes'),
(8, 'Developer', 'developer.foremost@gmail.com', NULL, '$2y$10$lPJSSsvqcLtchv27D5jaROuqSLvxtCW9zLmX6lzA4QXqgj00Z9Ol6', NULL, '2020-01-11 07:07:06', '2020-01-11 07:12:59', 'kishan', 'developer', NULL, '9724673156', 21, 'developer', 'yes', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `vfxs`
--

CREATE TABLE `vfxs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `vfx_group_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vfxs`
--

INSERT INTO `vfxs` (`id`, `type`, `created_at`, `updated_at`, `vfx_group_id`) VALUES
(6, 'Walking Around', '2020-01-06 20:47:17', '2020-01-06 20:47:17', 5),
(7, 'Fighting', '2020-01-06 20:47:33', '2020-01-06 20:47:33', 5),
(8, 'Aging', '2020-01-06 20:47:46', '2020-01-06 20:47:46', 5),
(9, 'De-Aging', '2020-01-06 20:48:05', '2020-01-06 20:48:05', 5),
(10, 'Dancing', '2020-01-06 20:48:17', '2020-01-06 20:48:17', 5),
(11, 'Acting / Performance', '2020-01-06 20:48:42', '2020-01-06 20:48:42', 5),
(12, 'Head Shot', '2020-01-06 20:49:06', '2020-01-06 20:49:06', 1),
(13, 'Wounds', '2020-01-06 20:49:51', '2020-01-06 20:49:51', 1),
(14, 'Body part re-attached', '2020-01-06 20:50:20', '2020-01-06 20:50:20', 1),
(15, 'Bullet Impacts', '2020-01-06 20:50:32', '2020-01-06 20:50:32', 1),
(16, 'Surgery', '2020-01-06 20:50:50', '2020-01-06 20:50:50', 1),
(17, 'Ships', '2020-01-06 20:51:14', '2020-01-06 20:51:14', 14),
(18, 'Submarine', '2020-01-06 20:52:24', '2020-01-06 20:52:24', 14),
(19, 'Sailing Ships', '2020-01-06 20:52:44', '2020-01-06 20:52:44', 14),
(20, 'Icebreakers', '2020-01-06 20:52:58', '2020-01-06 20:52:58', 14),
(21, 'Rowboats', '2020-01-06 20:53:23', '2020-01-06 20:53:23', 14),
(22, 'Apollo', '2020-01-06 21:01:58', '2020-01-06 21:01:58', 16),
(23, 'Re-entry', '2020-01-06 21:02:19', '2020-01-08 18:16:33', 16),
(24, 'UFO', '2020-01-06 21:02:30', '2020-01-06 21:02:30', 16),
(25, 'Film Grain', '2020-01-06 21:03:28', '2020-01-06 21:03:28', 4),
(26, 'Depth of Field', '2020-01-06 21:03:44', '2020-01-06 21:03:44', 4),
(27, 'Window Exposures', '2020-01-06 21:04:03', '2020-01-06 21:04:03', 4),
(28, 'Camera tests', '2020-01-06 21:04:24', '2020-01-06 21:04:24', 4),
(29, 'Lens Flares', '2020-01-06 21:04:38', '2020-01-06 21:04:38', 4),
(30, 'Over Exposure', '2020-01-06 21:04:49', '2020-01-06 21:04:49', 4),
(31, 'Slow Motion', '2020-01-06 21:05:11', '2020-01-06 21:05:11', 4),
(32, 'Time Lapse', '2020-01-06 21:05:22', '2020-01-06 21:05:22', 4),
(33, 'Film Look', '2020-01-06 21:05:35', '2020-01-06 21:05:35', 4),
(34, 'Window Brightness', '2020-01-06 21:05:50', '2020-01-06 21:05:50', 4),
(35, 'Bokeh', '2020-01-06 21:06:04', '2020-01-06 21:06:04', 4),
(36, 'Beautiful Shots', '2020-01-06 21:09:24', '2020-01-06 21:09:24', 13),
(37, 'Beautiful Lighting', '2020-01-06 21:18:32', '2020-01-06 21:18:32', 13),
(38, 'Production Fixes, Best of.., Paint Outs', '2020-01-06 21:19:12', '2020-01-06 21:19:12', 13),
(39, 'Wilhem Scream', '2020-01-06 21:19:27', '2020-01-06 21:19:27', 13),
(40, 'Powers of 10', '2020-01-06 21:19:47', '2020-01-06 21:19:47', 13),
(41, 'Title Sequence', '2020-01-06 21:20:08', '2020-01-06 21:20:08', 13),
(42, 'Flash Backs', '2020-01-06 21:20:24', '2020-01-06 21:20:24', 13),
(43, 'Flash Forward', '2020-01-06 21:20:40', '2020-01-06 21:20:40', 13),
(44, 'White Out', '2020-01-06 21:20:57', '2020-01-06 21:20:57', 13),
(45, 'Motion Graphics', '2020-01-06 21:21:17', '2020-01-06 21:21:17', 13),
(46, 'Explosions', '2020-01-06 21:28:08', '2020-01-06 21:28:08', 17),
(47, 'Nuclear Bombs', '2020-01-06 21:28:24', '2020-01-06 21:28:24', 17),
(48, 'Muzzle Flashes', '2020-01-06 21:28:40', '2020-01-06 21:28:40', 17),
(49, 'Impacts', '2020-01-06 21:28:56', '2020-01-06 21:28:56', 17),
(50, 'Pedestrian Impact', '2020-01-06 21:29:10', '2020-01-06 21:29:10', 17),
(51, 'Tank Firing', '2020-01-06 21:29:21', '2020-01-06 21:29:21', 17),
(52, 'Missiles', '2020-01-06 21:29:35', '2020-01-06 21:29:35', 17),
(53, 'Planets', '2020-01-06 21:30:38', '2020-01-06 21:30:38', 18),
(54, 'Sun', '2020-01-06 21:30:52', '2020-01-06 21:30:52', 18),
(55, 'Space', '2020-01-06 21:34:38', '2020-01-06 21:34:38', 18),
(56, 'Space Storm', '2020-01-06 21:35:04', '2020-01-06 21:35:04', 18),
(57, 'Stars - from space', '2020-01-06 21:35:23', '2020-01-06 21:35:23', 18),
(58, 'Planetarium', '2020-01-06 21:48:35', '2020-01-06 21:48:35', 18),
(59, 'Warp Speed', '2020-01-06 21:48:57', '2020-01-06 21:48:57', 18),
(60, 'monitor graphics', '2020-01-07 13:12:52', '2020-01-07 13:12:52', 7),
(61, 'Futuristic graphics', '2020-01-07 13:13:10', '2020-01-07 13:13:10', 7),
(62, 'Radar', '2020-01-07 13:13:23', '2020-01-07 13:13:23', 7),
(63, 'future binoculars', '2020-01-07 13:13:53', '2020-01-07 13:13:53', 7),
(64, 'HUD', '2020-01-07 13:14:04', '2020-01-07 13:14:04', 7),
(65, 'Theodolite', '2020-01-07 13:14:15', '2020-01-07 13:14:15', 7),
(66, 'Greenscreen', '2020-01-07 13:15:01', '2020-01-07 13:15:01', 19),
(67, 'LED Wall', '2020-01-07 13:15:14', '2020-01-07 13:15:14', 19),
(68, 'Rear Screen', '2020-01-07 13:15:25', '2020-01-07 13:15:25', 19),
(69, 'crazy roto', '2020-01-07 13:15:37', '2020-01-07 13:15:37', 19),
(70, 'Not VFX', '2020-01-07 13:15:48', '2020-01-07 13:15:48', 19),
(71, 'Ghosts', '2020-01-07 13:16:20', '2020-01-07 13:16:20', 15),
(72, 'Apperitions', '2020-01-07 13:16:31', '2020-01-07 13:16:31', 15),
(73, 'Vampire Transition', '2020-01-07 13:16:40', '2020-01-07 13:16:40', 15),
(74, 'Ectoplasm', '2020-01-07 13:16:53', '2020-01-07 13:16:53', 15),
(75, 'Objects come to life', '2020-01-07 13:17:03', '2020-01-07 13:17:03', 15),
(76, 'DMP', '2020-01-07 13:17:20', '2020-01-07 13:17:20', 6),
(77, 'City', '2020-01-07 13:17:33', '2020-01-07 13:17:33', 6),
(78, 'Futuristic City', '2020-01-07 13:17:45', '2020-01-07 13:17:45', 6),
(79, 'Set Extention', '2020-01-07 13:18:00', '2020-01-08 18:17:28', 6),
(80, 'period', '2020-01-07 13:18:10', '2020-01-07 13:18:10', 6),
(81, 'waves', '2020-01-07 13:18:22', '2020-01-07 13:18:22', 6),
(82, 'trees', '2020-01-07 13:18:34', '2020-01-07 13:18:34', 6),
(83, 'Aliens', '2020-01-07 13:19:22', '2020-01-07 13:19:22', 2),
(84, 'Dogs', '2020-01-07 13:19:34', '2020-01-07 13:19:34', 2),
(85, 'Cats', '2020-01-07 13:19:46', '2020-01-07 13:19:46', 2),
(86, 'Birds', '2020-01-07 13:19:57', '2020-01-07 13:19:57', 2),
(87, 'dinasours', '2020-01-07 13:20:08', '2020-01-07 13:20:08', 2),
(88, 'Talking Animals', '2020-01-07 13:20:20', '2020-01-07 13:20:20', 2),
(89, 'fish', '2020-01-07 13:20:46', '2020-01-07 13:20:46', 2),
(90, 'undersea mammals', '2020-01-07 13:20:58', '2020-01-07 13:20:58', 2),
(91, 'Early Flight', '2020-01-07 13:21:25', '2020-01-07 13:21:25', 3),
(92, 'WW1', '2020-01-07 13:21:39', '2020-01-07 13:21:39', 3),
(93, 'WW2', '2020-01-07 13:21:52', '2020-01-07 13:21:52', 3),
(94, 'Airliners', '2020-01-07 13:22:10', '2020-01-07 13:22:10', 3),
(95, 'helicopters', '2020-01-07 13:22:19', '2020-01-07 13:22:19', 3),
(96, 'high speed', '2020-01-07 13:22:33', '2020-01-07 13:22:33', 3),
(97, 'passenger aircraft', '2020-01-07 13:22:44', '2020-01-07 13:22:44', 3),
(98, 'dog fights', '2020-01-07 13:23:02', '2020-01-07 13:23:02', 3),
(99, 'Storms', '2020-01-07 13:24:11', '2020-01-07 13:24:11', 9),
(100, 'Sheet Lightning', '2020-01-07 13:24:26', '2020-01-07 13:24:26', 9),
(101, 'Rogue Waves', '2020-01-07 13:24:39', '2020-01-07 13:24:39', 9),
(102, 'Earthquake', '2020-01-07 13:24:53', '2020-01-07 13:24:53', 9),
(103, 'Volcano', '2020-01-07 13:25:08', '2020-01-07 13:25:08', 9),
(104, 'Falling Ash', '2020-01-07 13:25:24', '2020-01-07 13:25:24', 9),
(105, 'Embers', '2020-01-07 13:25:46', '2020-01-07 13:25:46', 9),
(106, 'Binocular', '2020-01-07 13:26:04', '2020-01-07 13:26:04', 10),
(107, 'Telescope', '2020-01-07 13:26:17', '2020-01-07 13:26:17', 10),
(108, 'Sniper Scope', '2020-01-07 13:26:30', '2020-01-07 13:26:30', 10),
(109, 'Vampire Vision', '2020-01-07 13:26:45', '2020-01-07 13:26:45', 10),
(110, 'Crazy Vision', '2020-01-07 13:26:56', '2020-01-07 13:26:56', 10),
(111, 'Drugged Vision', '2020-01-07 13:27:08', '2020-01-07 13:27:08', 10),
(112, 'Energy Blasts', '2020-01-07 13:27:31', '2020-01-07 13:27:31', 11),
(113, 'Beams of Death', '2020-01-07 13:27:41', '2020-01-07 13:27:41', 11),
(114, 'TractorBeams', '2020-01-07 13:28:00', '2020-01-07 13:28:00', 11),
(115, 'Laser Blasts', '2020-01-07 13:28:14', '2020-01-07 13:28:14', 11),
(116, 'Forcefields', '2020-01-07 13:28:33', '2020-01-07 13:28:33', 11),
(117, 'Arcing', '2020-01-07 13:28:44', '2020-01-07 13:28:44', 11),
(118, 'Time Warps', '2020-01-07 13:28:58', '2020-01-07 13:28:58', 11),
(119, 'Flying Cars', '2020-01-07 13:29:09', '2020-01-07 13:29:09', 11),
(120, 'Future Cities', '2020-01-07 13:29:21', '2020-01-07 13:29:21', 11),
(121, 'Early Vehicles', '2020-01-07 13:29:43', '2020-01-07 13:29:43', 8),
(122, 'cars', '2020-01-07 13:29:54', '2020-01-07 13:29:54', 8),
(123, 'car chase', '2020-01-07 13:30:12', '2020-01-07 13:30:12', 8),
(124, 'vehicle flip', '2020-01-07 13:30:23', '2020-01-07 13:30:23', 8),
(125, 'Vehicle Beauty Shot', '2020-01-07 13:30:34', '2020-01-07 13:30:34', 8),
(126, 'Tanks', '2020-01-07 13:30:45', '2020-01-07 13:30:45', 8),
(127, 'starfields from earth', '2020-01-07 13:31:02', '2020-01-07 13:31:02', 12),
(128, 'Aurora Borialis', '2020-01-07 13:31:40', '2020-01-07 13:31:40', 12),
(129, 'Night Sky', '2020-01-07 13:31:52', '2020-01-07 13:31:52', 12),
(130, 'Night CLouds', '2020-01-07 13:32:04', '2020-01-07 13:32:04', 12),
(131, 'Beautiful Skies', '2020-01-07 13:32:16', '2020-01-10 19:04:49', 12),
(132, 'Lightning in Sky', '2020-01-07 13:32:41', '2020-01-08 18:16:09', 12),
(133, 'rain', '2020-01-07 13:32:53', '2020-01-07 13:32:53', 12),
(134, 'snow', '2020-01-07 13:33:06', '2020-01-07 13:33:06', 12),
(135, 'fog', '2020-01-07 13:33:20', '2020-01-07 13:33:20', 12),
(136, 'Head Chopper Of', '2020-01-08 04:51:18', '2020-01-08 04:51:18', 1),
(137, 'Sonar', '2020-01-08 05:03:09', '2020-01-08 05:03:09', 7);

-- --------------------------------------------------------

--
-- Table structure for table `vfx_groups`
--

CREATE TABLE `vfx_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vfx_groups`
--

INSERT INTO `vfx_groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Blood and Gore', NULL, NULL),
(2, 'Creatures', NULL, '2020-01-08 06:49:55'),
(3, 'Aircraft', '2020-01-03 01:03:09', '2020-01-03 01:03:09'),
(4, 'Camera Artifacts', '2020-01-03 01:03:37', '2020-01-03 01:03:37'),
(5, 'Digi Doubles', '2020-01-03 01:03:47', '2020-01-03 01:03:47'),
(6, 'Environments', '2020-01-03 01:04:02', '2020-01-10 19:04:47'),
(7, 'Graphics', '2020-01-03 01:04:10', '2020-01-03 01:04:10'),
(8, 'Ground Vehicles', '2020-01-03 01:04:26', '2020-01-03 01:04:26'),
(9, 'Natural Disasters', '2020-01-03 01:04:36', '2020-01-03 01:04:36'),
(10, 'POV', '2020-01-03 01:04:42', '2020-01-03 01:04:42'),
(11, 'Sci Fi', '2020-01-03 01:04:51', '2020-01-03 01:04:51'),
(12, 'Skies', '2020-01-03 01:05:01', '2020-01-03 01:05:01'),
(13, 'Style', '2020-01-03 01:05:09', '2020-01-03 01:05:09'),
(14, 'Ships', '2020-01-03 01:05:16', '2020-01-03 01:05:16'),
(15, 'Supernatural', '2020-01-03 01:05:26', '2020-01-03 01:05:26'),
(16, 'Space Vehicles', '2020-01-03 01:05:36', '2020-01-03 01:05:36'),
(17, 'Weapons', '2020-01-03 01:05:47', '2020-01-03 01:05:47'),
(18, 'Space', '2020-01-06 21:30:12', '2020-01-06 21:30:12'),
(19, 'Comps', '2020-01-07 13:14:45', '2020-01-07 13:14:45');

-- --------------------------------------------------------

--
-- Table structure for table `vfx_types`
--

CREATE TABLE `vfx_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vfx_supe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scene_did` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scene_note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `vfx_group_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `project_type_id` bigint(20) UNSIGNED DEFAULT NULL,
  `vfx_type_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vfx_types`
--

INSERT INTO `vfx_types` (`id`, `project`, `year`, `vfx_supe`, `rating`, `scene_did`, `scene_note`, `link`, `user_id`, `vfx_group_id`, `created_at`, `updated_at`, `project_type_id`, `vfx_type_id`) VALUES
(17, 'Projject', '2015', 'a', '7.5', 'Three people in a small room.', 'Alien landing zone', 'http://www.yopmail.com/en/', 3, 1, '2019-12-31 12:56:38', '2020-01-06 05:39:55', 2, 3),
(18, 'Tim\'s test Project', '2019', 'John G', '1', NULL, NULL, 'https://test.me', 1, 1, '2020-01-03 02:00:17', '2020-01-06 21:55:27', 3, 3),
(19, 'Tim\'s test Project', '2019', 'John G', '1', NULL, NULL, 'https://test.me', 1, 1, '2020-01-03 02:00:19', '2020-01-03 02:00:19', 3, 3),
(20, 'myproject12', '2025', 'sup', '2.5', 'hii123', 'test12', 'https://hiimyvideolink12', 7, 2, '2020-01-08 06:48:57', '2020-01-08 07:00:13', 3, 84),
(21, 'myproject', '2020', 'sup', '2.0', NULL, NULL, 'https://hiimyvideolink', 7, 2, '2020-01-08 06:48:58', '2020-01-08 06:48:58', 3, 84),
(22, 'project', '2020', 'supe', '2.0', NULL, NULL, 'https://khlink', 7, 16, '2020-01-08 08:46:56', '2020-01-08 08:46:56', 2, 24),
(23, 'project', '2020', 'supe', '2.0', NULL, NULL, 'https://khlink', 7, 16, '2020-01-08 08:46:58', '2020-01-08 08:46:58', 2, 24),
(24, 'Stargate Atlantis', '1999', 'JJ Bean', '6.5', 'This is how we did it', 'This is Tim\'s test', 'https://https://grab.me', 1, 6, '2020-01-08 18:41:33', '2020-01-10 19:04:31', 1, 131);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `playlists`
--
ALTER TABLE `playlists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playlists_user_id_foreign` (`user_id`);

--
-- Indexes for table `project_types`
--
ALTER TABLE `project_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `save_searches`
--
ALTER TABLE `save_searches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `save_searches_user_id_foreign` (`user_id`);

--
-- Indexes for table `scene_images`
--
ALTER TABLE `scene_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `scene_images_scene_id_foreign` (`scene_id`);

--
-- Indexes for table `scene_playlist`
--
ALTER TABLE `scene_playlist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `scene_playlist_scene_id_foreign` (`scene_id`);

--
-- Indexes for table `scene_roles`
--
ALTER TABLE `scene_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `scene_roles_scene_id_foreign` (`scene_id`),
  ADD KEY `scene_roles_role_id_foreign` (`role_id`);

--
-- Indexes for table `scene_tags`
--
ALTER TABLE `scene_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `scene_tags_tag_id_foreign` (`tag_id`),
  ADD KEY `scene_tags_scene_id_foreign` (`scene_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `vfxs`
--
ALTER TABLE `vfxs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vfxs_vfx_group_id_foreign` (`vfx_group_id`);

--
-- Indexes for table `vfx_groups`
--
ALTER TABLE `vfx_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vfx_types`
--
ALTER TABLE `vfx_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vfx_types_project_type_id_foreign` (`project_type_id`),
  ADD KEY `vfx_types_vfx_type_id_foreign` (`vfx_type_id`),
  ADD KEY `vfx_types_user_id_foreign` (`user_id`),
  ADD KEY `vfx_types_vfx_group_id_foreign` (`vfx_group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `playlists`
--
ALTER TABLE `playlists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `project_types`
--
ALTER TABLE `project_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `save_searches`
--
ALTER TABLE `save_searches`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `scene_images`
--
ALTER TABLE `scene_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `scene_playlist`
--
ALTER TABLE `scene_playlist`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scene_roles`
--
ALTER TABLE `scene_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `scene_tags`
--
ALTER TABLE `scene_tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `vfxs`
--
ALTER TABLE `vfxs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT for table `vfx_groups`
--
ALTER TABLE `vfx_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `vfx_types`
--
ALTER TABLE `vfx_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `playlists`
--
ALTER TABLE `playlists`
  ADD CONSTRAINT `playlists_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `scene_images`
--
ALTER TABLE `scene_images`
  ADD CONSTRAINT `scene_images_scene_id_foreign` FOREIGN KEY (`scene_id`) REFERENCES `vfx_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
