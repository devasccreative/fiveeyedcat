<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaveSearch extends Model
{
    	protected $table ='save_searches';

        protected $fillable = [
            'search_name','vfx_type','tags','user_id','vfx_group'
        ];

        public function user(){
            return  $this->belongsTo('App\User','user_id');
        }
}
