<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SceneTag extends Model
{
    	protected $table ='scene_tags';

        protected $fillable = [
            'scene_id','tag_id'
        ];

        public function tagInfo(){
            return  $this->belongsTo('App\Tag','tag_id');
        }
}
