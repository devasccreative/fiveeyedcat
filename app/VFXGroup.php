<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VFXGroup extends Model
{
    	protected $table ='vfx_groups';

        protected $fillable = [
            'name'
        ];
}
