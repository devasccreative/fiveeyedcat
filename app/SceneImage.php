<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SceneImage extends Model
{
    	protected $table ='scene_images';

        protected $fillable = [
            'scene_id','image'
        ];

        public function getImageAttribute($value)
        {   
            if(file_exists(storage_path('app/public/vfxscene/1/compressedlogo/'.$value)) && $value != ''){
                return asset('public/storage/vfxscene/1/compressedlogo/').'/'.$value;     
            }else{
               return asset('public/storage/vfxscene/noimage.png');
           }
        }


        
}
