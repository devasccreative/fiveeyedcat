<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SceneRole extends Model
{
    	protected $table ='scene_roles';

        protected $fillable = [
            'scene_id','screen_credit','role_id'
        ];

        public function roleInfo(){
            return  $this->belongsTo('App\Role','role_id');
        }

}
