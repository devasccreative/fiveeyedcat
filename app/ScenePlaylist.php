<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScenePlaylist extends Model
{
    	protected $table ='scene_playlist';

        protected $fillable = [
            'scene_id','name'
        ];
}
