<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
class AdminController extends Controller
{
   /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
      */
      use AuthenticatesUsers;

      protected $guard = 'admin';

      /**

       * Where to redirect users after login.

       *

       * @var string

       */

    //  protected $redirectTo = '/user';


      /**

         * Create a new controller instance.

         *

         * @return void

         */

        public function __construct()
        {
          // $this->auth()->guard('admin');
        }

        

         public function showLoginForm()
         {    

               return view('admin.login');
         }

         public function login(Request $request)
            {
          
                if (auth()->guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
                    return redirect()->route('admin.user');
                }

                return back()->withErrors(['email' => 'Email or password are wrong.']);
            }

       
                protected function guard()
    {
        return auth()->guard('admin');
    }
            /**
         * Log the user out of the application.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function logout(Request $request)
        {
            $this->guard()->logout();
            $request->session()->invalidate();
            return redirect()->route('admin.login');
        }    

}
