<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


   protected $redirectTo = '/home';

   public function authenticate(Request $request)
   {
      // $credentials = $request->only('email', 'password');

        $login_type = filter_var($request->input('user_name'), FILTER_VALIDATE_EMAIL ) 
          ? 'email' 
          : 'user_name';
        //  return $login_type;
       if (Auth::attempt([$login_type => $request->input('user_name'), 'password' => $request->password, 'approval' => 'yes'])) {
                        
       }
       else
       {
              
       }
       
       
   }

   public function login(Request $request)
   {
   // $this->validateLogin($request);
    $login_type = filter_var($request->input('user_name'), FILTER_VALIDATE_EMAIL ) 
      ? 'email' 
      : 'user_name';
    $user = User::where($login_type,$request->user_name)->first();
    
    if(isset($user) && $user->approval == 'no'){
      return redirect()->back()->withErrors(['approval' => 'Your account is under review once approved then you will login with  your credentials.']);
    }
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
    if (method_exists($this, 'hasTooManyLoginAttempts') &&
      $this->hasTooManyLoginAttempts($request)) {
      $this->fireLockoutEvent($request);

      return $this->sendLockoutResponse($request);
    }

  if ($this->attemptLogin($request)) {
    return $this->sendLoginResponse($request);
  }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
  $this->incrementLoginAttempts($request);

  return $this->sendFailedLoginResponse($request);
}
    public function username()
    {
        return 'user_name';
    }


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
   

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
