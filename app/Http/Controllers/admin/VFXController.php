<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\VFXGroup;
use App\VFX;
use Yajra\Datatables\Datatables;
use Yajra\DataTables\EloquentDataTable;
use Illuminate\Support\Facades\Validator;
class VFXController extends Controller
{
    public function index(){
    	
    	return view('admin.vfxtypes');
    }

    //listing vfx group
    public function list(){
    	 $data = VFX::with('vfxgroup')
                    ->orderBy('created_at','desc')
                    ->get();
    	    return DataTables::of($data)
    	    ->addIndexColumn()
    	    ->rawColumns(['name'])
    	   
    	    ->order(function ($query) {
    	                if (request()->has('created_at')) {
    	                    $query->orderBy('created_at', 'DESC');
    	                }
    	                
    	            })
    	  //   ->setTotalRecords($totalData)
    	 //    ->setFilteredRecords($totalFiltered)
    	    ->skipPaging()
    	    ->make(true);
    }


    //adding vfx type
    public function add(Request $request){
    	$validator = Validator::make($request->all(),[
    	           'type' => 'unique:vfxs',
    	          
    	       ],[
    	        //    'name.required' => "Accommodation Name Required"
    	       ]);

    	if($validator->fails()){
    	   return response()->json(['status'=>'false','message'=>$validator->errors()->first()]);     
    	}   
    	  
            VFX::create($request->all());
    	  return response()->json(['status'=>true,'data'=>"",'message' => 'VFX Types Added Succesfully']);
    }

   //adding vfx group
   //Vfx Groups list
   public function vfxGroup(){
       $data = VFXGroup::all();
       return response()->json($data);
   } 

   //vfx type delete
   public function delete(Request $request){
     VFX::where('id',$request->id)->delete();
     return response()->json(['status'=>true,'data'=>"",'message' => 'VFX Type Deleted Succesfully']);
   }

   public function edit(Request $request){
       $data = VFX::where('id',$request->id)->with('vfxgroup')->first();
       return $data;
   }

   public function update(Request $request){
      $data = VFX::where('id',$request->id)->update($request->all());
      return response()->json(['status'=>true,'data'=>"",'message' => 'VFX Type Updated Succesfully']);
   }

}


