<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProjectType;
use Yajra\Datatables\Datatables;
use Yajra\DataTables\EloquentDataTable;
use Illuminate\Support\Facades\Validator;
class ProjectTypeController extends Controller
{
    public function index(){
        return view('admin.projecttypes');
    }

    //listing vfx group
    public function list(){
    	 $data = ProjectType::orderBy('created_at','desc')
                            ->get();
    	    return DataTables::of($data)
    	    ->addIndexColumn()
    	    ->rawColumns(['name'])
    	   
    	    ->order(function ($query) {
    	                if (request()->has('created_at')) {
    	                    $query->orderBy('created_at', 'DESC');
    	                }
    	                
    	            })
    	  //   ->setTotalRecords($totalData)
    	 //    ->setFilteredRecords($totalFiltered)
    	    ->skipPaging()
    	    ->make(true);
    }

    //add vfx group
    public function add(Request $request){
    	$validator = Validator::make($request->all(),[
    	           'types' => 'unique:project_types',
    	          
    	       ],[
    	        //    'name.required' => "Accommodation Name Required"
    	       ]);

    	if($validator->fails()){
    	   return response()->json(['status'=>'false','message'=>$validator->errors()->first()]);     
    	}   
    	    ProjectType::create($request->all());
    	  return response()->json(['status'=>true,'data'=>"",'message' => 'Project Type Added Succesfully']);
    }

    //delete vfx group
    public function delete(Request $request){
        ProjectType::where('id',$request->id)->delete();
        return response()->json(['status'=>true,'data'=>"",'message' => 'Project Type Deleted Succesfully']);
    }

    //vfx group edit
    public function edit(Request $request){
        $data = ProjectType::where('id',$request->id)->first();
        return $data;
    }

    //vfx group update
    public function update(Request $request){
       $data = ProjectType::where('id',$request->id)->update($request->all());
       return response()->json(['status'=>true,'data'=>"",'message' => 'Project Type Updated Succesfully']);
    }
}
