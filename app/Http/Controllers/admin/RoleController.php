<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use Yajra\Datatables\Datatables;
use Yajra\DataTables\EloquentDataTable;
use Illuminate\Support\Facades\Validator;
class RoleController extends Controller
{
    
    //show admin role blade
    public function index(){
    	
    	return view('admin.role');
    }

    //listing vfx group
    public function list(){
    	 $data = Role::orderBy('created_at','desc')
                      ->get();
    	    return DataTables::of($data)
    	    ->addIndexColumn()
    	    ->rawColumns(['name'])
    	   
    	    ->order(function ($query) {
    	                if (request()->has('created_at')) {
    	                    $query->orderBy('created_at', 'DESC');
    	                }
    	                
    	            })
    	  //   ->setTotalRecords($totalData)
    	 //    ->setFilteredRecords($totalFiltered)
    	    ->skipPaging()
    	    ->make(true);
    }

    //add vfx group
    public function add(Request $request){
    	$validator = Validator::make($request->all(),[
    	           'role' => 'unique:roles',
    	          
    	       ],[
    	        //    'name.required' => "Accommodation Name Required"
    	       ]);

    	if($validator->fails()){
    	   return response()->json(['status'=>'false','message'=>$validator->errors()->first()]);     
    	}   
    	    Role::create($request->all());
    	  return response()->json(['status'=>true,'data'=>"",'message' => 'Role Added Succesfully']);
    }

    //delete vfx group
    public function delete(Request $request){
        Role::where('id',$request->id)->delete();
        return response()->json(['status'=>true,'data'=>"",'message' => 'Role Deleted Succesfully']);
    }

    //vfx group edit
    public function edit(Request $request){
        $data = Role::where('id',$request->id)->first();
        return $data;
    }

    //vfx group update
    public function update(Request $request){
       $data = Role::where('id',$request->id)->update($request->all());
       return response()->json(['status'=>true,'data'=>"",'message' => 'Role Updated Succesfully']);
    }
}
