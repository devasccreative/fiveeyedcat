<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tag;
use Yajra\Datatables\Datatables;
use Yajra\DataTables\EloquentDataTable;
use Illuminate\Support\Facades\Validator;
class TagController extends Controller
{
    public function index(){
        return view('admin.tag');
    }

    //listing vfx group
    public function list(){
    	 $data = Tag::orderBy('created_at','desc')
                      ->get();
    	    return DataTables::of($data)
    	    ->addIndexColumn()
    	    ->rawColumns(['name'])
    	   
    	    ->order(function ($query) {
    	                if (request()->has('created_at')) {
    	                    $query->orderBy('created_at', 'DESC');
    	                }
    	                
    	            })
    	  //   ->setTotalRecords($totalData)
    	 //    ->setFilteredRecords($totalFiltered)
    	    ->skipPaging()
    	    ->make(true);
    }

    //add vfx group
    public function add(Request $request){
    	$validator = Validator::make($request->all(),[
    	           'tag' => 'unique:tags',
    	          
    	       ],[
    	        //    'name.required' => "Accommodation Name Required"
    	       ]);

    	if($validator->fails()){
    	   return response()->json(['status'=>'false','message'=>$validator->errors()->first()]);     
    	}   
    	    Tag::create($request->all());
    	  return response()->json(['status'=>true,'data'=>"",'message' => 'Tag Added Succesfully']);
    }

    //delete vfx group
    public function delete(Request $request){
        Tag::where('id',$request->id)->delete();
        return response()->json(['status'=>true,'data'=>"",'message' => 'Tag Deleted Succesfully']);
    }

    //vfx group edit
    public function edit(Request $request){
        $data = Tag::where('id',$request->id)->first();
        return $data;
    }

    //vfx group update
    public function update(Request $request){
       $data = Tag::where('id',$request->id)->update($request->all());
       return response()->json(['status'=>true,'data'=>"",'message' => 'Tag Updated Succesfully']);
    }
}
