<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Yajra\DataTables\EloquentDataTable;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Mail;
class UserController extends Controller
{
   
   
    //show admin user blade
    public function index(){
    	
    	return view('admin.user');
    }


    //listing Accomodation

    public function getUserList(Request $request){
        $data = User::orderBy('created_at','desc')
                      ->get();

        return DataTables::of($data)
        ->addIndexColumn()
       	->editColumn('approval', function($user){
       	         return '<a href="javascript:void(0);" data-id="'.$user->id.' " data-value="'.$user->approval.'" class="approval">'.$user->approval.'</a>'
       	                 ;
       	       })
        ->rawColumns(['name','approval'])
       
        ->order(function ($query) {
                    if (request()->has('created_at')) {
                        $query->orderBy('created_at', 'DESC');
                    }
                    
                })
    //   ->setTotalRecords($totalData)
     //    ->setFilteredRecords($totalFiltered)
        ->skipPaging()
        ->make(true);
    }

    //change Approval
    public function changeApproval(Request $request){


       User::where('id',$request->id)->update(['approval' => $request->value]);

       return response()->json(['status'=>true,'data'=>"",'message' => 'User approvel Status Changed Succesfully']);
    }

    //add User
    public function addUser(Request $request){
     
     

      $validator = Validator::make($request->all(),[
                 'user_name' => 'unique:users',
                 'email'     => 'unique:users',
             ],[
              //    'name.required' => "Accommodation Name Required"
             ]);


      if($validator->fails()){
         return response()->json(['status'=>'false','message'=>$validator->errors()->first()]);     
      }   

      $data['name']            = $request->input('name'); 
      $data['user_name']       = $request->input('user_name'); 
      $data['email']           = $request->input('email'); 
      $data['phone']           = $request->input('phone'); 
      $data['bio']             = $request->input('bio'); 
      $data['role_id']         = $request->input('role_id'); 
      $data['password']        = Str::random(10); 
     
      User::create($data);
      Mail::send([],[], function($message) use($data) {
             $message->to($data['email'], $data['name'])->subject
                ('Signing Up to 5eyedcat.com');
             $message->from('noreply@bulletproof.film','fiveeyedcat');
             $message->setBody('Thank you for signing up to 5eyedcat.com. Your account is '.$data['user_name'].' and password is '.$data['password'].''); 
          });
      return response()->json(['status'=>true,'data'=>"",'message' => 'User Added Succesfully']);
    }

    //delete User
    public function deleteUser(Request $request){
       return $request->all();
    }

    public function roleList(){
       $data = Role::all();
       return response()->json($data);  
    }

    //edit user
    public function editUser(Request $request){
      $data = User::where('id',$request->id)->first();
      return $data;
    } 

    //edit user password
    public function editUserPassword(Request $request){
      $data = User::where('id',$request->id)->first();
      $data['password'] = str_random(10);

       Mail::send([],[], function($message) use($data) {
             $message->to($data['email'], $data['name'])->subject
                ('Reset Password');
             $message->from('noreply@bulletproof.film','fiveeyedcat');
             $message->setBody('Your User Name is '.$data['user_name'].' And Your New Password is '.$data['password']); 
          });
       $data->password = bcrypt($data->password);
       $data->save();
       return response()->json(['status'=>true,'data'=>"",'message' => 'User Password Reset Succesfully']);
    }

    //edit user
    public function updateUser(Request $request){
      $data = User::where('id',$request->id)->update($request->all());
      return response()->json(['status'=>true,'data'=>"",'message' => 'User Updated Succesfully']);
    }

}
