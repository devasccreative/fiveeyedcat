<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Yajra\DataTables\EloquentDataTable;
use App\VFXGroup;
use App\VFX;
use Illuminate\Support\Facades\Validator;
class VFXtypeGroupController extends Controller
{
    public function index(){
        return view('admin.vfxgroups');
    }

    //listing vfx group
    public function list(){
    	 $data = VFXGroup::orderBy('created_at','desc')
                            ->get();
    	    return DataTables::of($data)
    	    ->addIndexColumn()
    	    ->rawColumns(['name'])
    	   
    	    ->order(function ($query) {
    	                if (request()->has('created_at')) {
    	                    $query->orderBy('created_at', 'DESC');
    	                }
    	                
    	            })
    	  //   ->setTotalRecords($totalData)
    	 //    ->setFilteredRecords($totalFiltered)
    	    ->skipPaging()
    	    ->make(true);
    }

    //add vfx group
    public function add(Request $request){
    	$validator = Validator::make($request->all(),[
    	           'name' => 'unique:vfx_groups',
    	          
    	       ],[
    	        //    'name.required' => "Accommodation Name Required"
    	       ]);

    	if($validator->fails()){
    	   return response()->json(['status'=>'false','message'=>$validator->errors()->first()]);     
    	}   
    	VFXGroup::create($request->all());
    	  return response()->json(['status'=>true,'data'=>"",'message' => 'VFX Group Added Succesfully']);
    }

    //delete vfx group
    public function delete(Request $request){
        VFXGroup::where('id',$request->id)->delete();
        return response()->json(['status'=>true,'data'=>"",'message' => 'VFX Group Deleted Succesfully']);
    }

    //vfx group edit
    public function edit(Request $request){
        $data = VFXGroup::where('id',$request->id)->first();
        return $data;
    }

    //vfx group update
    public function update(Request $request){
       $data = VFXGroup::where('id',$request->id)->update($request->all());
       return response()->json(['status'=>true,'data'=>"",'message' => 'VFX Group Updated Succesfully']);
    }

    

}
