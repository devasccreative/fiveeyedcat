<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PlayList;
use App\VFXType;
use Yajra\Datatables\Datatables;
use Yajra\DataTables\EloquentDataTable;
use Auth;
class PlayListController extends Controller
{
    	public function __construct()
    	{
    	    $this->middleware('auth');
    	}

        //show home blade
        public function index(){
          return view('user.playlist');
        }

        //vfx list
        public function list(){

             $data =PlayList::where('user_id',Auth::id());
if(!empty($data)){
             return DataTables::of($data)
            ->addIndexColumn()
             ->addColumn('action', function($playlist){
                            return '<a href="javascript:void(0);" data-id="'.$playlist->id.'" data-toggle="confirmation" data-title="Are You Sure?" data-popout="true" data-singleton="true">delete</a>';
                           }) 
            ->rawColumns(['name','action'])
            ->order(function ($query) {
                        if (request()->has('created_at')) {
                            $query->orderBy('created_at', 'DESC');
                        }
                        
                    })
           // ->setTotalRecords($totalData)
         //   ->setFilteredRecords($totalFiltered)
            ->skipPaging()
            ->make(true);
}else{
return '';}
        }

        public function deletePlaylist(Request $request){
          
           PlayList::where('id',$request->id)->delete();
           return response()->json(['status'=>true,'message' => 'PlayList Deleted Successfully']);  
        }

        //show playlist
        public function showPlaylist($url){
           return view('user.myplaylist'); 
        }

        // list my playlist
        public function myPlayList(Request $request){
            $data = PlayList::where('url',$request->url)->first();
             $ids = explode(',', $data->scene_id);
            $scene = VFXType::whereIn('id',$ids)->with('projecttype','vfxtype','scenes','tags','vfxgroup')->get();

                return DataTables::of($scene)
               ->addIndexColumn()
               ->addColumn('playlist', function($vfx){
                        return
                            '<div class="custom_checkbox">
                             <label class="control control--checkbox">                                
                               <input type="checkbox" data-id="'.$vfx->id.'" class="playlist" />
                               <div class="control__indicator"></div>
                             </label>                               
                           </div>';
                      }) 
               ->addColumn('scene', function($vfx){
                                 foreach ($vfx->scenes as $value) {
                                   return '<a href="' . route('user.vfxtype', $vfx->id) .'"><img src="'.$value->image.'" height="50" width="50"></a>';
                                 }
                      }) 
               ->addColumn('vfxgroup.name', function($vfx){
                                 return $vfx->vfxgroup->name;
                      })
               ->addColumn('tags', function($vfx){
                             $data =  array();
                                       foreach ($vfx->tags as $tag) {
                                           $data[] =$tag->tagInfo->tag;
                                       }
                                       $data = implode(',',$data);

                                 return $data;
                      })
               ->rawColumns(['name','playlist','scene','vfxgroup.name','tags'])
               ->order(function ($query) {
                           if (request()->has('created_at')) {
                               $query->orderBy('created_at', 'DESC');
                           }
                           
                       })
              // ->setTotalRecords($totalData)
            //   ->setFilteredRecords($totalFiltered)
               ->skipPaging()
               ->make(true);
        }
}
