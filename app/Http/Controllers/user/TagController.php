<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tag;
class TagController extends Controller
{
    //tag search
    public function autoComplete(Request $request){
    	 $term=$request->term;
    	  $queries=Tag::where('tag','LIKE','%'.$term.'%')->get();
    	  $results=array();

    	  foreach ($queries as $query)
    	  {
    	      $results[] = [ 'id' => $query->id, 'value' => $query->tag ];
    	  }
    	  return response()->json($results);
    }
}
