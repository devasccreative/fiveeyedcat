<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

use App\Role;
use Auth;
use  DB;
use Hash;
class HomeController extends Controller
{
    
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	    $this->middleware('auth');
	}

    //show home blade
    public function index(){
           return view('user.home');
    }

    public function profile($id){
        $sceneroles = Role::all();
    	$data = User::where('id',$id)->first(); 
       return view('user.profile',compact('data','sceneroles'));	
    }

    public function updateProfile(Request $request){
    	User::where('id',$request->id)->update($request->except('_token'));
    	return response()->json(['status'=>true,'data'=>"",'message' => 'User Updated Successfully']);
    }

    //change password
    public function changePassword(Request $request){
    	
    	$current_password = Auth::User()->password;           
    	    if(Hash::check($request->old_password, $current_password))
    	    {           
    	      $user_id = Auth::User()->id;                       
    	      $obj_user = User::find($user_id);
    	      $obj_user->password = Hash::make($request->new_password);;
    	      $obj_user->save(); 
    	      return response()->json(['status'=>true,'data'=>"",'message' => 'Password Changed Successfully']);
    	    }
    	    else{
    	    	return response()->json(['status'=>false,'data'=>"",'message' => 'Please Enter Correct Password']);
    	    }
    }
    
}
