<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image; //Intervention Image
use Illuminate\Support\Facades\Storage; //Laravel Filesystem
use App\VFXType;
use App\ProjectType;
use App\SceneTag;
use App\SceneImage;
use App\VFX;
use App\Role;
use App\PlayList;
use App\SceneRole;
use App\VFXGroup;
use App\Tag;
use App\SaveSearch;
use Yajra\Datatables\Datatables;
use Yajra\DataTables\EloquentDataTable;
use Auth;
class VFXTypeController extends Controller
{
    	/**
    	 * Create a new controller instance.
    	 *
    	 * @return void
    	 */
    	public function __construct()
    	{
    	    $this->middleware('auth');
    	}

        //show home blade
        public function index($id){
          $sceneroles     = Role::all();
          $tags           = Tag::all();
          $groups         = VFXGroup::all();
          $vfxtypes       = VFX::all();
          $projecttypes   = ProjectType::all();
          $data = VFXType::where('id',$id)->with('projecttype','vfxtype','scenes','vfxgroup','roles')->first();

          if(Auth::id()== $data->user_id){
              $editable = 1;
          }else{
              $editable = 0;
          }
          
          return view('user.vfxtype',compact('data','editable','sceneroles','tags','groups','vfxtypes','projecttypes'));
        }

        //show search blade
        public function viewSearch(){
          return view('user.search');
        }

        //show search result
        public function showSearch($id){
            
          $data = SaveSearch::where('id',$id)->first();
          return view('user.searchresult',compact('data'));
        }


        //show search list
        public function searchList(){
              $data = SaveSearch::where('user_id',Auth::id())->get();
              return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('search_name', function($search){
                            return '<a href="' . route('user.search.searchresult', $search->id) .'">'.$search->search_name.'</a>';
                           })
                    ->addColumn('action', function($search){
                            return '<a href="javascript:void(0);" data-id="'.$search->id.'" data-toggle="confirmation" data-title="Are You Sure?" data-popout="true" data-singleton="true">delete</a>';
                           }) 
                    
                    ->rawColumns(['name','search_name','action'])
                    ->order(function ($query) {
                                if (request()->has('created_at')) {
                                    $query->orderBy('created_at', 'DESC');
                                }
                                
                            })
                   // ->setTotalRecords($totalData)
                 //   ->setFilteredRecords($totalFiltered)
                    ->skipPaging()
                    ->make(true);
        }

        //show vfx form
        public function vfxForm(){
         
            return view('user.vfxtypeform');   
        }

        //add vfx form
        public function addvfxForm(Request $request){
         
       $request['link'] =str_replace('https://','',$request->input('link'));
       $request['link'] =str_replace('http://','',$request->input('link'));
        

        $data['vfx_type_id']       = $request->input('vfx_type_id');
        $data['project_type_id']   = $request->input('project_type_id');
        $data['vfx_group_id']      = $request->input('vfx_group_id');
            $data['project']       = $request->input('project');
            $data['year']          = $request->input('year');
            $data['type']          = $request->input('type');
            $data['vfx_supe']      = $request->input('vfx_supe');
            $data['rating']        = $request->input('rating');
            if($request->input('link') != '' ){
               $data['link']          = 'https://'.$request->input('link');
            }
            $data['user_id']       = Auth::id();
           

            $id = VFXType::create($data)->id;
            if($request->tagslist != ''){
            foreach ($request->tagslist as $tag){ 
                  $tags[] =[
                                    'tag_id' => Tag::where('tag', $tag)->first()->id,
                                    'scene_id' =>$id,
                                   ];     
            }

          
            SceneTag::insert($tags);
            }
            if($request->hasfile('image'))
                    {
                       foreach($request->file('image') as $file)
                       {
                           $name =$file->getClientOriginalName();
                           $root              = storage_path().'/public/vfxscene/1/';
                          
                           Storage::put('public/vfxscene/1/'.$name, fopen($file, 'r+'));
                           Storage::put('public/vfxscene/1/compressedlogo/'.$name, fopen($file, 'r+')); 

                            $thumbnailpath = public_path('storage/vfxscene/1/compressedlogo/'.$name);
                           
                           
                           $img = Image::make($thumbnailpath)->resize(100, 100, function($constraint) {
                               $constraint->aspectRatio();
                           });
                           $img->save($thumbnailpath); 
                           $images[] = [
                                       'image' => $name,
                                       'scene_id' =>$id,
                                      ]; 
                            
                       }
                    
                   SceneImage::insert($images);
                   }  
            return response()->json(['status'=>true,'data'=>"",'message' => 'VFX Type Added successfully']);
        }

        //vfx list
        public function vfxList(){

             $data = VFXType::with('projecttype','vfxtype','scenes','tags','vfxgroup')->get();
             
   
             return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('playlist', function($vfx){
                     return
                         '<div class="custom_checkbox">
                          <label class="control control--checkbox">                                
                            <input type="checkbox" data-id="'.$vfx->id.'" class="playlist" />
                            <div class="control__indicator"></div>
                          </label>                               
                        </div>';
                   }) 
            ->addColumn('scene', function($vfx){
                              foreach ($vfx->scenes as $value) {
                                return '<a href="' . route('user.vfxtype', $vfx->id) .'"><img src="'.$value->image.'" height="50" width="50"></a>';
                              }
                   }) 
            ->addColumn('vfxgroup.name', function($vfx){
                              return $vfx->vfxgroup->name;
                   })
            ->addColumn('tags', function($vfx){
                          $data =  array();
                                    foreach ($vfx->tags as $tag) {
                                        if(isset($tag->tagInfo->tag)){
                                        $data[] =$tag->tagInfo->tag;
                                      }
                                      else{
                                        $data[] =''; 
                                      }

                                    }
                                    $data = implode(',',$data);

                              return $data;
                   })
            ->rawColumns(['name','playlist','scene','vfxgroup.name','tags'])
            ->order(function ($query) {
                        if (request()->has('created_at')) {
                            $query->orderBy('created_at', 'DESC');
                        }
                        
                    })
           // ->setTotalRecords($totalData)
         //   ->setFilteredRecords($totalFiltered)
            ->skipPaging()
            ->make(true);
        }

        //vfx list
        public function vfxTypeList(Request $request){
       
            $data = VFX::where('vfx_group_id',$request->data)->orderby('type','asc')->get();
           
            return response()->json($data);
        } 

        //vfx list
        public function projectTypeList(){
            $data = ProjectType::all();
            return response()->json($data);
        }

        //tags list
        public function tagsList(){
            $data = Tag::all();
            return response()->json($data);
        }

        //Vfx Groups list
        public function vfxGroup(){
            $data = VFXGroup::orderby('name','asc')->get();
            return response()->json($data);
        }


       //add Tag
       public function addTag(Request $request){
               $object = Tag::updateOrCreate(['tag' => $request->tag], ['tag' => $request->tag]);

          return   $id = $object->id;
       } 

       //search vfx group
       public function searchVFXGroup(Request $request){

          $data = VFXType::whereNotNull('user_id')->with('projecttype','vfxtype','tags','vfxgroup','roles');
          if($request->has('group') && $request->group != ''){
            $data->whereHas('vfxgroup',function($query) use($request){
             $query->where('name','like','%'.$request->group.'%');
           });
          }
          if($request->has('data') && $request->data != ''){
            $data->whereHas('vfxtype',function($query) use($request){
             $query->where('type','like','%'.$request->data.'%');
           });
          }
          if($request->has('tag') && $request->tag != ''){
          $ids =  Tag::where('tag','like','%'.$request->tag.'%')->get();
         
          $tagids = [];
          foreach ($ids as $tag) {
            $tagids[] = $tag->id;
          }
            $data->whereHas('tags', function($q) use($tagids) {
                        $q->whereIn('tag_id',$tagids);
               });
          }
          if($request->has('credit') && $request->credit != ''){
            $data->whereHas('roles', function($q) use($request) {
                        $q->where('screen_credit','like','%'.$request->credit.'%');
                });
          }

          $data =$data->get();
           return DataTables::of($data)
                 ->addIndexColumn()
                 ->addColumn('playlist', function($vfx){
                          return
                              '<div class="custom_checkbox">
                               <label class="control control--checkbox">                                
                                 <input type="checkbox" data-id="'.$vfx->id.'" class="playlist" />
                                 <div class="control__indicator"></div>
                               </label>                               
                             </div>';
                        }) 
                 ->addColumn('scene', function($vfx){
                                 foreach ($vfx->scenes as $value) {
                                 return '<a href="' . route('user.vfxtype', $vfx->id) .'"><img src="'.$value->image.'" height="50" width="50"></a>';
                                 }
                      })
                 ->addColumn('vfxgroup.name', function($vfx){
                               return $vfx->vfxgroup->name;
                    })
                 ->addColumn('tags', function($vfx){
                           $data =  array();
                                     foreach ($vfx->tags as $tag) {
                                         if(isset($tag->tagInfo->tag)){
                                        $data[] =$tag->tagInfo->tag;
                                      }
                                      else{
                                        $data[] =''; 
                                      }
                                     }
                                     $data = implode(',',$data);

                               return $data;
                    })
                ->rawColumns(['name','playlist','scene','vfxgroup.name','tags'])

                
                 ->order(function ($query) {
                             if (request()->has('created_at')) {
                                 $query->orderBy('created_at', 'DESC');
                             }
                             
                         })
                // ->setTotalRecords($totalData)
              //   ->setFilteredRecords($totalFiltered)
                 ->skipPaging()
                 ->make(true);     



         
                        
       
         

        

       }

       //search vfx type
       public function searchVFXType(Request $request){
           
        $ids =  Tag::where('tag','like','%'.$request->tag.'%')->get();
        
        $tagids = [];
        foreach ($ids as $tag) {
          $tagids[] = $tag->id;
        }
        
        if($request->tag != ''){

          


           $data = VFXType::whereHas('tags', function($q) use($tagids) {
                        $q->whereIn('tag_id',$tagids);
             })->whereHas('vfxtype', function($q) use($request) {
                        $q->where('type','like','%'.$request->data.'%');
             })->with('projecttype','vfxtype','tags','vfxgroup')->get();
               
           }
           else if($request->data == ''){
                $data = VFXType::whereHas('tags', function($q) use($tagids) {
                        $q->whereIn('tag_id',$tagids);
                })->with('projecttype','vfxtype','tags','vfxgroup')->get();
           }
           else{
                $data = VFXType::whereHas('vfxtype', function($q) use($request) {
                             $q->where('type','like','%'.$request->data.'%');
                  })->with('projecttype','vfxtype','tags','vfxgroup')->get();
           }
        return DataTables::of($data)
              ->addIndexColumn()
              ->addColumn('playlist', function($arrProduct){
                       return
                           '<div class="custom_checkbox">
                            <label class="control control--checkbox">                                
                              <input type="checkbox" checked="checked"/>
                              <div class="control__indicator"></div>
                            </label>                               
                          </div>';
                     }) 
              ->addColumn('scene', function($vfx){
                              foreach ($vfx->scenes as $value) {
                                return '<img src="'.$value->image.'" height="50" width="50">';
                              }
                   })
              ->rawColumns(['name','playlist','scene'])
              ->order(function ($query) {
                          if (request()->has('created_at')) {
                              $query->orderBy('created_at', 'DESC');
                          }
                          
                      })
             // ->setTotalRecords($totalData)
           //   ->setFilteredRecords($totalFiltered)
              ->skipPaging()
              ->make(true);
           
       }

       //search tags
       public function searchTag(Request $request){
         
        $ids =  Tag::where('tag','like','%'.$request->tag.'%')->get();
        
        $tagids = [];
        foreach ($ids as $tag) {
          $tagids[] = $tag->id;
        }

        if($request->tag == ''){

              $data = VFXType::whereHas('vfxtype', function($q) use($request) {
                           $q->where('type','like','%'.$request->data.'%');
                })->with('projecttype','vfxtype','tags','vfxgroup')->get();
             
         }
        else if($request->data != ''){
           $data = VFXType::whereHas('tags', function($q) use($tagids) {
                        $q->whereIn('tag_id',$tagids);
             })->whereHas('vfxtype', function($q) use($request) {
                        $q->where('type','like','%'.$request->data.'%');
             })->with('projecttype','vfxtype','tags','vfxgroup')->get();
               
           }
           else{

                $data = VFXType::whereHas('tags', function($q) use($tagids) {
                        $q->whereIn('tag_id',$tagids);
                })->with('projecttype','vfxtype','tags','vfxgroup')->get();
           }
        return DataTables::of($data)
              ->addIndexColumn()
              ->addColumn('playlist', function($arrProduct){
                       return
                           '<div class="custom_checkbox">
                            <label class="control control--checkbox">                                
                              <input type="checkbox" checked="checked"/>
                              <div class="control__indicator"></div>
                            </label>                               
                          </div>';
                     }) 
              ->addColumn('scene', function($vfx){
                              foreach ($vfx->scenes as $value) {
                                return '<img src="'.$value->image.'" height="50" width="50">';
                              }
                   })
              ->rawColumns(['name','playlist','scene'])
              ->order(function ($query) {
                          if (request()->has('created_at')) {
                              $query->orderBy('created_at', 'DESC');
                          }
                          
                      })
             // ->setTotalRecords($totalData)
           //   ->setFilteredRecords($totalFiltered)
              ->skipPaging()
              ->make(true);
           
       }

       //save searches
       public function saveSearch(Request $request){
             $data['search_name'] = $request->name;
             $data['vfx_type']    = $request->vfx_type;
             $data['vfx_group']   = $request->vfx_group;
             $data['tags']        = $request->tags;
             $data['user_id']     = Auth::id();
              SaveSearch::create($data);
              return response()->json(['status'=>true,'data'=>"",'message' => 'Search Saved SuccessFully']);
       }

       //save Search result
       public function saveSearchResult(Request $request){
            $data['search_name']     = $request->name;
            $data['vfx_type'] = $request->vfx_type;
            $data['vfx_group']   = $request->vfx_group;
            $data['tags']     = $request->tags;
            $data['user_id']  = Auth::id();
          SaveSearch::where('id',$request->id)->update($data);
           return response()->json(['status'=>true,'data'=>"",'message' => 'Search Updated SuccessFully']); 
       }

       public function roleList(){
          $data = Role::all();
          return response()->json($data);  
       }

       public function addSceneRole(Request $request){

         SceneRole::insert($request->row);
         return response()->json(['status'=>true,'data'=>"",'message' => 'Contact Added SuccessFully']); 
       }

       //Update scene Note
       public function updateSceneNote(Request $request){
       
          VFXType::where('id',$request->scene_id)->update(['scene_note' => $request->scene_note]);
           return response()->json(['status'=>true,'data'=>"",'message' => 'Scene Note Updated SuccessFully']);
       }

       //Update scene Note
       public function updateSceneDid(Request $request){
       
          VFXType::where('id',$request->scene_id)->update(['scene_did' => $request->scene_did]);
           return response()->json(['status'=>true,'data'=>"",'message' => 'Scene Did Updated SuccessFully']);
       }

       public function deleteSearch(Request $request){
         
          SaveSearch::where('id',$request->id)->delete();
          return response()->json(['status'=>true,'message' => 'Search Deleted Successfully']);  
       }

       //autocomplete vfx group
       /*
        AJAX request
        */
        public function getVFXGroup(Request $request){

           $search = $request->search;

           if($search == ''){
             $queries = VFXGroup::orderby('name','asc')->get();
           }else{
              $queries = VFXGroup::orderby('name','asc')->select('id','name')->where('name', 'like', '%' .$search . '%')->limit(20)->get();
           }

           $results=array();
           foreach ($queries as $query)
           {
               $results[] = [ 'id' => $query->id, 'value' => $query->name ];
           }
           return response()->json($results);
        }


         public function getVFX(Request $request){
          
           $group = VFXGroup::where('name',$request->group)->first();
           $search = $request->search;

           if($search == '' && $group != ''){
             $queries = VFX::where('vfx_group_id',$group->id)->orderby('type','asc')->get();
           }else if($group != ''){
              $queries = VFX::orderby('type','asc')->select('id','type')->where(['type'=>'like', '%' .$search . '%','vfx_group_id' => $group->id])->limit(10)->get();
           }else{
               $queries = '';
           }

           $results=array();
          if($queries != ''){
            foreach ($queries as $query)
            {
               $results[] = [ 'id' => $query->id, 'value' => $query->type ];
            }
          } 
           return response()->json($results);
        } 

        public function getTag(Request $request){

           $search = $request->search;

           if($search == ''){
             $queries = Tag::all();
           }else{
              $queries = Tag::orderby('tag','asc')->select('id','tag')->where('tag', 'like', '%' .$search . '%')->limit(10)->get();
           }

           $results=array();
           foreach ($queries as $query)
           {
               $results[] = [ 'id' => $query->id, 'value' => $query->tag ];
           }
           return response()->json($results);
        }

        //get screen credit autocomplete
         //screen credit list
        public function getCreditlist(Request $request){
          $search = $request->search;

          if($search == ''){
            $queries = SceneRole::all();
          }else{
             $queries = SceneRole::orderby('screen_credit','asc')->select('id','screen_credit')->where('screen_credit', 'like', '%' .$search . '%')->limit(10)->get();
          }

          $results=array();
          foreach ($queries as $query)
          {
              $results[] = [ 'id' => $query->id, 'value' => $query->screen_credit ];
          }
          return response()->json($results);
          
        }

        //vfx group focus out
        public function focusoutVFXGroup(Request $request){
            VFXType::where('id',$request->scene_id)->update(['vfx_group_id' => $request->vfx_group_id]);
           return response()->json(['status'=>true,'message' => 'VFX Group Updated Successfully']);  

        } 

        //vfx type focus out
        public function focusoutVFXType(Request $request){
          VFXType::where('id',$request->scene_id)->update(['vfx_type_id' => $request->vfx_type_id]);
          return response()->json(['status'=>true,'message' => 'VFX Type Updated Successfully']);  

        }

        //project focusout
        public function focusoutProject(Request $request){
          VFXType::where('id',$request->scene_id)->update(['project' => $request->project]);
          return response()->json(['status'=>true,'message' => 'Project Updated Successfully']);          
        }

        //year focusout
        public function focusoutYear(Request $request){
          VFXType::where('id',$request->scene_id)->update(['year' => $request->year]);
          return response()->json(['status'=>true,'message' => 'Year Updated Successfully']); 
        }

        //year focusout
        public function focusoutLink(Request $request){
          VFXType::where('id',$request->scene_id)->update(['link' => $request->link]);
          return response()->json(['status'=>true,'message' => 'Link Updated Successfully']); 
        }

        //rating focusout
        public function focusoutRating(Request $request){
          VFXType::where('id',$request->scene_id)->update(['rating' => $request->rating]);
          return response()->json(['status'=>true,'message' => 'Rating Updated Successfully']); 
        }

        //project type focus type
        public function focusoutProjectType(Request $request){
          VFXType::where('id',$request->scene_id)->update(['project_type_id' => $request->project_type_id]);
          //ProjectType::where('id',$request->id)->update(['type' => $request->type]);
          return response()->json(['status'=>true,'message' => 'Project Type Updated Successfully']); 
        }

        //save playlist
        public function savePlaylist(Request $request){
          
            
             $data = array();
          
          
                $data[] =[
                                   'playlist' =>$request->name,
                                   'url'      =>date('mdYHis') . uniqid() . $request->name,
                                   'scene_id' => $request->sceneids,
                                   'user_id' => Auth::id(),

                                  ];   
         
        
           PlayList::Insert($data);
           return response()->json(['status'=>true,'message' => 'Playlist Saved Successfully']); 
        }

        //delete Contact
        public function deleteVfxContact(Request $request){
                SceneRole::where('id',$request->id)->delete();
                return response()->json(['status'=>true,'message' => 'Role Deleted Successfully']); 
        }

        //update screen credit
        public function focusoutScreenCredit(Request $request){
         
          SceneRole::where('id',$request->id)->update($request->all());
          return response()->json(['status'=>true,'message' => 'Screen Credit Updated Successfully']);
        } 

        //update scene role
        public function ChangeSceneRole(Request $request){
         
          SceneRole::where('id',$request->id)->update($request->all());
          return response()->json(['status'=>true,'message' => 'Role Updated Successfully']);
        }

        //update tag
        public function changeSceneTag(Request $request){
          SceneTag::create($request->all());
          return response()->json(['status'=>true,'message' => 'Scene Tag Added Successfully']);
        }

        //remove scene tag
        public function removeSceneTag(Request $request){
           SceneTag::where(['scene_id' => $request->scene_id,'tag_id' => $request->tag_id])->delete();
           return response()->json(['status'=>true,'message' => 'Scene Tag Removed Successfully']);

        }
}
