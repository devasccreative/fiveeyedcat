<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VFXType extends Model
{
    
	protected $table ='vfx_types';

    protected $fillable = [
         'project','year','vfx_supe','rating','link','project_type_id','vfx_type_id','vfx_group_id','scene_note','scene_did','user_id'];


    public function getSceneAttribute($value)
    {   
        if(file_exists(storage_path('app/public/vfxscene/compressedlogo/'.$value)) && $value != ''){
            return asset('public/storage/vfxscene/compressedlogo/').'/'.$value;     
        }else{
           return asset('public/storage/vfxscene/noimage.png');
       }
    }

    public function projecttype(){
        return  $this->belongsTo('App\ProjectType','project_type_id');
    }

    public function vfxtype(){
        return  $this->belongsTo('App\VFX','vfx_type_id');
    } 

    public function vfxgroup(){
        return  $this->belongsTo('App\VFXGroup','vfx_group_id');
    }

    public function scenes(){
        return  $this->hasMany('App\SceneImage','scene_id');
    } 

    public function tags(){
        return  $this->hasMany('App\SceneTag','scene_id')->with('tagInfo');
    }

    public function roles(){
        return  $this->hasMany('App\SceneRole','scene_id')->with('roleInfo');
    }
}
