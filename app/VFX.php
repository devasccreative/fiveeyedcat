<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VFX extends Model
{
    	protected $table ='vfxs';

        protected $fillable = [
            'type','vfx_group_id'
        ];

        public function vfxgroup(){
            return  $this->belongsTo('App\VFXGroup','vfx_group_id');
        }
}
