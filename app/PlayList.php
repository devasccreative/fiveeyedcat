<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayList extends Model
{
    	protected $table ='playlists';

        protected $fillable = [
            'scene_id','user_id','playlist','url'
        ];

        public function getUrlAttribute($value)
        {   
        	if(is_null($value)){
        	    return "";
        	}
            return asset('playlist/').'/'.$value; 
        }
}
