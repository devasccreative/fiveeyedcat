<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVfxTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vfx_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('project')->nullable();
            $table->string('year')->nullable();
            $table->string('vfx_supe')->nullable();
            $table->string('rating')->nullable();
            $table->string('link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vfx_types');
    }
}
