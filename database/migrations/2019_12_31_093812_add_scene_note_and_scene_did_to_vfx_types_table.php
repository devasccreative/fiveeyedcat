<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSceneNoteAndSceneDidToVfxTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vfx_types', function (Blueprint $table) {
            $table->string('scene_note')->nullable()->after('rating');
            $table->string('scene_did')->nullable()->after('rating');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vfx_types', function (Blueprint $table) {
            //
        });
    }
}
