<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVfxGroupIdToVfxTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vfx_types', function (Blueprint $table) {
            $table->bigInteger('vfx_group_id')->unsigned()->nullable()->after('user_id');
            $table->foreign('vfx_group_id')->references('id')->on('vfx_groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vfx_types', function (Blueprint $table) {
            //
        });
    }
}
