<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVfxGroupIdToVfxs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vfxs', function (Blueprint $table) {
            $table->bigInteger('vfx_group_id')->unsigned()->nullable();
            $table->foreign('vfx_group_id')->references('id')->on('vfx_groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vfxs', function (Blueprint $table) {
            //
        });
    }
}
