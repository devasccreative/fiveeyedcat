<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProjectTypeIdAndVfxTypeIdToVfxTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vfx_types', function (Blueprint $table) {
            $table->bigInteger('project_type_id')->unsigned()->nullable();
            $table->foreign('project_type_id')->references('id')->on('project_types')->onDelete('cascade');
            $table->bigInteger('vfx_type_id')->unsigned()->nullable();
            $table->foreign('vfx_type_id')->references('id')->on('vfxs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vfx_types', function (Blueprint $table) {
            //
        });
    }
}
