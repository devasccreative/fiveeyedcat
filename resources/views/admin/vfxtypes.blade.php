@extends('admin.layouts.app')
@section('content')
<div class="users_datatablebox">
	<div class="tagserchcover">

	<div class="signeup_lefttextbox">
		<p>VFX Type</p>
	</div>
	<div class="addbtnbox">
		  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add</button>
	</div>
	<table class="table table-striped table-hover" style="width: 100%;" id="dataTable">
		<thead>
			<tr>
				<th>Number</th>
				<th>Group</th>
				<th>Type</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">          
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add VFX Type</h4>
      </div>
      <div class="modal-body">
        <form id="vfxtypeform" enctype="multipart/form-data" method="post">

          <div class="form-group">
          	<label for="vfxtype">VFX Type</label>
            <input type="text" name="type" id="addcompanyname" class="form-control" placeholder="Enter Type Name">
          </div>

          <div class="form-group">
          	<label for="vfxtype">VFX Group</label>
          	<select name="vfx_group_id" id="vfx_group">
          		<option></option>
          	</select>
          </div>
        
          <!-- <div class="form-group">
          	<img id="addimage" src="#" alt="" />
            <input type="file" name="image" accept="image/*" class="required"    onchange="readURL(this);"  />
          </div> -->
          <div class="form-group">
            <!-- <a  class="btn btn-default" onclick="UpdateIndustry()">Update</a>  --> 
            <a  class="btn btn-default" onclick="AddType()">Add</a>
          
          </div>
          </form>
          <div class="modal-footer">

          </div>
        </div>
      </div>
    </div> 
  </div>

  <!--Edit Modal -->
  <div class="modal fade" id="editModal" role="dialog">          
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update VFX Type</h4>
        </div>
        <div class="modal-body">
          <form id="editvfxtypeform"  method="post">
          	<input type="hidden" name="id" id="edittypeid">
            <div class="form-group">
            	<label for="vfxtype">VFX Type</label>
              <input type="text" name="type" id="editvfxtype" class="form-control" placeholder="Enter Type Name">
            </div>

            <div class="form-group">
            	<label for="vfxtype">VFX Group</label>
            	<select name="vfx_group_id" id="editvfxgroup">
            		<option></option>
                </select>
            </div>
          
            <!-- <div class="form-group">
            	<img id="addimage" src="#" alt="" />
              <input type="file" name="image" accept="image/*" class="required"    onchange="readURL(this);"  />
            </div> -->
            <div class="form-group">
              <!-- <a  class="btn btn-default" onclick="UpdateIndustry()">Update</a>  --> 
              <a  class="btn btn-default" onclick="UpdateType()">Update</a>
            
            </div>
            </form>
            <div class="modal-footer">

            </div>
          </div>
        </div>
      </div> 
    </div>
@endsection
@section('js')
	<script type="text/javascript">

		$(document).ready(function(){
			//speakers list
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
			$.ajax({
			   url:"{{route('admin.vfxtype.vfxgroup')}}",
			   type:"POST",
			  
			   success:function(data){
			       $.each(data,function(key, value)
			                     {
			                         $("#editvfxgroup").append('<option value=' + value.id + '>' + value.name + '</option>'); 
			                       
			                         $("#vfx_group").append('<option value=' + value.id + '>' + value.name + '</option>'); 
			                        
			                     });
			       
			        }
			});
		});
		var table   =  $('#dataTable').DataTable({
	              ajax: {
	                            type: "post",
	                            url: "{{route('admin.vfxtype.list') }}",
	                            data: function ( d ) {
	                                d._token = "{{ csrf_token() }}";
	                            }
	                             
	                        },
	                        columns:[
	                           { data:'DT_RowIndex',name:'id'},
	                           { data:'vfxgroup.name',name:'vfxgroup.name'},
	                           { data:'type',name:'type'},
	                           { data:action,name:'action'},
	                          
	                          
	                          
	                          
	                         ]
	              });

		function action(data){
		  return '<a href="javascript:void(0);" class="editbtn" onclick="editvfxtype('+data.id+')" data-toggle="modal" data-target="#editModal"><img src="{{ asset("public/assets/Admin/svg/edit.svg") }}" alt=""></a> <a href="javascript:void(0);" class="deletebtn" data-id="'+data.id+'" data-toggle="confirmation" data-title="Are You Sure?" data-popout="true" data-singleton="true"><img src="{{ asset("public/assets/Admin/svg/delete-button.svg") }}" alt=""></a>';
		}

		function AddType()
		{
		   $('#vfxtypeform').submit();
		}

		$('#vfxtypeform').validate({ // initialize the plugin
		       rules: {
		           type: {
		               required:true,
		             
		           },
		           vfx_group_id: {
		               required:true,
		           }

		       },
		       submitHandler: function(form) {
		       		$.LoadingOverlay("show",{
		       			  image       : "",
		       			  text        : "inserting..."
		       		});
		         		var formData = $('#vfxtypeform').serialize();
		         		 //var formData = $( "#category_form" ).serializeArray();
		         		     
		         		 $.ajaxSetup({
		         		   headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
		         		 });
		         		 $.ajax({
		         		     type:'POST',
		         		     url:'{{route("admin.vfxtype.add")}}',
		         		   
		         		     data: formData,

		         		     beforeSend:function(){},
		         		     success:function(data) {
		         		     	  $.LoadingOverlay("hide");
		         		         if (data.status==true) {
		         		            toastr.success(data.message);
		         		            table.ajax.reload();
		         		            $('#myModal').modal('toggle');
		         		             $('#vfxtypeform')[0].reset();
		         		         } else {                              toastr.error(data.message);  
		         		            
		         		         }
		         		     },
		         		 });
		        }
		   }); 


	//vfx group
	$('#vfx_group').select2({		
	       placeholder: 'Select VFX Group',
	       
	     });

		table.on( 'draw', function () {
		    jQuery('[data-toggle=confirmation]').confirmation({
		      rootSelector: '[data-toggle=confirmation]',
		        onConfirm: function(value) {
		          id = $(this).data('id');
		          $.ajax({
		              url: '{{route("admin.vfxtype.delete")}}',
		              method: 'POST',
		              data: { 
		                    id:id,
		                    _token: "{{ csrf_token() }}"
		                  }, 
		              }).done( function( result ){
		              toastr.success(result.message);
		              table.ajax.reload();
		          });
		        },
		        
		    });
		});

		

		//update vfx type
		//vfx group
		$('#editvfxgroup').select2({		
		       placeholder: 'Select VFX Group',
		       ajax: {
		       	 headers: {
		                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		                },

		         url: '{{route("admin.vfxtype.vfxgroup")}}',
		         type: 'POST',
		         dataType: 'json',
		         data: function (params) {

		                    var queryParameters = {
		                        term: params.term,
		                        
		                    }
		                    return queryParameters;
		                },
		             delay: 400,       
		         processResults: function (data) {
		           	$('#vfx_type').val('');
		           return {
		             results:  $.map(data, function (item) {
		                   return {
		                       text: item.name,
		                       id: item.id
		                   }
		               })
		           };
		         },
		         cache: true
		       }
		     });



		function editvfxtype(id){
		  $.ajaxSetup({
		    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
		  });
		  $.ajax({
		      url: '{{route("admin.vfxtype.edit")}}',
		      method: 'POST',
		      data: { 
		            id:id,
		          }, 
		      }).done( function( result ){
		      $('#edittypeid').val(result.id);
		      $('#editvfxtype').val(result.type);
		      $("#editvfxgroup").val(result.vfx_group_id).trigger('change');
		     // $('#editvfxgroup').val(result.vfx_group_id);
		         // $('#editvfxgroup').select2().trigger('change');
		      
		      

		  });
		}

		function UpdateType()
		{
		   $('#editvfxtypeform').submit();
		}

		$('#editvfxtypeform').validate({ // initialize the plugin
		       rules: {
		           type: {
		               required:true,
		             
		           },
		           vfx_group_id: {
		               required:true,
		           }

		       },
		       submitHandler: function(form) {
		       		$.LoadingOverlay("show",{
		       			  image       : "",
		       			  text        : "inserting..."
		       		});
		         		var formData = $('#editvfxtypeform').serialize();
		         		 //var formData = $( "#category_form" ).serializeArray();
		         		     
		         		 $.ajaxSetup({
		         		   headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
		         		 });
		         		 $.ajax({
		         		     type:'POST',
		         		     url:'{{route("admin.vfxtype.update")}}',
		         		   
		         		     data: formData,

		         		     beforeSend:function(){},
		         		     success:function(data) {
		         		     	  $.LoadingOverlay("hide");
		         		         if (data.status==true) {
		         		            toastr.success(data.message);
		         		            table.ajax.reload();
		         		            $('#editModal').modal('toggle');
		         		             $('#editvfxtypeform')[0].reset();
		         		         } else {                             toastr.error(data.message);  
		         		            
		         		         }
		         		     },
		         		 });
		        }
		   }); 




	</script>
@endsection		