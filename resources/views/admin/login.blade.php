<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=""/>
    <link rel="stylesheet" href="{{ asset('public/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/assets/css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/assets/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/assets/css/styles.css') }}">
    
  
  
</head>
<body>

    <div class="signupcover">
        <div class="wrapper">
            <div class="signtitle"><h3>Sign In</h3></div>
            <div class="table table-full align-center">
                <div class="table-row h100">
                    <div class="table-cell v-align-bottom">
                        <!-- ui-slide -->
                        <div class="relative">
                            <!-- card -->
                            <div class="card">
                                  <form method="POST" action="{{ route('admin.login') }}" id="userform">
                                    @csrf
                                <ul class="ui-formSlide">
                                    <li data-step="1" class="active">
                                        <div class="ui-step-content in">
                                            <div class="signupiner">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control"  name="email" required>
                                                     @error('email')
                                                     <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                               
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="password" class="form-control" name="password" required>
                                                </div>  
                                                <div class="rememberbox">
                                                    <div class="notifcheckbox">
                                                        <div class="custom_checkbox">
                                                            <label class="control control--checkbox">         
                                                                <p>{{ __('Remember Me') }}</p>                      
                                                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                                <div class="control__indicator"></div>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>                                             
                                            </div>
                                        </div>
                                        

                                        <!-- <div class="form-group row">
                                            <div class="col-md-6 offset-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                    <label class="form-check-label" for="remember">
                                                        {{ __('Remember Me') }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="btnsetboxnps">                                          
                                            <div class="buttons">
                                                <button type="submit" id="formsubmit">Submit</button>
                                            </div>                                          
                                        </div>
                                       <!--  <div class="signiupbtn">
                                            @if (Route::has('password.request'))
                                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                                    {{ __('Forgot Your Password?') }}
                                                </a>
                                            @endif
                                        </div> -->
                                    </li>                                   
                                </ul>
                            </form>
                            </div>
                            <!-- ./card -->
                        </div>
                        <!-- .ui-slide -->
                    </div>
                </div>
                
            </div>  
        </div>

    </div>

<script type="text/javascript" src="{{ asset('public/assets/js/jquery.js') }}"></script>
<script src="{{ asset('public/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/assets/js/jquery.dataTables.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js" integrity="sha256-sPB0F50YUDK0otDnsfNHawYmA5M0pjjUf4TvRJkGFrI=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js" integrity="sha256-vb+6VObiUIaoRuSusdLRWtXs/ewuz62LgVXg2f1ZXGo=" crossorigin="anonymous"></script>
<script>
    console.clear();

    var uiFormSlide = {
        init: function() {
            this.steps = $(".ui-formSlide > [data-step]");
            this.currentStep = 0;
            $(this.steps[0])
            .addClass("active")
            .find(".ui-step-content")
            .addClass("in");
        },
        goTo: function(index) {
            this.play(index);
        },
        next: function() {
            this.nextStep = this.getNext(this.currentStep);
            this.animateHeight($(this.steps[this.nextStep]).outerHeight());
            this.currentStep = this.getNext(this.currentStep);
            this.play(this.currentStep, 'forward');
        },
        prev: function() {
            this.prevStep = this.getPrev(this.currentStep);
            this.animateHeight($(this.steps[this.prevStep]).outerHeight());
            this.currentStep = this.getPrev(this.currentStep);
            this.play(this.currentStep, 'backward');
        },
        getNext: function(currentStep) {
            return currentStep + 1 >= this.steps.length ? 0 : currentStep + 1;
        },
        getPrev: function(currentStep) {
            return currentStep - 1 < 0 ? this.steps.length - 1 : currentStep - 1;
        },
        play: function(currentStep, direction) {
            var _self = this;
            $('.ui-formSlide').removeClass('forward backward').addClass(direction);
            $(this.steps[currentStep])
            .addClass("active")
            .siblings("[data-step]")
            .removeClass("active");
            setTimeout(function() {
                $(_self.steps[currentStep])
                .find(".ui-step-content")
                .addClass("in")
                .end()
                .siblings("li")
                .find(".ui-step-content")
                .removeClass("in");
            }, 300);
        },
        animateHeight: function(targetHeight) {
            $(".ui-formSlide").animate(
            {
                height: targetHeight + "px"
            },
            300,
            function() {
                $(".ui-formSlide").css("height", "auto");
            }
            );
        }
    };

    $(document).ready(function() {
      

        $("#userform").on("submit", function(e){
           
            e.preventDefault();
          
      
   
  
       var form1 =  $('#userform').serialize() ;
        
        
        $.ajax({
            url: "{{ route('admin.login') }}",
            method: 'POST',
            data: form1,
            }).done( function( result ){
            window.location.href = "{{route('admin.user')}}";
            });
     });
       
    });

</script>
</body>
</html>