@extends('admin.layouts.app')
@section('content')
<div class="users_datatablebox">
	<div class="signeup_lefttextbox">
		<p>Users</p>
	</div>
	<div class="addbtnbox">
	  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add</button>
	</div>
	<table class="table table-striped table-hover" style="width: 100%;" id="dataTable">
		<thead>
			<tr>
				<th>Number</th>
				<th>Name</th>
				<th>Email</th>
				<th>User Name</th>
				<th>Approval</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		
		</tbody>
	</table>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">          
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add User</h4>
      </div>
      <div class="modal-body">
        <form id="userform" enctype="multipart/form-data" method="post">

          <div class="form-group">
            <input type="text" name="name" id="addcompanyname" class="form-control" placeholder="Enter Name">
          </div>
          <div class="form-group">
            <input type="text" name="user_name" id="addusername" class="form-control" placeholder="Enter User Name">
          </div>
          
          <div class="form-group">
            <input type="text" name="email" id="" class="form-control" placeholder="Enter Email">
          </div>

          <div class="form-group">
            <input type="text" name="phone" id="" class="form-control" placeholder="Enter Phone">
          </div> 

          <div class="form-group">
            <input type="text" name="bio" id="" class="form-control" placeholder="Enter Short Bio">
          </div>

          <div class="form-group">
            	<select name="role_id" id="addroleid">
            		<option></option>
                </select>
          </div>
       
          <!-- <div class="form-group">
          	<img id="addimage" src="#" alt="" />
            <input type="file" name="image" accept="image/*" class="required"    onchange="readURL(this);"  />
          </div> -->
          <div class="form-group">
            <!-- <a  class="btn btn-default" onclick="UpdateIndustry()">Update</a>  --> 
            <a  class="btn btn-default" onclick="AddUser()">Add</a>
          
          </div>
          </form>
          <div class="modal-footer">

          </div>
        </div>
      </div>
    </div> 
  </div>

  <!--Edit Modal -->
  <div class="modal fade" id="editModal" role="dialog">          
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit User</h4>
        </div>
        <div class="modal-body">
          <form id="edituserform" enctype="multipart/form-data" method="post">

            <div class="form-group">
              <input type="hidden" name="id" id="editid">
              <input type="text" name="name" id="editname" class="form-control" placeholder="Enter Name">
            </div>
            <div class="form-group">
              <input type="text" name="user_name" id="editusername" class="form-control" placeholder="Enter User Name">
            </div>
            
            <div class="form-group">
              <input type="text" name="email" id="editemail" class="form-control" placeholder="Enter Email">
            </div>

            <div class="form-group">
              <input type="text" name="phone" id="editphone" class="form-control" placeholder="Enter Phone">
            </div> 

            <div class="form-group">
              <input type="text" name="short_bio" id="editbio" class="form-control" placeholder="Enter Short Bio">
            </div>

            <div class="form-group">
              	<select name="role_id" id="editroleid">
              		<option></option>
                  </select>
            </div>
         
            <!-- <div class="form-group">
            	<img id="addimage" src="#" alt="" />
              <input type="file" name="image" accept="image/*" class="required"    onchange="readURL(this);"  />
            </div> -->
            <div class="form-group">
              <!-- <a  class="btn btn-default" onclick="UpdateIndustry()">Update</a>  --> 
              <a  class="btn btn-default" onclick="EditUserSubmit()">Update</a>
            
            </div>
            </form>
            <div class="modal-footer">

            </div>
          </div>
        </div>
      </div> 
    </div>
@endsection
@section('js')
	<script type="text/javascript">
		
	  	//getting Data

	  var table   =  $('#dataTable').DataTable({
	              ajax: {
	                            type: "post",
	                            url: "{{route('admin.userlist') }}",
	                            data: function ( d ) {
	                                d._token = "{{ csrf_token() }}";
	                            }
	                             
	                        },
	                        columns:[
	                           { data:'DT_RowIndex',name:'id'},
	                           { data:'name',name:'name'},
	                           { data:'email',name:'email'},
	                           { data:'user_name',name:'user_name'},
	                           { data:'approval',name:'approval'},
	                           { data:action,name:'action'},
	                          
	                          
	                          
	                         ]
	              });

	  function action(data){
	    return '<a href="javascript:void(0);" class="editbtn" onclick="edituser('+data.id+')" data-toggle="modal" data-target="#editModal"><img src="{{ asset("public/assets/Admin/svg/edit.svg") }}" alt=""></a><a href="javascript:void(0);" class="editbtn" onclick="edituserpassword('+data.id+')"><img src="{{ asset("public/assets/Admin/images/reset.png") }}" alt="" title="Reset Password"></a>';
	  }


	 

	  //edit user password
	  function edituserpassword(id){
	  	$.LoadingOverlay("show",{
	  		  image       : "",
	  		  text        : "password resetting..."
	  	});
	  	$.ajaxSetup({
	  	  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
	  	});
	  	$.ajax({
	  	    url: '{{route("admin.user.edituserpassword")}}',
	  	    method: 'POST',
	  	    data: { 
	  	          id:id,
	  	        }, 
	  	    }).done( function( result ){
	  	    $.LoadingOverlay("hide");
	  	    toastr.success(result.message);
	  	});
	  }

	  //edit user
	  function edituser(id){
	  	$.ajaxSetup({
	  	  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
	  	});
	  	$.ajax({
	  	    url: '{{route("admin.user.edit")}}',
	  	    method: 'POST',
	  	    data: { 
	  	          id:id,
	  	        }, 
	  	    }).done( function( result ){
	  	    $('#editid').val(result.id);
	  	    $('#editname').val(result.name);
	  	    $('#editusername').val(result.user_name);
	  	    $('#editemail').val(result.email);
	  	    $('#editphone').val(result.phone);
	  	    $('#editbio').val(result.short_bio);
	  	    $("#editroleid").val(result.role_id).trigger('change');
	  	});
	  }

	  	//changing approval permission 
	  	$(document).on("click", ".approval", function(){
	 		id    = $(this).data('id')
	 		value = $(this).data('value')
	 		if(value == 'yes'){
	 			value = 'no';
	 		}
	 		else{
	 			value = 'yes';
	 		}
	 		$.ajaxSetup({
	 		  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
	 		});
	 		$.ajax({
	 		    type:'POST',
	 		    url:'{{route("admin.changeapproval")}}',
	 		    data: {id:id,value:value},
	 		    success:function(data) {
	 		    	toastr.success(data.message);
	 		    	table.ajax.reload();	
	 		    },
	 		});		
	  	});

	  	function AddUser()
	  	{
	  	   $('#userform').submit();
	  	}  

	  	$('#userform').validate({ // initialize the plugin
	  	       rules: {
	  	           name: {
	  	               required:true,
	  	             
	  	           },
	  	           user_name: {
	  	               required:true,
	  	              
	  	           },
	  	           email:{
	  	           		required:true,
	  	           		email:true
	  	           },
	  	           phone:{
	  	           		required:true,
	  	           },
	  	          

	  	       },
	  	       submitHandler: function(form) {
	  	       		$.LoadingOverlay("show",{
	  	       			  image       : "",
	  	       			  text        : "inserting..."
	  	       		});
	  	         		var formData = $('#userform').serialize();
	  	         		 //var formData = $( "#category_form" ).serializeArray();
	  	         		     
	  	         		 $.ajaxSetup({
	  	         		   headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
	  	         		 });
	  	         		 $.ajax({
	  	         		     type:'POST',
	  	         		     url:'{{route("admin.user.add")}}',
	  	         		   
	  	         		     data: formData,

	  	         		     beforeSend:function(){},
	  	         		     success:function(data) {
	  	         		     	  $.LoadingOverlay("hide");
	  	         		         if (data.status==true) {
	  	         		            toastr.success(data.message);
	  	         		            table.ajax.reload();
	  	         		            $('#myModal').modal('toggle');
	  	         		             $('#userform')[0].reset();
	  	         		         } else {                              
	  	         		         	toastr.error(data.message);  
	  	         		         }
	  	         		     },
	  	         		 });
	  	        }
	  	   });

	  	$(document).ready(function(){
	  		//role list
	  		$('#addroleid').select2({		
		       placeholder: 'Select Role'
		   });

	  		$('#editroleid').select2({		
		       placeholder: 'Select Role'
		   });
	  		$.ajaxSetup({
	  		    headers: {
	  		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  		    }
	  		});
	  		$.ajax({
	  		   url:"{{route('admin.user.rolelist')}}",
	  		   type:"POST",
	  		  
	  		   success:function(data){
	  		       $.each(data,function(key, value)
	  		                     {
	  		                         $("#addroleid").append('<option value=' + value.id + '>' + value.role + '</option>'); 
	  		                         $("#editroleid").append('<option value=' + value.id + '>' + value.role + '</option>'); 
	  		                        
	  		                     });
	  		       
	  		        }
	  		});
	  	});


	  	function EditUserSubmit()
	  	{
	  	   $('#edituserform').submit();
	  	}  

	  	$('#edituserform').validate({ // initialize the plugin
	  	       rules: {
	  	           name: {
	  	               required:true,
	  	             
	  	           },
	  	           user_name: {
	  	               required:true,
	  	              
	  	           },
	  	           email:{
	  	           		required:true,
	  	           		email:true
	  	           },
	  	           phone:{
	  	           		required:true,
	  	           },
	  	          

	  	       },
	  	       submitHandler: function(form) {
	  	       		$.LoadingOverlay("show",{
	  	       			  image       : "",
	  	       			  text        : "updating..."
	  	       		});
	  	         		var formData = $('#edituserform').serialize();
	  	         		 //var formData = $( "#category_form" ).serializeArray();
	  	         		     
	  	         		 $.ajaxSetup({
	  	         		   headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
	  	         		 });
	  	         		 $.ajax({
	  	         		     type:'POST',
	  	         		     url:'{{route("admin.user.update")}}',
	  	         		   
	  	         		     data: formData,

	  	         		     beforeSend:function(){},
	  	         		     success:function(data) {
	  	         		     	  $.LoadingOverlay("hide");
	  	         		         if (data.status==true) {
	  	         		            toastr.success(data.message);
	  	         		            table.ajax.reload();
	  	         		            $('#editModal').modal('toggle');
	  	         		             $('#edituserform')[0].reset();
	  	         		         } else {                              
	  	         		         	toastr.error(data.message);  
	  	         		         }
	  	         		     },
	  	         		 });
	  	        }
	  	   });
	  </script> 
@endsection	             