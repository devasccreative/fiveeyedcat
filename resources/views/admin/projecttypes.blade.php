@extends('admin.layouts.app')
@section('content')
<div class="users_datatablebox">
	<div class="signeup_lefttextbox">
		<p>Project Type</p>
	</div>
	<div class="addbtnbox">
		  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add</button>
	</div>
	<table class="table table-striped table-hover" style="width: 100%;" id="dataTable">
		<thead>
			<tr>
				<th>Number</th>
				<th>Type</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">          
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Project Type</h4>
      </div>
      <div class="modal-body">
        <form id="addprojecttypeform" enctype="multipart/form-data" method="post">

          <div class="form-group">
            <input type="text" name="type" id="addprojecttype" class="form-control" placeholder="Enter Project type">
          </div>
         
          <!-- <div class="form-group">
          	<img id="addimage" src="#" alt="" />
            <input type="file" name="image" accept="image/*" class="required"    onchange="readURL(this);"  />
          </div> -->
          <div class="form-group">
            <!-- <a  class="btn btn-default" onclick="UpdateIndustry()">Update</a>  --> 
            <a  class="btn btn-default" onclick="AddProject()">Add</a>
          
          </div>
          </form>
          <div class="modal-footer">

          </div>
        </div>
      </div>
    </div> 
  </div>

  <!--Edit Modal -->
  <div class="modal fade" id="editModal" role="dialog">          
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Project</h4>
        </div>
        <div class="modal-body">
          <form id="updateprojectform"  method="post">

            <div class="form-group">
             <input type="hidden" name="id" id="editprojectid">	
              <input type="text" name="type" id="ediprojecttype" class="form-control" placeholder="Enter Project Type">
            </div>
           
            <!-- <div class="form-group">
            	<img id="addimage" src="#" alt="" />
              <input type="file" name="image" accept="image/*" class="required"    onchange="readURL(this);"  />
            </div> -->
            <div class="form-group">
              <!-- <a  class="btn btn-default" onclick="UpdateIndustry()">Update</a>  --> 
              <a  class="btn btn-default" onclick="UpdateProject()">Update</a>
            
            </div>
            </form>
            <div class="modal-footer">

            </div>
          </div>
        </div>
      </div> 
    </div>
@endsection
@section('js')
	<script type="text/javascript">
		var table   =  $('#dataTable').DataTable({
	              ajax: {
	                            type: "post",
	                            url: "{{route('admin.projecttype.list') }}",
	                            data: function ( d ) {
	                                d._token = "{{ csrf_token() }}";
	                            }
	                             
	                        },
	                        columns:[
	                           { data:'DT_RowIndex',name:'id'},
	                           { data:'type',name:'type'},
	                           { data:action,name:'action'},
	                          
	                          
	                          
	                          
	                         ]
	              });

		function action(data){
		  return '<a href="javascript:void(0);" class="editbtn" onclick="editprojecttype('+data.id+')" data-toggle="modal" data-target="#editModal"><img src="{{ asset("public/assets/Admin/svg/edit.svg") }}" alt=""></a> <a href="javascript:void(0);" class="deletebtn" data-id="'+data.id+'" data-toggle="confirmation" data-title="Are You Sure?" data-popout="true" data-singleton="true"><img src="{{ asset("public/assets/Admin/svg/delete-button.svg") }}" alt=""></a>';
		}

		function AddProject()
		{
		   $('#addprojecttypeform').submit();
		}

		$('#addprojecttypeform').validate({ // initialize the plugin
		       rules: {
		           type: {
		               required:true,
		             
		           },
		       },
		       submitHandler: function(form) {
		       		$.LoadingOverlay("show",{
		       			  image       : "",
		       			  text        : "inserting..."
		       		});
		         		var formData = $('#addprojecttypeform').serialize();
		         		 //var formData = $( "#category_form" ).serializeArray();
		         		     
		         		 $.ajaxSetup({
		         		   headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
		         		 });
		         		 $.ajax({
		         		     type:'POST',
		         		     url:'{{route("admin.projecttype.add")}}',
		         		   
		         		     data: formData,

		         		     beforeSend:function(){},
		         		     success:function(data) {
		         		     	  $.LoadingOverlay("hide");
		         		         if (data.status==true) {
		         		            toastr.success(data.message);
		         		            table.ajax.reload();
		         		            $('#myModal').modal('toggle');
		         		             $('#addprojecttypeform')[0].reset();
		         		         } else {                              toastr.error(data.message);  
		         		            
		         		         }
		         		     },
		         		 });
		        }
		   });  

		table.on( 'draw', function () {
		    jQuery('[data-toggle=confirmation]').confirmation({
		      rootSelector: '[data-toggle=confirmation]',
		        onConfirm: function(value) {
		          id = $(this).data('id');
		          $.ajax({
		              url: '{{route("admin.projecttype.delete")}}',
		              method: 'POST',
		              data: { 
		                    id:id,
		                    _token: "{{ csrf_token() }}"
		                  }, 
		              }).done( function( result ){
		             toastr.success(result.message);
		             table.ajax.reload();
		          });
		        },
		        
		    });
		} );

		function editprojecttype(id){
		  $.ajaxSetup({
		    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
		  });
		  $.ajax({
		      url: '{{route("admin.projecttype.edit")}}',
		      method: 'POST',
		      data: { 
		            id:id,
		          }, 
		      }).done( function( result ){
		      $('#editprojectid').val(result.id);
		      $('#ediprojecttype').val(result.type);
		  });
		}

		function UpdateProject()
		{
		   $('#updateprojectform').submit();
		}

		$('#updateprojectform').validate({ // initialize the plugin
		       rules: {
		           role: {
		               required:true,
		             
		           },
		       },
		       submitHandler: function(form) {
		       		$.LoadingOverlay("show",{
		       			  image       : "",
		       			  text        : "updating..."
		       		});
		         		var formData = $('#updateprojectform').serialize();
		         		 //var formData = $( "#category_form" ).serializeArray();
		         		     
		         		 $.ajaxSetup({
		         		   headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
		         		 });
		         		 $.ajax({
		         		     type:'POST',
		         		     url:'{{route("admin.projecttype.update")}}',
		         		   
		         		     data: formData,

		         		     beforeSend:function(){},
		         		     success:function(data) {
		         		     	  $.LoadingOverlay("hide");
		         		         if (data.status==true) {
		         		            toastr.success(data.message);
		         		            table.ajax.reload();
		         		            $('#editModal').modal('toggle');
		         		           $('#updateprojectform')[0].reset();
		         		         } else {                              
		         		         	toastr.error(data.message);  
		         		            
		         		         }
		         		     },
		         		 });
		        }
		   });  

	</script>
@endsection		