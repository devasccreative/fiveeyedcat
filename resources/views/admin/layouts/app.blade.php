<!DOCTYPE html>
<html>
<head>
	<title>5eyedcat | Admin</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content=""/>
	<link rel="icon" type="image/png" href="">
	<link rel="stylesheet" href="{{ asset('public/assets/Admin/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('public/assets/Admin/font-awesome/css/font-awesome.min.css') }}">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>	
	<link rel="stylesheet" href="{{ asset('public/assets/Admin/css/styles.css') }}">
	<link rel="stylesheet" href="{{ asset('public/assets/Admin/css/toastr.min.css') }}">
	<link rel="stylesheet" href="{{ asset('public/assets/Admin/css/wickedpicker.min.css') }}">
	<link rel="stylesheet" href="{{ asset('public/assets/Admin/css/select2.min.css') }}">
	<link rel="stylesheet" href="{{ asset('public/assets/Admin/css/bootstrap-datepicker.min.css') }}">
	<link rel="stylesheet" href="{{ asset('public/assets/Admin/css/bootstrap-datetimepicker.min.css') }}">
	<link rel="stylesheet" href="{{ asset('public/assets/Admin/css/daterangepicker.min.css') }}">
	<link rel="stylesheet" href="{{ asset('public/assets/Admin/css/fancybox.min.css') }}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" />
   

@include('admin.layouts.header')
</head>
<body>
 <div class="parettow_coverbox">
 	@include('admin.layouts.sidebar')
 	<div class="right_paddingboxpart">
 		@yield('content')
 	</div>	
 </div>	

 <footer> 	
 @include('admin.layouts.footer')
 </footer>

 @include('admin.layouts.js')
 		@yield('js')
 

</body>
</html>