<div class="left_paretboxpart">
	<div class="innercategories">
		<ul>
			
			<li  class="{{Request::is('admin/user')?'activelist':''}}">
				<a href="{{route('admin.user')}}">Users</a>
			</li>

			<li  class="{{Request::is('admin/vfxgrouptype')?'activelist':''}}">
				<a href="{{route('admin.vfxgrouptype')}}">VFX Group</a>
			</li>
			<li  class="{{Request::is('admin/vfxtype')?'activelist':''}}">
				<a href="{{route('admin.vfxtype')}}">VFX Types</a>
			</li>
			<li  class="{{Request::is('admin/role')?'activelist':''}}">
				<a href="{{route('admin.role')}}">Roles</a>
			</li>
			<li  class="{{Request::is('admin/projecttype')?'activelist':''}}">
				<a href="{{route('admin.projecttype')}}">Project Type</a>
			</li>
			<li  class="{{Request::is('admin/tag')?'activelist':''}}">
				<a href="{{route('admin.tag')}}">Tag</a>
			</li>
			


             	
		</ul>
	</div>
</div>	