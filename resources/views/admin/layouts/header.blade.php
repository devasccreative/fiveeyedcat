
		
<header class="bgheader_bgcolor">
	<div class="logo_leftbox">
		<img src="" alt="">
			
	</div>
	<div class="logout_rightbox">
		<a class="dropdown-item" href="{{ route('admin.logout') }}"
		   onclick="event.preventDefault();
		                 document.getElementById('logout-form').submit();">
		                <!-- <h4>LOGOUT</h4> -->
            <img src="{{ asset('public/assets/Admin/svg/icon-off.svg') }}">
		</a>

		<form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
		    @csrf
		</form>
	</div>
</header>
