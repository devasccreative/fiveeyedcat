<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=""/>
    <link rel="stylesheet" href="{{ asset('public/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/assets/css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/assets/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/assets/css/styles.css') }}">
    
  
  
</head>
<body>

    <div class="signupcover">
        <div class="wrapper">
            <div class="signtitle"><h3>{{ __('Reset Password') }}</h3></div>
            <div class="table table-full align-center">
                <div class="table-row h100">
                    <div class="table-cell v-align-bottom">
                        <!-- ui-slide -->
                        <div class="relative">
            <div class="card">
               

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                @csrf
                    <input type="hidden" name="token" value="{{ $token }}">
            <ul class="ui-formSlide">
                <li data-step="1" class="active">
                    <div class="ui-step-content in">
                        <div class="signupiner">
                            <div class="form-group">
                               <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                           
                                              
                       
                            <div class="form-group">
                               <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                 <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                           
                                              
                        
                            <div class="form-group">
                               <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                           
                                              
                        </div>
                    </div>
                   
                    <div class="btnsetboxnps">                                          
                        <div class="buttons">
                          <button type="submit" class="btn btn-primary">
                              {{ __('Reset Password') }}
                          </button>
                        </div>                                          
                    </div>
                       
                </li>                                   
            </ul>
        </form>
                </div>
            </div>
                                </div>
                                <!-- .ui-slide -->
                            </div>
                        </div>
                        
                    </div>  
                </div>

            </div>

        <script type="text/javascript" src="{{ asset('public/assets/js/jquery.js') }}"></script>
        <script src="{{ asset('public/assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('public/assets/js/jquery.dataTables.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js" integrity="sha256-sPB0F50YUDK0otDnsfNHawYmA5M0pjjUf4TvRJkGFrI=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js" integrity="sha256-vb+6VObiUIaoRuSusdLRWtXs/ewuz62LgVXg2f1ZXGo=" crossorigin="anonymous"></script>
        <script>
            console.clear();

            var uiFormSlide = {
                init: function() {
                    this.steps = $(".ui-formSlide > [data-step]");
                    this.currentStep = 0;
                    $(this.steps[0])
                    .addClass("active")
                    .find(".ui-step-content")
                    .addClass("in");
                },
                goTo: function(index) {
                    this.play(index);
                },
                next: function() {
                    this.nextStep = this.getNext(this.currentStep);
                    this.animateHeight($(this.steps[this.nextStep]).outerHeight());
                    this.currentStep = this.getNext(this.currentStep);
                    this.play(this.currentStep, 'forward');
                },
                prev: function() {
                    this.prevStep = this.getPrev(this.currentStep);
                    this.animateHeight($(this.steps[this.prevStep]).outerHeight());
                    this.currentStep = this.getPrev(this.currentStep);
                    this.play(this.currentStep, 'backward');
                },
                getNext: function(currentStep) {
                    return currentStep + 1 >= this.steps.length ? 0 : currentStep + 1;
                },
                getPrev: function(currentStep) {
                    return currentStep - 1 < 0 ? this.steps.length - 1 : currentStep - 1;
                },
                play: function(currentStep, direction) {
                    var _self = this;
                    $('.ui-formSlide').removeClass('forward backward').addClass(direction);
                    $(this.steps[currentStep])
                    .addClass("active")
                    .siblings("[data-step]")
                    .removeClass("active");
                    setTimeout(function() {
                        $(_self.steps[currentStep])
                        .find(".ui-step-content")
                        .addClass("in")
                        .end()
                        .siblings("li")
                        .find(".ui-step-content")
                        .removeClass("in");
                    }, 300);
                },
                animateHeight: function(targetHeight) {
                    $(".ui-formSlide").animate(
                    {
                        height: targetHeight + "px"
                    },
                    300,
                    function() {
                        $(".ui-formSlide").css("height", "auto");
                    }
                    );
                }
            };

            $(document).ready(function() {
                $("#userform").on("submit", function(){
            
               var form1 =  $('#userform').serialize() ;
                $.ajax({
                    url: "{{ route('login') }}",
                    method: 'POST',
                    data: form1,
                    }).done( function( result ){
                         window.location.href = "{{route('user.home')}}";
                    });
             });
            });

        </script>
        </body>
        </html>