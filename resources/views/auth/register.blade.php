<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=""/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('public/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/assets/css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/assets/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/assets/css/styles.css') }}">
    
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />

</head>
<body>

    <div class="signupcover">
        <div class="wrapper">
            <div class="signtitle"><h3>Sign Up</h3></div>
            <div class="table table-full align-center">
                <div class="table-row h100">
                    <div class="table-cell v-align-bottom">
                        <!-- ui-slide -->
                        <div class="relative">
                            <!-- card -->
                            <div class="card">
                                <form method="POST" action="{{ route('register') }}" id="userform">
                                    {{ csrf_field() }}
                                    <ul class="ui-formSlide">
                                        <li data-step="1" class="active">
                                            <div class="ui-step-content in">
                                                <div class="signupiner">

                                                    <div class="form-group">
                                                        <label >Name</label>
                                                        <input type="text" class="form-control"  name="name" value="{{ old('name') }}" autocomplete="nope" id="name">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="usr">User Name</label>
                                                        <input type="text" class="form-control" id="username" name="user_name"  value="{{ old('user_name') }}" autocomplete="nope">
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong id="usernameerror"></strong>
                                                        </span>
                                                    </div>
                                                  
                                                    
                                                   
                                                    <div class="form-group">
                                                        <label >Password</label>
                                                        <input type="password" class="form-control" name="password" id="password">
                                                    </div>
                                                    <div class="form-group">
                                                        <label >Confirm Password</label>
                                                        <input type="password" class="form-control" name="confirm_password">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input type="text" class="form-control" name="email" value="{{ old('email') }}" id="email" autocomplete="nope">
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong id="emailerror"></strong>
                                                        </span>
                                                    </div>
                                                  
                                                   
                                                  
                                                    <div class="form-group">
                                                        <label for="usr">Name of Referrer</label>
                                                        <input type="text" class="form-control" id="reffer" name="referrer_name" autocomplete="nope">
                                                    </div>
                                                    <div class="notifcheckbox">
                                                        <div class="custom_checkbox">
                                                            <label class="control control--checkbox">
                                                                <p>Notice that the app is for VFX industry professionals only. The new user must agree to these terms before proceeding.</p>
                                                                <input type="checkbox"  id="termsandcondition" />
                                                                <div class="control__indicator"></div>
                                                            </label>
                                                        </div>                                                  
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="btnsetboxnps">                                          
                                                <div class="buttons">
                                                    <button id="btnNext" disabled="" class="btn">Next</button>
                                                </div>                                          
                                            </div>
                                        </li>
                                        <li data-step="1" class="">
                                            <div class="ui-step-content">
                                                <div class="signupiner">
                                                    <div class="form-group">
                                                        <label >Role</label>
                                                        <select class="js-example-basic-single updatescenerole" name="role_id" data-id="" id="roleid">
                                                    @foreach($sceneroles as $role)
                        <option value="{{$role->id}}" 
                            
                            >{{$role->role}}</option>
                    @endforeach
                                                    </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Phone</label>
                                                        <input type="text" class="form-control" required name="phone" autocomplete="nope" id="phone">
                                                    </div>

                                                    <div class="form-group">
                                                        <label >Short Bio</label>
                                                        <textarea class="form-control" name="short_bio" autocomplete="nope" id="shortbio"></textarea>
                                                        
                                                    </div>
                                                    <div class="notifcheckbox">
                                                        <div class="custom_checkbox">
                                                            <label class="control control--checkbox">
                                                                <p>Flag - Allow other users to contact them via email and phone</p>
                                                                <input type="checkbox" checked="checked" name="contact_permission"  value="yes"  />
                                                                <div class="control__indicator"></div>
                                                            </label>
                                                        </div>                                                  
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="btnsetboxnps">                                          
                                                <div class="buttons">
                                                    <button id="btnPrev">Prev</button>
                                                    <button  type="submit" id="submitform">Submit</button>
                                                </div>                                          
                                            </div>
                                        </li>
                                        <div class="signiupbtn">
                                            <a class="btn btn-link" href="{{route('login')}}">Sign In</a>
                                        </div>
<!-- <li data-step="2">
<div class="ui-step-content">
<h3>Form C</h3>
</div>
</li> -->
</ul>
</form>
</div>
<!-- ./card -->
</div>
<!-- .ui-slide -->
</div>
</div>

</div>  
</div>

</div>

<script type="text/javascript" src="{{ asset('public/assets/js/jquery.js') }}"></script>
<script src="{{ asset('public/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/assets/js/jquery.dataTables.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js" integrity="sha256-sPB0F50YUDK0otDnsfNHawYmA5M0pjjUf4TvRJkGFrI=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js" integrity="sha256-vb+6VObiUIaoRuSusdLRWtXs/ewuz62LgVXg2f1ZXGo=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script>
    console.clear();

    var uiFormSlide = {
        init: function() {
            this.steps = $(".ui-formSlide > [data-step]");
            this.currentStep = 0;
            $(this.steps[0])
            .addClass("active")
            .find(".ui-step-content")
            .addClass("in");
        },
        goTo: function(index) {
            this.play(index);
        },
        next: function() {
            this.nextStep = this.getNext(this.currentStep);
            this.animateHeight($(this.steps[this.nextStep]).outerHeight());
            this.currentStep = this.getNext(this.currentStep);
            this.play(this.currentStep, 'forward');
        },
        prev: function() {
            this.prevStep = this.getPrev(this.currentStep);
            this.animateHeight($(this.steps[this.prevStep]).outerHeight());
            this.currentStep = this.getPrev(this.currentStep);
            this.play(this.currentStep, 'backward');
        },
        getNext: function(currentStep) {
            return currentStep + 1 >= this.steps.length ? 0 : currentStep + 1;
        },
        getPrev: function(currentStep) {
            return currentStep - 1 < 0 ? this.steps.length - 1 : currentStep - 1;
        },
        play: function(currentStep, direction) {
            var _self = this;
            $('.ui-formSlide').removeClass('forward backward').addClass(direction);
            $(this.steps[currentStep])
            .addClass("active")
            .siblings("[data-step]")
            .removeClass("active");
            setTimeout(function() {
                $(_self.steps[currentStep])
                .find(".ui-step-content")
                .addClass("in")
                .end()
                .siblings("li")
                .find(".ui-step-content")
                .removeClass("in");
            }, 300);
        },
        animateHeight: function(targetHeight) {
            $(".ui-formSlide").animate(
            {
                height: targetHeight + "px"
            },
            300,
            function() {
                $(".ui-formSlide").css("height", "auto");
            }
            );
        }
    };

    $(document).ready(function() {
        $("#name").attr("autocomplete","nope");
        $("#email").attr("autocomplete","nope");
        $("#reffer").attr("autocomplete","nope");
        $("#username").attr("autocomplete","nope");
        $("#role").attr("autocomplete","nope");
        $("#phone").attr("autocomplete","nope");
        $("#shortbio").attr("autocomplete","nope");

        uiFormSlide.init();

        $("#termsandcondition").change(function() {
            if(this.checked) {
                $("#btnNext").attr("disabled", false);
            }
            else{
                $("#btnNext").attr("disabled",true);   
            }
        });
        $("#btnNext").click(function(e) {
            e.preventDefault();
            var form = $("#userform");
            form.validate({
                rules: {
                    name: {
                        required: true,
                    },
                   
                    password: {
                        required: true,
                        minlength: 6,
                    },
                    confirm_password:{
                        required: true,
                        equalTo : "#password"
                    },
                    

                },
                messages: {
                    user_name: {
                        required: "Username required",
                    },
                    confirm_password:{
                        equalTo: "Confirm Password same as Password"
                    },

                }
            });
            if (form.valid() == true){
               
                 if ($('#termsandcondition').is(':checked')) {
                     uiFormSlide.next();
                     console.log("next");
                 }
                else{


                }
                return false;

            }

        });
        $("#btnPrev").click(function() {


            uiFormSlide.prev();
            return false;
        });
    });


//submit form 
$("#userform").on("submit", function(e){
    e.preventDefault();
    var form1 =  $('#userform').serialize() ;

    $.ajaxSetup({
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });
    $.ajax({
        url: "{{ route('register') }}",
        method: 'POST',
        data: form1,
      
        error: function(xhr, status, error)
        {

            var err = JSON.parse(xhr.responseText);
            $.each(err.errors, function(index) {
                if(index=='user_name'){
                    $('#usernameerror').html(err.errors[index])
                }
                if(index=='email'){
               
                $('#emailerror').html(err.errors[index])
                }
            });
            jQuery('#btnPrev').click();
        },
    }).done( function( result ){
     
         window.location.href = "{{route('login')}}";
 });

// form1.validate({
//     rules: {

//       phone_number  : {
//             required: true,

//         },
//         bio:{
//             required: true,
//         }

//     },
//     messages: {
//         phone_number: {
//             required: "Phone Required",
//         },

//     }
// });
// if (form1.valid() == true){

// }

});

//vfx group
$(document).ready(function(){
$('#roleid').select2({       
       placeholder: 'Select VFX Group',
       
     });
});
</script>
</body>
</html>
