@extends('user.layouts.app')
@section('content')
<style type="text/css">
	.hidecontact{
		pointer-events: none;
  cursor: default;
	}
</style>
<div class="sidelefrig_right">
<div class="tagserchcover">
	<div class="profmnubox">
		<a href="{{route('user.profile',['id'=>Auth::user()->id])}}"><img src="{{ asset('public/assets/user/svg/user.svg') }}" alt=""></a>
	</div>
	<div class="allformcover">
		<div class="noteboxcover">
			<h3>Add New Scene</h3>
			<p>By uploading this scene, you acknowledge and verify that it has been published in an existing production.</p>
		</div>
		<form id="vfxtypeform" enctype="multipart/form-data" method="post">
			@csrf
			<div class="formdetacover">
				<div class="inputselect">
					<div class="width50">
						<div class="form-group">
							<label for="vfxtype">VFX Group</label>
							<select name="vfx_group_id" id="vfx_group">
								<option></option>
							</select>
						</div>
					</div>
					<div class="width50">
						<div class="form-group">
							<label for="vfxtype">VFX Type</label>
							<select name="vfx_type_id" id="vfx_type">
								<option></option>
							</select>
						</div>
					</div>
				</div>
				<div class="inputselect">
					<div class="width50">
						<div class="form-group">
							<label for="vfxtype">Project Name</label>
							<input type="text" name="project" id="" class="form-control" placeholder="Enter Project Name">
						</div> 
					</div> 
					<div class="width50">
						<div class="form-group">
							<label for="vfxtype">Project Year</label>
							<!-- <input type="text" name="year" id="" class="form-control" placeholder="Enter Project Year"> -->
							<select name="year" id="selectyear">
								<option></option>
							</select>
						</div>
					</div>
				</div>
				
				<div class="inputselect">
					<div class="width50">
						<div class="form-group">
							<label for="vfxtype">Project Type</label>
							<select name="project_type_id" id="project_type">
								<option></option>
							</select>
						</div>
					</div> 
					<div class="width50">
						<div class="form-group">
							<label for="vfxtype">VFX Supe</label>
							<input type="text" name="vfx_supe" id="" class="form-control" placeholder="Enter VFX Supe">
						</div>
					</div>
				</div>
				<div class="inputselect">
					<div class="width50">
						<div class="form-group">
							<label for="vfxtype">Video Link</label>
							<div class="input-group">
							    <span class="input-group-addon">https:://</span>

							<input type="text" name="link" id="" class="form-control" placeholder="Enter Link">
							</div>
						</div>
					</div> 
					<div class="width50">
						<div class="form-group">
							<label for="vfxtype">Tag</label>
							<select name="tagslist[]" id="tagslist" multiple="multiple">
								<option></option>
							</select>
						</div>
					</div>
				</div>
				<div class="inputselect">
					<div class="width50">
						<div class="form-group">
							<label for="vfxtype">Rating</label>
							<input type="text" name="rating" id="" class="form-control" placeholder="Enter Rating" value="2.5">
						</div>
						<p class="notetextset">Rating needs to be a number between 1-10</p>
					</div> 
					<div class="width50">
					</div>
				</div>

				<div class="uploadboxset">
					<div class="width100">
						<label for="vfxtype">Upload frame grab</label>
						<div id="scenes" class="imgshowbox">
							
						</div>

						<div class="file-upload">
							<div class="file-select">
								<div class="file-select-button" id="fileName">Choose File</div>
								<div class="file-select-name" id="noFile">No file chosen...</div> 
								<!-- <input type="file" name="chooseFile" id="chooseFile"> -->
								<input type="file" name="image[]" accept="image/*"  multiple=""  id="sceneimages" />
							</div>
						</div>
						<!-- <div class="form-group">							
							<input type="file" name="image[]" accept="image/*"  multiple=""  id="sceneimages" />
							<span style="color: red;" id="maximageserror"></span>
							<p class="note">Maximum upload 4 image.</p>

						</div> -->
					</div>
				</div>
				<div class="width100">
					<div class="addbtnboxset">
						<!-- <a  class="btn btn-default" onclick="UpdateIndustry()">Update</a>  --> 
						<a href="javascript:void(0);" onclick="addVFXType();" id="savevfxform">Save</a>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>
@endsection
@section('js')
<script>
	$('#chooseFile').bind('change', function () {
		var filename = $("#chooseFile").val();
		if (/^\s*$/.test(filename)) {
			$(".file-upload").removeClass('active');
			$("#noFile").text("No file chosen..."); 
		}
		else {
			$(".file-upload").addClass('active');
			$("#noFile").text(filename.replace("C:\\fakepath\\", "")); 
		}
	});

</script>
<script type="text/javascript">
	var nowY = new Date().getFullYear(),
	    options = "";

	for(var Y=nowY; Y>=1968; Y--) {
	  options += "<option>"+ Y +"</option>";
	}

	$("#selectyear").append( options );
	$('#selectyear').select2({
		placeholder: 'Please Select Year'
	});
	if ( window.FileReader ) {
	 
	    document.getElementById("sceneimages").onchange = function(){
	     	$('#maximageserror').html('');	
	        var counter = -1, file;
	        $('#scenes').html('');
	        if(this.files.length>4){
	        	$(this).val('');
	        	$('#maximageserror').html('No more than 4 images');
	        	return false;	
	        }
	        
	        while ( file = this.files[ ++counter ] ) {
	         
	            var reader = new FileReader();
	 
	            reader.onloadend = (function(file){
	                
	                return function(){
	                		
	                    var image = new Image();
	    
	                    image.height = 50;
	                    image.width = 50;
	                    image.title = file.name;
	                    image.src =  this.result;
	    		$(image).appendTo("#scenes");
	    		console.log(image);
	                   
	                    
	                }
	                    
	            })(file);
	                
	            reader.readAsDataURL( file );
	            
	        }
	        
	    }
	    
	}
	
	// function readURL(input) {
	//         if (input.files && input.files[0]) {
	//             var reader = new FileReader();

	//             reader.onload = function (e) {
	//                 $('#addimage')
	//                     .attr('src', e.target.result)
	//                     .width(50)
	//                     .height(50);
	//             };

	//             reader.readAsDataURL(input.files[0]);
	//         }
	//         else{
	//         	 $('#addimage').attr('src','');
	//         }
	//     }


	function addVFXType()
	{

	   $('#vfxtypeform').submit();
	}

	$('#vfxtypeform').validate({ // initialize the plugin
	       rules: {
	           vfx_group_id: {
	           	 required : true,
	           },
	           vfx_type_id: {
	               required: true,
	           },
	           project: {
	               required: true,
	           },
	           rating:{
	           		required: true,
	           		min:0,
	           		max:10
	           }, 
	           
	       },
	       submitHandler: function(form) {
	       		$("#savevfxform").addClass('hidecontact');
	       		
	         		var formData = new FormData($('#vfxtypeform')[0]);
	         		 //var formData = $( "#category_form" ).serializeArray();
	         		     
	         		 $.ajaxSetup({
	         		   headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
	         		 });
	         		 $.ajax({
	         		     type:'POST',
	         		     url:'{{route("user.vfxtypeform")}}',
	         		     processData: false,
	         		     contentType: false,
	         		     data: formData,
	         		     beforeSend:function(){},
	         		     success:function(data) {
	         		     	window.location.href = "{{route('user.home')}}";
	         		     },
	         		 });
	        }
	   });

	//select tags


	$(document).ready(function(){
	   

	$( "#tag" ).autocomplete({
	source: function(request, response) {
	        $.ajax({
	            headers: {
	                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                },
	            type: 'POST',
	            url: '{{route("user.taglist")}}',
	            data: {
	                term : request.term
	            },
	            success: function(data) {
	                response(data);

	            }
	        });
	    },
	   // minLength: 3,
	select: function(event, ui) {
	  $('#tag').val(ui.item.value);
	}
	 });

	    });
	//vfx group
	$('#vfx_group').select2({		
	       placeholder: 'Select VFX Group',
	       ajax: {
	       	 headers: {
	                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                },

	         url: '{{route("user.vfxgroup")}}',
	         type: 'POST',
	         dataType: 'json',
	         data: function (params) {

	                    var queryParameters = {
	                        term: params.term,
	                        
	                    }
	                    return queryParameters;
	                },
	             delay: 400,       
	         processResults: function (data) {
	           
	           return {
	             results:  $.map(data, function (item) {
	                   return {
	                       text: item.name,
	                       id: item.id
	                   }
	               })
	           };
	         },
	         cache: true
	       }
	     });

	//vfx type
	$('#vfx_type').select2({
	    
	       placeholder: 'Select VFX Type',
	       ajax: {
	       	 headers: {
	                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                },

	         url: '{{route("user.vfxslist")}}',
	         type: 'POST',
	         dataType: 'json',
	         //data:$('#vfxgroup').val(),
	         data: function (params) {

	                    var queryParameters = {
	                        term: params.term,
	                        data:$('#vfx_group').val()	                       
	                    }
	                    return queryParameters;
	                },
	             delay: 400,       
	         processResults: function (data) {
	           return {
	             results:  $.map(data, function (item) {
	                   return {
	                       text: item.type,
	                       id: item.id
	                   }
	               })
	           };
	         },
	         cache: true
	       }
	     });

	//project type
	$('#project_type').select2({
	       placeholder: 'Select Project Type',
	       ajax: {
	       	 headers: {
	                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                },

	         url: '{{route("user.projecttypelist")}}',
	         type: 'POST',
	         dataType: 'json',
	         data: function (params) {

	                    var queryParameters = {
	                        term: params.term
	                    }
	                    return queryParameters;
	                },
	             delay: 400,       
	         processResults: function (data) {
	           return {
	             results:  $.map(data, function (item) {
	                   return {
	                       text: item.type,
	                       id: item.id
	                   }
	               })
	           };
	         },
	         cache: true
	       }
	     });

	//tags list
	$('#tagslist').select2({
	        tags: true,
	        
	       placeholder: 'Select Tags',
	       ajax: {
	       	 headers: {
	                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                },

	         url: '{{route("user.tagslist")}}',
	         type: 'POST',
	         dataType: 'json',
	         data: function (params) {

	                    var queryParameters = {
	                        term: params.term
	                    }
	                    return queryParameters;
	                },
			
			  
	             delay: 400,       
	         processResults: function (data) {
	          
	           return {
	             results:  $.map(data, function (item) {
	                   return {
	                       text: item.tag,
	                       id: item.tag
	                   }
	               })
	           };
	         },
	         cache: true
	       }
	     });


	$('#tagslist').on('select2:select', function (e) {
	   var data = e.params.data;
	      console.log(data.text);
	  
	   $.ajax({
	       headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	           },
	       type: 'POST',
	       url: '{{route("user.addtag")}}',
	       data: {
	           tag : data.text,
	           id  : data.id
	       },
	       success: function(data) {
                       
	       }
	   });
	   
	});

	$('#vfx_group').on('select2:select', function (e) {
	  
	   
	           	$("#vfx_type").val('').trigger('change');
	});
</script>
@endsection	    