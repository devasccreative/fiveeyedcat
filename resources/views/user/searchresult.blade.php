@extends('user.layouts.app')
@section('content')
<style type="text/css">
	.hidecontact{
		pointer-events: none;
  cursor: default;
	}
</style>
<div class="sidelefrig_right">
	<div class="tagserchcover">
		<div class="profmnubox">
			<a href="{{route('user.profile',['id'=>Auth::user()->id])}}"><img src="{{ asset('public/assets/user/svg/user.svg') }}" alt=""></a>
		</div>
		<div class="tagserchin_cover">
			<p>Search</p>
			<div class="tagserchin_credit clearser">
				<div class="form-group">
				  	<input type="text" class="form-control" id="searchvfxgroup" placeholder="VFX Group" value="{{$data->vfx_group}}">
				  	<a href="javascript:void(0);" id="cleargroup"><img src="{{ asset('public/assets/user/svg/close.svg') }}" alt=""></a>
				  	<p id="groupfirst" class="error"></p>
				</div>
			</div>
			<div class="tagserchin_type clearser">
				<div class="form-group">
					<input type="hidden" name="id" value="{{$data->id}}">
				  	<input type="text" class="form-control" id="searchvfxtype" placeholder="VFX Type" value="{{$data->vfx_type}}">
				  	<a href="javascript:void(0);" id="cleartype"><img src="{{ asset('public/assets/user/svg/close.svg') }}" alt=""></a>
				</div>
			</div>
			<div class="tagserchin_tag clearser">
				<div class="form-group">
				  	<input type="text" class="form-control" id="searchtag" placeholder="Tag, Tag" value="{{$data->tags}}">
				  	<a href="javascript:void(0);" id="cleartag"><img src="{{ asset('public/assets/user/svg/close.svg') }}" alt=""></a>
				</div>
			</div>
			<div class="tagserchin_credit clearser">
				<div class="form-group">
				  	<input type="text" class="form-control" id="searchscreencredit" placeholder="Screen Credit">
				  	<a href="javascript:void(0);" id="clearcredit"><img src="{{ asset('public/assets/user/svg/close.svg') }}" alt=""></a>
				</div>
			</div>
		</div>				
		<div class="tagserchin_btn">
			<button data-toggle="modal" data-target="#myModal" onclick="savetag();" id="savesearchall" title="please filled atleast one field">Update Search</button>
		</div>
	</div>
	<div class="tablecovrbox">
		<table id="dataTable" class="display responsive" width="100%">
		    <thead>
	            <tr>
			
	              
	                <th>
	                	<div class="custom_checkbox topchckbox">
	                		<label class="control control--checkbox">                                
	                			<input type="checkbox"  id="allplaylist" />
	                			<div class="control__indicator"></div>
	                		</label>                               
	                	</div>
                        <span class="savlaybox">Save <br> Playlist</span></th>
	                <th>Scene</th>
	                <th>VFX Group</th>
	                <th>VFX type</th>
	                <th>Project</th>
	                <th>Year</th>
	                <th>Type</th>
	                <th>VFX Supe</th>
	                <th>Rating</th>
	                <th>Link</th>
	                <th>Tags</th>
	            </tr>
	        </thead>
	        <tbody>
	        	
	        	
	          <!--   <tr>
	            	<td></td>
	                <td>
	                	<div class="custom_checkbox">
				          <label class="control control--checkbox">						           
				            <input type="checkbox" checked="checked"/>
				            <div class="control__indicator"></div>
				          </label>						         
				        </div>
	                </td>
	                <td><div class="vfxtypimg"><img src="{{ asset('public/assets/user/images/img1.jpg')}}" alt=""></div></td>
	                <td>Alien Landing Zone</td>
	                <td>Project Blue</td>
	                <td>2018</td>
	                <td>Movie</td>
	                <td>JJ Peters</td>
	                <td>7/10</td>
	                <td>https://foremostdigital.com</td>
	            </tr>
	            <tr>
	            	<td></td>
	                <td>
	                	<div class="custom_checkbox">
				          <label class="control control--checkbox">						           
				            <input type="checkbox" checked="checked"/>
				            <div class="control__indicator"></div>
				          </label>						         
				        </div>
	                </td>
	                <td><div class="vfxtypimg"><img src="{{ asset('public/assets/user/images/img1.jpg')}}" alt=""></div></td>
	                <td>Alien Landing Zone</td>
	                <td>Project Blue</td>
	                <td>2018</td>
	                <td>Movie</td>
	                <td>JJ Peters</td>
	                <td>7/10</td>
	                <td>https://foremostdigital.com</td>
	            </tr>	 -->		            
	            			            
	        </tbody>			       
		</table>
		<a href="{{route('user.vfxtypeform')}}" class="addnewbtn">Add New</a>
		<a href="javascript:void(0);" class="addnewbtn" data-toggle="modal" data-target="#playListModal" id="addplaylist">Save PlayList</a>
	</div>
	
</div>

<!-- Modal -->
<div class="modal fade sercname" id="myModal" role="dialog">          
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Search Name</h4>
			</div>
			<div class="modal-body">
				<form id="searchform"  method="post">
					<div class="form-group">
						<input type="hidden" name="id" value="{{$data->id}}">
						<input type="text" name="name" id="addname" class="form-control" placeholder="Enter Search Name" value="{{$data->search_name}}">
						<input type="hidden" name="vfx_type" id="savevfx" value="">
						<input type="hidden" name="vfx_group" id="savegroup" value="">
						<input type="hidden" name="tags" id="savetag">
					</div>
					<div class="sercnamebtn">
						<a  class="btn btn-default" onclick="submitsearch();" id="saveaddsearch">Save</a>
					</div>
				</form>
			</div>
		</div>
	</div> 
</div>
<!--PlayList Modal -->
<div class="modal fade sercname" id="playListModal" role="dialog">          
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Playlist Name</h4>
			</div>
			<div class="modal-body">
				<form id="playlistform"  method="post">

					<div class="form-group">
						<input type="text" name="name" id="addname" class="form-control" placeholder="Enter Playlist Name">
						<input type="hidden" name="sceneids" id="playlistsceneid">

					</div>
					<div class="sercnamebtn">
						<a  class="btn btn-default" onclick="submitplaylist();" d="saveaddplaylist">Save</a>
					</div>
				</form>

			</div>
		</div>
	</div> 
</div>
@endsection
@section('js')
<script type="text/javascript">
	
	function savetag(){
		$('#savevfx').val($('#searchvfxtype').val());
		$('#savetag').val($('#searchtag').val());
		$('#savegroup').val($('#searchvfxgroup').val());
	}

	$(document).on('change', '.playlist', function() {
		   len = $(".playlist:checked").length;
		   if(len == 0){
		     	$('#addplaylist').hide();
		   }
		   else{
		   	   $('#addplaylist').show();
		   }
		});


	var table   =  $('#dataTable').DataTable({
			  
	            ajax: {
	                          type: "post",
	                          url: "{{route('user.searchvfxgroup') }}",
	                          data: function ( d ) {
	                              d._token = "{{ csrf_token() }}",
	     			                              d.data = $('#searchvfxtype').val(),
	     			                              d.tag  = $('#searchtag').val(),
	     			                              d.group = $('#searchvfxgroup').val()

	                          }
	                           
	                      },
	                      columns:[
	     			                       
	     			                         { data:'playlist',name:'playlist'},
	     			                         { data:'scene',name:'scene'},
	     			                         { data:'vfxgroup.name',name:'vfxgroup.name',"defaultContent":""},
	     			                         { data:'vfxtype.type',name:'vfxtype.type',"defaultContent":""},
	     			                         { data:'project',name:'project'},
	     			                         { data:'year',name:'year'},
	     			                         { data:'projecttype.type',name:'projecttype.type',"defaultContent":""},
	     			                         { data:'vfx_supe',name:'vfx_supe'},
	     			                         { data:'rating',name:'rating'},
	     			                         { data:'link',name:'link'},
	     			                         { data:'tags',name:'tags',"defaultContent":""},
	     			                       
	     			                        
	     			                        
	     			                       ]
	            });


	    function searchscene(){
	   	
	   		table   =  $('#dataTable').DataTable({
	   				   
	   		            ajax: {
	   		                          type: "post",
	   		                          url: "{{route('user.vfxtypelist') }}",
	   		                          data: function ( d ) {
	   		                              d._token = "{{ csrf_token() }}";
	   		                          },
	   		                          responsive: true,

	   		                           
	   		                      },
	   		                      columns:[
	   		                       
	   		                         { data:'playlist',name:'playlist'},
	   		                         { data:'scene',name:'scenes'},
	   		                         { data:'vfxgroup.name',name:'vfxgroup.name',"defaultContent":""},
	   		                         { data:'vfxtype.type',name:'vfxtype.type',"defaultContent":""},
	   		                         { data:'project',name:'project'},
	   		                         { data:'year',name:'year'},
	   		                         { data:'projecttype.type',name:'projecttype.type',"defaultContent":""},
	   		                         { data:'vfx_supe',name:'vfx_supe'},
	   		                         { data:'rating',name:'rating'},
	   		                         { data:'link',name:'link'},
	   		                         { data:'tags',name:'tags',"defaultContent":""},
	   		                       
	   		                        
	   		                        
	   		                       ]
	   		            });
	            }
		

		

		// //submit save search
		// function submitsearch(){
				
		// 	$.ajax({
		// 	    headers: {
		// 	             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		// 	        },
		// 	    type: 'POST',
		// 	    url: '{{route("user.savesearchresult")}}',
		// 	    data: $('#searchform').serialize(),
		// 	    success: function(data) {
		// 	          toastr.success(data.message);
		// 	          $('#myModal').modal('toggle');         
		// 	    }
		// 	});
		// }

		// //save playlist
		// function submitplaylist(){
				
		// 	$.ajax({
		// 	    headers: {
		// 	             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		// 	        },
		// 	    type: 'POST',
		// 	    url: '{{route("user.saveplaylist")}}',
		// 	    data: $('#playlistform').serialize(),
		// 	    success: function(data) {
		// 	          toastr.success(data.message);
		// 	          $('#playListModal').modal('toggle');         
		// 	    }
		// 	});
		// }

			//submit save search
			function submitsearch(){
				$("#searchform").submit();
					
				// $.ajax({
				//     headers: {
				//              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				//         },
				//     type: 'POST',
				//     url: '{{route("user.savesearch")}}',
				//     data: $('#searchform').serialize(),
				//     success: function(data) {
				//           toastr.success(data.message);
				//           $('#myModal').modal('toggle');         
				//     }
				// });
			}

		    //search form submit

			$('#searchform').validate({ // initialize the plugin
			       rules: {
			           name: {
			               required:true,
			             
			           },
			       },
			       submitHandler: function(form) {
			       		 $("#saveaddsearch").addClass('hidecontact');
			         		var formData = $('#searchform').serialize();
			         		 //var formData = $( "#category_form" ).serializeArray();
			         		     
			         		 $.ajaxSetup({
			         		   headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
			         		 });
			         		 $.ajax({
			         		     type:'POST',
			         		     url:'{{route("user.savesearchresult")}}',
			         		   
			         		     data: formData,

			         		     beforeSend:function(){},
			         		     success:function(data) {
			         		     	 toastr.success(data.message);
			         		     	 $('#myModal').modal('toggle');
			         		     	 $("#saveaddsearch").removeClass('hidecontact'); 
			         		     },
			         		 });
			        }
			   });



			//save playlist
			function submitplaylist(){
				$("#playlistform").submit();
			}

			

			//playlist form submit

			$('#playlistform').validate({ // initialize the plugin
			       ignore: [],
			       rules: {
			           name: {
			               required:true,
			             
			           },
			           sceneids: {
			               required:true,
			           },
			       },
			       messages: {
			          name: "Please Enter Playlist Name",
			          sceneids: {
			            required: "Playlist should not be Empty",
			          }
			        },
			       submitHandler: function(form) {
			       		 $("#saveaddplaylist").addClass('hidecontact');
			         		var formData = $('#playlistform').serialize();
			         		 //var formData = $( "#category_form" ).serializeArray();
			         		     
			         		 $.ajaxSetup({
			         		   headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
			         		 });
			         		 $.ajax({
			         		     type:'POST',
			         		     url:'{{route("user.saveplaylist")}}',
			         		   
			         		     data: formData,

			         		     beforeSend:function(){},
			         		     success:function(data) {
			         		     	 toastr.success(data.message);
			         		     	 $('#playListModal').modal('toggle'); 
			         		     	 $("#saveaddplaylist").removeClass('hidecontact');
			         		     },
			         		 });
			        }
			   });


		//autocomplete off
		$(document).ready(function(){
		    toastr.options = {
		    "preventDuplicates": true,
		    "preventOpenDuplicates": true
		    };
			
			$("#searchvfxgroup").attr("autocomplete", "nope");
			$("#searchvfxtype").attr("autocomplete", "nope");
			$("#searchtag").attr("autocomplete", "nope");
			$('#addplaylist').hide();
			if($('#searchvfxgroup').val() == ''){
			 $('#cleargroup').hide();
		    }
		    if($('#searchvfxtype').val() == ''){
			  $('#cleartype').hide();
		    }
		    if($('#searchtag').val() == ''){
			   $('#cleartag').hide();
		    }
		    if($('#searchscreencredit').val() == ''){
			  $('#clearcredit').hide();
		    }
		});

		

		

		    //delete search
		  //  table.on('draw', function () {
		    	     
		    	  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		    	 
		    		    //vfx group autocomplete
		    		      $("#searchvfxgroup").autocomplete({
		    		        source: function( request, response ) {
		    		          // Fetch data
		    		          $.ajax({
		    		            url:"{{route('user.vfxgroup.autocomplete')}}",
		    		            type: 'post',
		    		            dataType: "json",
		    		            data: {
		    		               _token: CSRF_TOKEN,
		    		               search: request.term
		    		            },
		    		             responsive: true,
		    		            success: function( data ) {
		    		               response( data );
		    		            },
		    	       
		    		          });
		    		        },
		    		        change: function(event,ui){
		    	                    $(this).val((ui.item ? ui.item.value : ""));
		    		            	 var data = $('#searchvfxtype').val(),tag=$('#searchtag').val(),group=$('#searchvfxgroup').val(),credit=$('#searchscreencredit').val();

		    	                   
		    	                },
		    		        select: function (event, ui) {
		    		        	$("#savesearchall").attr("disabled",false);
		    		        	$('#cleargroup').show();
		    		           $('#searchvfxgroup').val(ui.item.value); // save 
		    		            var data = $('#searchvfxtype').val(),tag=$('#searchtag').val(),group=ui.item.value,
		    		            credit=$('#searchscreencredit').val();

		    		
		                           if(tag == '' && data == '' && group == '' && credit == ''){
		     								searchscene();

		     	                   }
		     	                   else{
		     								searchscenelist();
		     	                   }
		    		           return false;
		    		        }
		    		      }); 



		    		       $("#searchvfxtype").autocomplete({
				minLength:0,
	    		      	source: function( request, response ) {
	    		          // Fetch data
	    		          $.ajax({
	    		          	url:"{{route('user.vfxtype.autocomplete')}}",
	    		          	type: 'post',
	    		          	dataType: "json",
	    		          	data: {
	    		          		_token: CSRF_TOKEN,
	    		          		search: request.term,
	    		          		group : $('#searchvfxgroup').val()

	    		          	},
	    		          	success: function( data ) {
	    		          		response( data );
	    		          	},
	    		          	
	    		          });
	    		      },
	    		      change: function(event,ui){
	    		      	$(this).val((ui.item ? ui.item.value : ""));
	    		      },
	    		      select: function (event, ui) {
	    		      	$("#savesearchall").attr("disabled",false);
	    		           $('#searchvfxtype').val(ui.item.value); // save 
	    		            $('#cleartype').show();
	    		           var data =ui.item.value,tag=$('#searchtag').val(),group=$('#searchvfxgroup').val(),credit=$('#searchscreencredit').val();
	    		           if(tag == '' && data == '' && group == '' && credit == ''){
	    		           	searchscene();

	    		           }
	    		           else{
	    		           	searchscenelist();
	    		           }
	    		           return false;
	    		       }
	    		   }).bind('focus', function(){ $(this).autocomplete("search"); } ); 


		    		      $("#searchtag").autocomplete({
		    		      	source: function( request, response ) {
		    		          // Fetch data
		    		          $.ajax({
		    		          	url:"{{route('user.tag.autocomplete')}}",
		    		          	type: 'post',
		    		          	dataType: "json",
		    		          	data: {
		    		          		_token: CSRF_TOKEN,
		    		          		search: request.term
		    		          	},
		    		          	responsive: true,
		    		          	success: function( data ) {
		    		          		response( data );
		    		          	},
		    		          	
		    		          });
		    		      },
		    		      change: function(event,ui){
		    		      	
		    		      	$(this).val((ui.item ? ui.item.value : ""));
		    		      	
		    		      },
		    		      select: function (event, ui) {
		    		      	$("#savesearchall").attr("disabled",false);
		    		      	  $('#cleartag').show();
		    		           $('#searchtag').val(ui.item.value); // save selected 
		    		           var data =$('#searchvfxtype').val(),tag=ui.item.value,group=$('#searchvfxgroup').val(),credit=$('#searchscreencredit').val();
		    		           if(tag == '' && data == '' && group == '' && credit == ''){
		    		           	searchscene();

		    		           }
		    		           else{
		    		           	searchscenelist();
		    		           }
		    		           return false;
		    		       }
		    		   });
		    		      
		    		      $("#searchscreencredit").autocomplete({
		    		      	source: function( request, response ) {
								    		          // Fetch data
								    		          $.ajax({
								    		          	url:"{{route('user.screencredit.autocomplete')}}",
								    		          	type: 'post',
								    		          	dataType: "json",
								    		          	data: {
								    		          		_token: CSRF_TOKEN,
								    		          		search: request.term
								    		          	},
								    		          	responsive: true,
								    		          	success: function( data ) {
								    		          		response( data );
								    		          	},
								    		          	
								    		          });
								    		      },
								    		      change: function(event,ui){
								    		      	$(this).val((ui.item ? ui.item.value : ""));
								    		      	searchscenelist();
								    		      },
								    		      select: function (event, ui) {
								    		      	$("#savesearchall").attr("disabled",false);
								    		        	 $('#searchscreencredit').val(ui.item.value); // save selected
								    		        	 $('#clearcredit').show(); 
								    		        	 var data =$('#searchvfxtype').val(),tag=$('#searchtag').val(),group=$('#searchvfxgroup').val(),credit= $('#searchscreencredit').val();
								    		        	 if(tag == '' && data == '' && group == '' && credit == ''){
								    		        	 	searchscene();

								    		        	 }
								    		        	 else{
								    		        	 	searchscenelist();
								    		        	 }
								    		        	 return false;
								    		        	}
								    		        	
								    		        	
								    		        	
								    		        });

		    		 //   });


			//saveplaylist
			$('#addplaylist').on('click', function() {
			       let a = [];
			          $(".playlist:checked").each(function() {
			              a.push($(this).data('id'));
			          });
			        
			        $('#playlistsceneid').val(a);
			       
			       });

			//all playlist
			$("#allplaylist").change(function() {
			    if(this.checked) {
			    	 $(".playlist").prop('checked',this.checked); 
			    	  $('#addplaylist').show();   
			    }
			    else{
			    	 $('#addplaylist').hide();
			    	 $(".playlist").prop('checked',false);
			    }
			});
		   
			function searchscenelist(){
				 var data =$('#searchvfxtype').val(),tag=$('#searchtag').val(),group=$('#searchvfxgroup').val(),credit=$('#searchscreencredit').val();

				table   =  $('#dataTable').DataTable({
		     					   "bDestroy": true,
		     			            ajax: {
		     			                          type: "post",
		     			                          url: "{{route('user.searchvfxgroup') }}",
		     			                          data: function ( d ) {
		     			                              d._token = "{{ csrf_token() }}",
		     			                              d.group = group,
		     			                              d.data = data,
		     			                              d.tag  = tag
		     			                              d.credit  = credit
		     			                          },
		     			                           responsive: true,
		     			                           
		     			                      },
		     			                      columns:[
		     			                       
		     			                         { data:'playlist',name:'playlist'},
		     			                         { data:'scene',name:'scene'},
		     			                         { data:'vfxgroup.name',name:'vfxgroup.name',"defaultContent":""},
		     			                         { data:'vfxtype.type',name:'vfxtype.type',"defaultContent":""},
		     			                         { data:'project',name:'project'},
		     			                         { data:'year',name:'year'},
		     			                         { data:'projecttype.type',name:'projecttype.type',"defaultContent":""},
		     			                         { data:'vfx_supe',name:'vfx_supe'},
		     			                         { data:'rating',name:'rating'},
		     			                         { data:'link',name:'link'},
		     			                         { data:'tags',name:'tags',"defaultContent":""},
		     			                        
		     			                        
		     			                       ]
		     			            });
			}


			//on search tag null
			$("#searchtag").keyup(function() {

			    if (!this.value) {
			       $('#cleartag').hide();	                   
			       searchscenelist();
			       if($('#searchvfxgroup').val() == '' && $('#searchvfxtype').val() == '' &&
			       $('#searchscreencredit').val() == '' &&  $('#searchtag').val() == ''){
			       	  $("#savesearchall").attr("disabled", true);
			       }
			    }
			    else{
			    	$('#cleartag').show();
			    }
			});

			//on search tag null
			$("#searchvfxgroup").keyup(function() {

			   if (!this.value) {
			      searchscenelist();
			      $('#cleargroup').hide();	
			      if($('#searchvfxgroup').val() == '' && $('#searchvfxtype').val() == '' &&
			      $('#searchscreencredit').val() == '' &&  $('#searchtag').val() == ''){
			      	  $("#savesearchall").attr("disabled", true);
			      }                   
			   }
			   else{
			   	$('#cleargroup').show();
			   }

			});

			//on search tag null
			$("#searchvfxtype").keyup(function() {

			   if (!this.value) {
			      searchscenelist();
			      $('#cleartype').hide();
			      if($('#searchvfxgroup').val() == '' && $('#searchvfxtype').val() == '' &&
			      $('#searchscreencredit').val() == '' &&  $('#searchtag').val() == ''){
			      	  $("#savesearchall").attr("disabled", true);
			      }	                   
			   }
			   else{
			   	$('#cleartype').show();
			   }

			   if($('#searchvfxgroup').val() == ''){
			   	$('#groupfirst').html('Please Select VFX Group First');
			   }
			   else{
			   	$('#groupfirst').html('');
			   }

			});

			//on search tag null
			$("#searchscreencredit").keyup(function() {

			    if (!this.value) {
			       searchscenelist();
			       $('#clearcredit').hide();
			       if($('#searchvfxgroup').val() == '' && $('#searchvfxtype').val() == '' &&
			       $('#searchscreencredit').val() == '' &&  $('#searchtag').val() == ''){
			       	  $("#savesearchall").attr("disabled", true);
			       }	                   
			    }
			    else{
			    	$('#clearcredit').show();
			    }

			});

	
			$('#searchvfxtype').focus(function(){
				if($('#searchvfxgroup').val() == ''){
					$('#groupfirst').html('Please Select VFX Group First');
				}
				else{
					$('#groupfirst').html('');
				}
			}); 

		$('#cleargroup').click(function(){
			$('#searchvfxgroup').val('');
			$('#cleargroup').hide();
			if($('#searchvfxgroup').val() == '' && $('#searchvfxtype').val() == '' &&
			$('#searchscreencredit').val() == '' &&  $('#searchtag').val() == ''){
				  $("#savesearchall").attr("disabled", true);
			}
		});
		$('#cleartype').click(function(){
			$('#searchvfxtype').val('');
			$('#cleartype').hide();
			if($('#searchvfxgroup').val() == '' && $('#searchvfxtype').val() == '' &&
			$('#searchscreencredit').val() == '' &&  $('#searchtag').val() == ''){
				  $("#savesearchall").attr("disabled", true);
			}
		});$('#cleartag').click(function(){
			$('#searchtag').val('');
			$('#cleartag').hide();
			if($('#searchvfxgroup').val() == '' && $('#searchvfxtype').val() == '' &&
			$('#searchscreencredit').val() == '' &&  $('#searchtag').val() == ''){
				  $("#savesearchall").attr("disabled", true);
			}
		});$('#clearcredit').click(function(){
			$('#searchscreencredit').val('');
			$('#clearcredit').hide();
			if($('#searchvfxgroup').val() == '' && $('#searchvfxtype').val() == '' &&
			$('#searchscreencredit').val() == '' &&  $('#searchtag').val() == ''){
				  $("#savesearchall").attr("disabled", true);
			}
		});

</script>
@endsection