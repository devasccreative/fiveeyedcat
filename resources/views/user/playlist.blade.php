@extends('user.layouts.app')
@section('content')
<div class="sidelefrig_right">
	
	
	<div class="tablecovrbox plylisttbl">
		<table id="dataTable" class="display responsive nowrap" width="100%">
		    <thead>
	            <tr>
	                <th class="numberwi">Number</th>
	                <th>Playlist</th>
	                <th>Url</th>
	                <th>Action</th>
	            </tr>
	        </thead>
	        <tbody>
	        	
	        	
	          <!--   <tr>
	            	<td></td>
	                <td>
	                	<div class="custom_checkbox">
				          <label class="control control--checkbox">						           
				            <input type="checkbox" checked="checked"/>
				            <div class="control__indicator"></div>
				          </label>						         
				        </div>
	                </td>
	                <td><div class="vfxtypimg"><img src="{{ asset('public/assets/user/images/img1.jpg')}}" alt=""></div></td>
	                <td>Alien Landing Zone</td>
	                <td>Project Blue</td>
	                <td>2018</td>
	                <td>Movie</td>
	                <td>JJ Peters</td>
	                <td>7/10</td>
	                <td>https://foremostdigital.com</td>
	            </tr>
	            <tr>
	            	<td></td>
	                <td>
	                	<div class="custom_checkbox">
				          <label class="control control--checkbox">						           
				            <input type="checkbox" checked="checked"/>
				            <div class="control__indicator"></div>
				          </label>						         
				        </div>
	                </td>
	                <td><div class="vfxtypimg"><img src="{{ asset('public/assets/user/images/img1.jpg')}}" alt=""></div></td>
	                <td>Alien Landing Zone</td>
	                <td>Project Blue</td>
	                <td>2018</td>
	                <td>Movie</td>
	                <td>JJ Peters</td>
	                <td>7/10</td>
	                <td>https://foremostdigital.com</td>
	            </tr>	 -->		            
	            			            
	        </tbody>			       
		</table>
		
	</div>
</div>
@endsection
@section('js')

<script type="text/javascript">
	var table   =  $('#dataTable').DataTable({
			  
	            ajax: {
	                          type: "post",
	                          url: "{{route('user.playlist.list') }}",
	                          data: function ( d ) {
	                              d._token = "{{ csrf_token() }}";
	                          }
	                           
	                      },
	                      columns:[
	                         { data:'DT_RowIndex',name:'id'},
	                         { data:'playlist',name:'playlist'},
				 { data:url,name:'url'},
	                         { data:'action',name:'action'},
	                       ]
	            });

	function url(data){
		if(data.url != ''){
			return '<p id="'+data.id+'"><a href="'+data.url+'" target="_blank">'+data.url+'</a></p>';
		}
		return '';
	}

	function copyToClipboard(element) {
		
	  var $temp = $("<input>");
	  $("body").append($temp);
	  $temp.val($(element).text()).select();
	  document.execCommand("copy");
	  $temp.remove();
	}

	//delete search
	table.on( 'draw', function () {
	  	jQuery('[data-toggle=confirmation]').confirmation({
	  	  rootSelector: '[data-toggle=confirmation]',
	  	  	onConfirm: function(value) {
	  	  		
	  	  		id = $(this).data('id');
	  	  		$.ajax({
	  	  		    url: '{{route("user.playlist.delete")}}',
	  	  		    method: 'POST',
	  	  		    data: { 
	  	  		    	    id:id,
	  	  		    	    _token: "{{ csrf_token() }}"
	  	  		    	  }, 
	  	  		    }).done( function( result ){
	  	  		  
	  	  		   	toastr.success(result.message);
	  	  		   	table.ajax.reload();
	  	  		});
	  	  	},
	  	  	
	  	});
	} );
</script>
@endsection