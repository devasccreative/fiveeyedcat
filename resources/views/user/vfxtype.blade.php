@extends('user.layouts.app')
@section('content')
<style type="text/css">
	.hidecontact{
		pointer-events: none;
  cursor: default;
	}
</style>
<div class="sidelefrig_right">
	<div class="tagserchcover">
		<div class="profmnubox">
			<a href="{{route('user.profile',['id'=>Auth::user()->id])}}"><img src="{{ asset('public/assets/user/svg/user.svg') }}" alt=""></a>
		</div>
		<div class="tagserchin_cover">
			<p>Search</p>
			<div class="tagserchin_credit">
				<div class="form-group">
				  	<input type="text" class="form-control"  placeholder="VFX Group" name="" autocomplete="nope">
				</div>
			</div>
			<div class="tagserchin_type">
				<div class="form-group">
				  	<input type="text" class="form-control" id="" placeholder="VFX Type" >
				</div>
			</div>
			<div class="tagserchin_tag">
				<div class="form-group">
				  	<input type="text" class="form-control" id="" placeholder="Tag, Tag">
				</div>
			</div>
			<div class="tagserchin_credit">
				<div class="form-group">
				  	<input type="text" class="form-control" id="" placeholder="">
				</div>
			</div>
		</div>				
		<div class="tagserchin_btn">
			<button>Save Search</button>
		</div>
	</div>

	<div class="vfxtpypt_cover">
		<div class="vfxboxiner1">
			<div class="form-group">
				
				<label for="">VFX Group <span>@if($editable == 1)
				      <a href="javascript:void(0);" data-value="scenedidit" onclick="edittextarea('editvfxgroup','showvfxgroup');" class="edittype"><img src="{{ asset('public/assets/user/svg/pencil.svg')}}" alt=""></a>
				@endif</span></label>
			  	<select name="vfx_group_id" id="searchvfxgroup" class="form-control editvfxgroup" style="display: none;">
			  		@foreach($groups as $group)
			  			<option value="{{$group->id}}" 
			  				@if($data->vfx_group_id == $group->id)
			  					selected
			  				@endif
			  				>{{$group->name}}</option>
			  		@endforeach
			  	</select>
			  	<div class="showvfxgroup textshowbox"  
			  	  @isset($data->vfxgroup->name)
			  	   @if($data->vfxgroup->name == '')
			  	      style="display: none;" 
			  	   @endif
			  	  @endisset 
			  	    >
			  			<p>{{ isset($data->vfxgroup->name) && $data->vfxgroup->name ? $data->vfxgroup->name : '-' }}</p>
			  		
			  	</div>
			  	<input type="hidden" name="scene_id" value="{{$data->id}}" id="sceneid">
			  	<input type="hidden" name="scene_id" value="{{ isset($data->vfx_group_id) && $data->vfx_group_id ? $data->vfx_group_id : '-' }}" id="vfxgroupid">
			  	<input type="hidden" name="scene_id" value="{{ isset($data->vfx_type_id) && $data->vfx_type_id ? $data->vfx_type_id : '-' }}" id="vfxtypeid">
			  	<input type="hidden" name="scene_id" value="{{ isset($data->project_type_id) && $data->project_type_id ? $data->project_type_id : '-' }}" id="projecttypeid">
			</div>
		</div>
		<div class="vfxboxiner6">
			<div class="form-group">
				<label for="">VFX Type <span>@if($editable == 1)
				      <a href="javascript:void(0);" data-value="scenedidit" onclick="edittextarea('editvfxtype','showvfxtype');" class="edittype"><img src="{{ asset('public/assets/user/svg/pencil.svg')}}" alt=""></a>
				@endif</span></label>
				<select name="vfx_type_id" id="vfxtypefocusout" class="form-control editvfxtype" style="display: none;">
					@foreach($vfxtypes as $typevalue)
						<option value="{{$typevalue->id}}" 
							@if($data->vfx_type_id == $typevalue->id)
								selected
							@endif
							>{{$typevalue->type}}</option>
					@endforeach
				</select>
			  	<div class="showvfxtype textshowbox" 
			  	  @isset($data->vfxtype->type) 
			  	   @if($data->vfxtype->type == '')
			  	      style="display: none;" 
			  	   @endif
			  	  @endisset 
			  	    >
			  			<p>{{ isset($data->vfxtype->type) && $data->vfxtype->type ? $data->vfxtype->type : '-' }}</p>
			  		
			  	</div>
			</div>
		</div>
		<div class="vfxboxiner2">
			<div class="form-group">
				<label for="">Project<span>@if($editable == 1)
				      <a href="javascript:void(0);" data-value="scenedidit" onclick="edittextarea('editproject','showproject');" class="edittype"><img src="{{ asset('public/assets/user/svg/pencil.svg')}}" alt=""></a>
				@endif</span></label>
			  	<input type="text" class="form-control editproject" <?php if($editable==0){ echo 'disabled';} ?> id="projectfocusout" placeholder="" value="{{ isset($data->project) && $data->project ? $data->project : '' }}"
			  	      style="display: none;" 
			  	      >
			  	<div class="showproject textshowbox"  
			  	   @if($data->project == '')
			  	      style="display: none;" 
			  	   @endif
			  	    >
			  			<p>{{ isset($data->project) && $data->project ? $data->project : '-' }}</p>
			  		
			  	</div>
			</div>
		</div>
		<div class="vfxboxiner3">
			<div class="form-group">
				<label for="">Year<span>@if($editable == 1)
				      <a href="javascript:void(0);" data-value="scenedidit" onclick="edittextarea('edityear','showyear');" class="edittype"><img src="{{ asset('public/assets/user/svg/pencil.svg')}}" alt=""></a>
				@endif</span></label>
			  	<select name="year" id="yearfocusout" class="form-control edityear"  style="display: none;">
			  		<option></option>
			  	</select>      
			  	<div class="showyear textshowbox"  
			  	   @if($data->year == '')
			  	      style="display: none;" 
			  	   @endif
			  	    >
			  			<p>{{ isset($data->year) && $data->year ? $data->year : '-' }}</p>
			  		
			  	</div>
			</div>
			<div class="form-group">
				<label for="">Project Type<span>@if($editable == 1)
				      <a href="javascript:void(0);" data-value="scenedidit" onclick="edittextarea('editprojecttype','showprojecttype');" class="edittype"><img src="{{ asset('public/assets/user/svg/pencil.svg')}}" alt=""></a>
				@endif</span></label>
			  	<select name="project_type_id" id="projecttypefocusout" class="form-control editprojecttype" style="display: none;">
			  		@foreach($projecttypes as $typevalue)
			  			<option value="{{$typevalue->id}}" 
			  				@if($data->project_type_id == $typevalue->id)
			  					selected
			  				@endif
			  				>{{$typevalue->type}}</option>
			  		@endforeach
			  	</select>

			  	<div class="showprojecttype textshowbox"  
			  	   @isset($data->projecttype->type) 
			  	   @if($data->projecttype->type == '')
			  	      style="display: none;" 
			  	   @endif
			  	   @endisset 
			  	    >
			  			<p>{{ isset($data->projecttype->type) && $data->projecttype->type ? $data->projecttype->type : '-' }}</p>
			  		
			  	</div>
			</div>
		</div>
		<div class="vfxboxiner5">
			<!-- <h6>Rating</h6> -->
			<div class="form-group">
				<label for="">Rating<span>@if($editable == 1)
				      <a href="javascript:void(0);" data-value="scenedidit" onclick="edittextarea('editrating','showrating');" class="edittype"><img src="{{ asset('public/assets/user/svg/pencil.svg')}}" alt=""></a>
				@endif</span></label>
			  	<input type="text" class="form-control editrating" <?php if($editable==0){ echo 'disabled';} ?> id="ratingfocusout" placeholder="" value="{{ isset($data->rating) && $data->rating ? $data->rating : '' }}" 
			  	      style="display: none;" 
			  	   >
			  	    <div class="showrating textshowbox"  
			  	       @if($data->rating == '')
			  	          style="display: none;" 
			  	       @endif
			  	        >
			  	    		<p>{{ isset($data->rating) && $data->rating ? $data->rating : '-' }}</p>
			  	    	
			  	    </div>
			</div>
		</div>
		<div class="vfxboxiner4">
			
			<h6>Tags
				@if($editable == 1) 
				<a href="javascript:void(0);" data-value="scenedidit" onclick="edittextarea('edittags','showtags');" class="edittype"><img src="{{ asset('public/assets/user/svg/pencil.svg')}}" alt="" height="15" width="15"></a>
			@endif</h6>

			<div class="edittags" <?php if($data->tags !=''){ ?> style="display: none;" <?php }?>>

								<select class="js-example-basic-single  form-control" name="scenetags[]" data-id="" multiple="multiple" id="myscenetag">
									@foreach($tags as $tag)
										
										<option  value="{{$tag->id}}" 
											@foreach($data->tags as $mytag)
											@if($tag->id == $mytag->tag_id)
												selected
											@endif
										@endforeach	
											 >{{ isset($tag->tag) && $tag->tag ? $tag->tag : '-' }}</option>
									@endforeach
								</select>
				</div>	
							
			<div class="taglisallcover showtags"  
			 @isset($data->tags)
			     @if($data->tags == '')
			  	          style="display: none;" 
			  	       @endif
			 @endisset>
			 	@isset($data->tags)
				  @foreach($data->tags as $tag)
					<p id="tagp{{ isset($tag->tag_id) && $tag->tag_id ? $tag->tag_id : '-' }}">{{ isset($tag->tagInfo->tag) && $tag->tagInfo->tag ? $tag->tagInfo->tag : '-' }}</p>
				 @endforeach
               @endisset
			</div>
			
		</div>
	</div>

	<div class="sldtextcover">
		<div class="sldcoverbox">
			<div class="gallery-slider">
				<div class="gallery-slider__images">
					<div>
					  @isset($data->scenes)
						@foreach($data->scenes as $scene)
							<div class="item">
								<div class="img-fill"><img src="{{ isset($scene->image) && $scene->image ? $scene->image : '' }}"></div>
							</div>
						@endforeach
					  @endisset	
						
					</div>
					<button class="prev-arrow slick-arrow">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1280 1792">
							<path fill="#fff" d="M1171 301L640 832l531 531q19 19 19 45t-19 45l-166 166q-19 19-45 19t-45-19L173 877q-19-19-19-45t19-45L915 45q19-19 45-19t45 19l166 166q19 19 19 45t-19 45z" />
						</svg>
					</button>
					<button class="next-arrow slick-arrow">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1280 1792">
							<path fill="#fff" d="M1107 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45L275 45q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z" />
						</svg>
					</button>
					<span class="caption">&nbsp;</span>
				</div>
				<div class="gallery-slider__thumbnails">
					<div>
						@foreach($data->scenes as $scene)
							<div class="item">
								<div class="img-fill"><img src="{{$scene->image}}"></div>
							</div>
						@endforeach	
						
					</div>

					<button class="prev-arrow slick-arrow">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1280 1792">
							<path fill="#fff" d="M1171 301L640 832l531 531q19 19 19 45t-19 45l-166 166q-19 19-45 19t-45-19L173 877q-19-19-19-45t19-45L915 45q19-19 45-19t45 19l166 166q19 19 19 45t-19 45z" />
						</svg>
					</button>
					<button class="next-arrow slick-arrow">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1280 1792">
							<path fill="#fff" d="M1107 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45L275 45q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z" />
						</svg>
					</button>
				</div>
			</div>
		</div>
		<div class="textslright">
			<div class="textslinerbox">
				
				<h3>Scene Notes:</h3>
				 @if($editable == 1)	
				<a href="javascript:void(0);"  onclick="edittextarea('edittextareanote','showtextboxnote');"  data-value="scenenotes" class="edittype"><img src="{{ asset('public/assets/user/svg/pencil.svg')}}" alt=""></a>
				@endif
				<div class="edittextareanote" <?php if($data->scene_note!=''){ ?>style="display: none;" <?php }?>>
				<textarea class="form-control hidden" id="scenenotes" >{{isset($data->scene_note) && $data->scene_note ? $data->scene_note : ''}}</textarea>
			</div>
				<div class="showtextboxnote"  <?php if($data->scene_note==''){ ?>style="display: none;" <?php }?>>
					<p>{{ $data->scene_note}}</p>
				</div>				
			</div>
			<div class="textslinerbox">

				<h3>How we did it: </h3>
						 @if($editable == 1)
				<a href="javascript:void(0);" data-value="scenedidit" onclick="edittextarea('edittextarea','showtextbox');" class="edittype"><img src="{{ asset('public/assets/user/svg/pencil.svg')}}" alt=""></a>
				  @endif
				<div class="edittextarea" style="display: none;">
				<textarea class="form-control " id="scenedidit">{{isset($data->scene_did) && $data->scene_did ? $data->scene_did : ''}}</textarea>
				</div>
				
				<div class="showtextbox"  <?php if($data->scene_did==''){ ?>style="display: none;" <?php }?>>
					<p>{{$data->scene_did}}</p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="linkboxset">
		<p>Link<span>@if($editable == 1)
				      <a href="javascript:void(0);" data-value="scenedidit" onclick="edittextarea('editlink','showlink');" class="edittype"><img src="{{ asset('public/assets/user/svg/pencil.svg')}}" alt="" height="20" width="20"></a>
				@endif</span></p>
				<input type="text" class="form-control editlink" <?php if($editable==0){ echo 'disabled';} ?> onclick="edittextarea('editlink','showlink');" id="linkfocusout" value="{{ isset($data->link) && $data->link ? $data->link : '' }}" placeholder=""
				      style="display: none;" 
				      >
				<div class="showlink textshowbox"  
				   @if($data->link == '')
				      style="display: none;" 
				   @endif
				    >
		             <a href="javascript:void(0);">{{$data->link}}</a>
	           </div>
	</div>
	<div class="contcoverbox" >
		<h3>CONTACTS</h3>
		<div class="roleleftri_title">
			<div class="roleleftbox"><p>Role</p></div>
			<div class="screcredrightbox"><p>Screen Credit</p></div>
		</div>
	 @if($editable == 1)
		@foreach($data->roles as $scenerole)
		<div class="roleleftri_discbox contactrow" id="deleterow{{$scenerole->id}}">
			<div class="roleleftbox">
				
				
				<select class="js-example-basic-single updatescenerole" name="role_id" data-id="{{$scenerole->id}}">
					@foreach($sceneroles as $role)
						<option value="{{$role->id}}" 
							@if($role->id == $scenerole->role_id) 
                          	  selected                    
						    @endif
							>{{$role->role}}</option>
					@endforeach
				</select>
				
			</div>
			<div class="screcredrightbox">
				
				<div class="form-group">
				  	<input type="text" class="form-control updatescreencontact" id="" placeholder="" value="{{$scenerole->screen_credit	}}" data-id="{{$scenerole->id}}">
				</div>
				<a href="javascript:void(0);" class="contactdelete" data-id="{{$scenerole->id}}" ><img src="{{ asset('public/assets/user/svg/delete.svg')}}" alt=""></a>
			</div>
		</div>

		@endforeach
		<div id="contactfields"></div>
		
		
		<div class="addcontbox">
			<a href="javascript:void(0);" id="addcontact">Add Contact</a>
			<a href="javascript:void(0);" id="savecontact">SAVE</a>
		</div>
		<div class="addcontbox">
		</div>
		@else
					@isset($data->roles) 
					@foreach($data->roles as $scenerole)
					<div class="roleleftri_discbox contactrow" id="deleterow{{$scenerole->id}}">
						<div class="roleleftbox">
							<div class="showyear textshowbox"">
							<p>{{ isset($scenerole->roleInfo->role) && $scenerole->roleInfo->role ? $scenerole->roleInfo->role : '' }}</p>
						    </div>
						</div>

						<div class="screcredrightbox">
							<div class="showyear textshowbox"">
							   <p>{{ isset($scenerole->screen_credit) && $scenerole->screen_credit ? $scenerole->screen_credit : '' }}</p>
						   </div>
						</div>
					</div>
					@endforeach
					@endisset
		@endif
		



	</div>

</div>
@endsection
@section('js')
<script>
		
		$(document).ready(function() {
			toastr.options = {
			"preventDuplicates": true,
			"preventOpenDuplicates": true
			};
			var i = 1;
			$('#savecontact').click(function(e){
				//$('#savecontact').attr('disabled','disabled');
				 $("#savecontact").addClass('hidecontact');
				 var rows = [];
				$('.newcontact').each(function() {
					 var row = {};
                    	row['role_id']       = $(this).find('select').val();
                    	row['scene_id']      = $('#sceneid').val();
                    	row['screen_credit'] = $(this).find('input').val();
                 		rows.push(row);
                 });
			//	console.log(rows);
				if(rows == ''){
					$("#savecontact").removeClass('hidecontact');
					return false;

				}
				if($('#contactfields div').hasClass('newcontact')){
					$('#contactfields div').removeClass('newcontact')
				}
				

				$.ajaxSetup({
				  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
				});
				$.ajax({
				    type:'POST',
				    url:'{{route("user.scenerole.add")}}',
				    
				    data: {row:rows},
				    beforeSend:function(){},
				    success:function(data) {
				    	toastr.success(data.message);
				    	//location.reload();
				    	  $("#savecontact").removeClass('hidecontact');
				    },
				});
			});

			$('#addcontact').click(function(){
			     
			     $('#contactfields').append('<div class="roleleftri_discbox contactrow deleterow newcontact"><div class="roleleftbox"><select class="js-example-basic-single" name="state"></select></div><div class="screcredrightbox"><div class="form-group"><input type="text" class="form-control" id="" placeholder=""></div><a href="javascript:void(0);" class="contactdelete" ><img src="{{ asset("public/assets/user/svg/delete.svg") }}" alt="" ></a></div></div>');
			 jQuery('.js-example-basic-single').select2({		
	       placeholder: 'Select Role',
	       ajax: {
	       	 headers: {
	                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                },

	         url: '{{route("user.rolelist")}}',
	         type: 'POST',
	         dataType: 'json',
	         data: function (params) {

	                    var queryParameters = {
	                        term: params.term,
	                        
	                    }
	                    return queryParameters;
	                },
	             delay: 400,       
	         processResults: function (data) {
	           
	           return {
	             results:  $.map(data, function (item) {
	                   return {
	                       text: item.role,
	                       id: item.id
	                   }
	               })
	           };
	         },
	         cache: true
	       }
	     });
			 });
			
		});

		/*
		variables
		*/

		var $imagesSlider = $(".gallery-slider .gallery-slider__images>div"),
		$thumbnailsSlider = $(".gallery-slider__thumbnails>div");

	/*
		sliders
		*/

		// images options
		$imagesSlider.slick({
			speed:300,
			slidesToShow:1,
			slidesToScroll:1,
			cssEase:'linear',
			fade:true,
			draggable:false,
			asNavFor:".gallery-slider__thumbnails>div",
			prevArrow:'.gallery-slider__images .prev-arrow',
			nextArrow:'.gallery-slider__images .next-arrow'
		});

		// thumbnails options
		$thumbnailsSlider.slick({
			speed:300,
			centerPadding: '0px',
			slidesToShow:3,
			slidesToScroll:1,
			cssEase:'linear',
			centerMode:true,
			draggable:false,
			focusOnSelect:true,
			asNavFor:".gallery-slider .gallery-slider__images>div",
			prevArrow:'.gallery-slider__thumbnails .prev-arrow',
			nextArrow:'.gallery-slider__thumbnails .next-arrow',
			responsive: [
			{
				breakpoint: 720,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 576,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 350,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			}
			]
		});

	/* 
		captions
		*/

		var $caption = $('.gallery-slider .caption');

		// get the initial caption text
		var captionText = $('.gallery-slider__images .slick-current img').attr('alt');
		updateCaption(captionText);

		// hide the caption before the image is changed
		$imagesSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
			$caption.addClass('hide');
		});

		// update the caption after the image is changed
		$imagesSlider.on('afterChange', function(event, slick, currentSlide, nextSlide){
			captionText = $('.gallery-slider__images .slick-current img').attr('alt');
			imageUrl = $('.gallery-slider__images .slick-current img').attr('src');
			image = imageUrl.replace("/compressedlogo/", "/");
			updateCaption(captionText);
			$('.gallery-slider__images .slick-current img').attr('src',image);
		});

		function updateCaption(text) {
			// if empty, add a no breaking space
			if (text === '') {
				text = '&nbsp;';
			}
			$caption.html(text);
			$caption.removeClass('hide');
		}
</script>
<script>
	// In your Javascript (external .js resource or <script> tag)
	$(document).ready(function() {
		var nowY = new Date().getFullYear(),
		    options = "";

		for(var Y=nowY; Y>=1968; Y--) {
		  options += "<option>"+ Y +"</option>";
		}


		$("#yearfocusout").append(options);
		$("#yearfocusout").val("{{ isset($data->year) && $data->year ? $data->year : '' }}");

	    $('.js-example-basic-single').select2({		
	     });
	    imageUrl = $('.gallery-slider__images .slick-current img').attr('src');
	    image = imageUrl.replace("/compressedlogo/", "/");
	    $('.gallery-slider__images .slick-current img').attr('src',image);

	});

	//scene notes focus out
	$("#scenenotes").focusout(function(){
	 
	  $.ajaxSetup({
	    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
	  });
	  var sreennotes=$(this).val();
	  $.ajax({
	      type:'POST',
	      url:'{{route("user.scenenote.update")}}',
	      
	      data: {scene_note:$(this).val(),scene_id:$('#sceneid').val()},
	      beforeSend:function(){},
	      success:function(data) {
	      	toastr.success(data.message);
	      	 //$(this).addClass('hidden');
	      	 jQuery('.showtextboxnote p').html(sreennotes);
	  	    	 	jQuery('.edittextareanote').css('display','none');
					jQuery('.showtextboxnote').css('display','block');

	      },
	  });
	});

	//scene did focus out
	$("#scenedidit").focusout(function(){
	  	$.ajaxSetup({
	  	  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
	  	});
	  	var sreen = jQuery(this).val();
	  	$.ajax({
	  	    type:'POST',
	  	    url:'{{route("user.scenedid.update")}}',
	  	    
	  	    data: {scene_did:$(this).val(),scene_id:$('#sceneid').val()},
	  	    beforeSend:function(){},
	  	    success:function(data) {
	  	    	toastr.success(data.message);
	  	    	    jQuery('.showtextbox p').html(sreen);
	  	    	 	jQuery('.edittextarea').css('display','none');
					jQuery('.showtextbox').css('display','block');
	  	    },
	  	});
	});

	//vfx group focus out
	$("#searchvfxgroup").on('change focusout',function(){
				group = $("#searchvfxgroup option:selected").text();

		$.ajaxSetup({
		  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
		});
		$.ajax({
		    type:'POST',
		    url:'{{route("user.vfxgroup.focusout")}}',
		    
		    data: {vfx_group_id:$(this).val(),scene_id:$('#sceneid').val()},
		    beforeSend:function(){},
		    success:function(data) {
		    	toastr.success(data.message);
		    	  	    	    jQuery('.showvfxgroup p').html(group);
		    	  	    	 	jQuery('.editvfxgroup').css('display','none');
		    					jQuery('.showvfxgroup').css('display','block');
		    },
		});
	});

	//vfx type focus out
	$("#vfxtypefocusout").on('change focusout',function(){
		vfx = $("#vfxtypefocusout option:selected").text();
		$.ajaxSetup({
		  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
		});
		$.ajax({
		    type:'POST',
		    url:'{{route("user.vfxtype.focusout")}}',
		    
		    data: {scene_id:$('#sceneid').val(),vfx_type_id:$(this).val()},
		    beforeSend:function(){},
		    success:function(data) {
		    	toastr.success(data.message);
		    	  	    	    jQuery('.showvfxtype p').html(vfx);
		    	  	    	 	jQuery('.editvfxtype').css('display','none');
		    					jQuery('.showvfxtype').css('display','block');
		    },
		});
	});


	//project focus out
	$("#projectfocusout").focusout(function(){
		project = $(this).val();
		$.ajaxSetup({
		  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
		});
		$.ajax({
		    type:'POST',
		    url:'{{route("user.project.focusout")}}',
		    
		    data: {project:$(this).val(),scene_id:$('#sceneid').val()},
		    beforeSend:function(){},
		    success:function(data) {
		    	toastr.success(data.message);
		    	      	 jQuery('.showproject p').html(project);
		    	  	    	 	jQuery('.editproject').css('display','none');
		    					jQuery('.showproject').css('display','block')
		    },
		});
	});

	

	//year focus out
	$("#yearfocusout").on('change focusout',function(){
		year = $(this).val();
		$.ajaxSetup({
		  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
		});
		$.ajax({
		    type:'POST',
		    url:'{{route("user.year.focusout")}}',
		    
		    data: {year:$(this).val(),scene_id:$('#sceneid').val()},
		    beforeSend:function(){},
		    success:function(data) {
		    	toastr.success(data.message);
		    	      	 jQuery('.showyear p').html(year);
		    	  	    	 	jQuery('.edityear').css('display','none');
		    					jQuery('.showyear').css('display','block')
		    },
		});
	});

	//link focus out
	$("#linkfocusout").focusout(function(){
		link = $(this).val();
		$.ajaxSetup({
		  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
		});
		$.ajax({
		    type:'POST',
		    url:'{{route("user.link.focusout")}}',
		    
		    data: {link:$(this).val(),scene_id:$('#sceneid').val()},
		    beforeSend:function(){},
		    success:function(data) {
		    	toastr.success(data.message);
		    	      	 jQuery('.showlink a').html(link);
		    	  	    	 	jQuery('.editlink').css('display','none');
		    					jQuery('.showlink').css('display','block')
		    },
		});
	});

	//project type focus out
	$("#projecttypefocusout").on('change focusout',function(){
		project = $("#projecttypefocusout option:selected").text();
		$.ajaxSetup({
		  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
		});
		$.ajax({
		    type:'POST',
		    url:'{{route("user.projecttype.focusout")}}',
		    
		    data: {project_type_id:$(this).val(),scene_id:$('#sceneid').val()},
		    beforeSend:function(){},
		    success:function(data) {
		    	toastr.success(data.message);
		    	  	    	    jQuery('.showprojecttype p').html(project);
		    	  	    	 	jQuery('.editprojecttype').css('display','none');
		    					jQuery('.showprojecttype').css('display','block');
		    },
		});
	});	

	//rating focus out
	$("#ratingfocusout").focusout(function(){
		rating = $(this).val();
		$.ajaxSetup({
		  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
		});
		$.ajax({
		    type:'POST',
		    url:'{{route("user.rating.focusout")}}',
		    
		    data: {rating:$(this).val(),scene_id:$('#sceneid').val()},
		    beforeSend:function(){},
		    success:function(data) {
		    	toastr.success(data.message);
		    	  	    	    jQuery('.showrating p').html(rating);
		    	  	    	 	jQuery('.editrating').css('display','none');
		    					jQuery('.showrating').css('display','block');
		    },
		});
	});

	//edit button click

	$( ".edittype" ).click(function() {
	    id =  $(this).data('value');
	    $('#'+id).removeClass('hidden');
	});
	
	function edittextarea(inputClass,ShowClass){
		jQuery('.'+inputClass).css('display','block');
		jQuery('.'+ShowClass).css('display','none');	
	}

	//delete contact
	$(document).on('click','.contactdelete',function(){
	 		
	    id =  $(this).data('id');
	   
	   if(id){
	    $("#deleterow"+id).remove();
	    $.ajaxSetup({
	      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
	    });
	    $.ajax({
	        type:'POST',
	        url:'{{route("user.contact.delete")}}',
	        
	        data: {id:id},
	        beforeSend:function(){},
	        success:function(data) {
	        	toastr.success(data.message);
	        },
	    });
	   }
	   else{
	     	 
	   	$('.deleterow').remove();
	   } 
	});

	//on role change
	$(".updatescenerole").on('change', function(){   
		id=$(this).data('id');
		role = $(this).val();
		$.ajaxSetup({
		  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
		});
		$.ajax({
		    type:'POST',
		    url:'{{route("user.scenerole.change")}}',
		    
		    data: {id:id,role_id:role},
		    beforeSend:function(){},
		    success:function(data) {
		    	toastr.success(data.message);
		    },
		});
	});

	//update screen credit
	$(".updatescreencontact").focusout(function(){
		
		id=$(this).data('id');
		screencredit = $(this).val();
		$.ajaxSetup({
		  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
		});
		$.ajax({
		    type:'POST',
		    url:'{{route("user.screencredit.focusout")}}',
		    
		    data: {id:id,screen_credit:screencredit},
		    beforeSend:function(){},
		    success:function(data) {
		    	toastr.success(data.message);
		    },
		});
	});

	//on adding tag
	$('#myscenetag').on("select2:select", function(e) { 
	   tag_id = e.params.data.id;
	   $.ajaxSetup({
	   		  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
	   		});
	   		$.ajax({
	   		    type:'POST',
	   		    url:'{{route("user.scenetag.change")}}',
	   		    
	   		     data: {tag_id:tag_id,scene_id:$('#sceneid').val()},
	   		    beforeSend:function(){},
	   		    success:function(data) {
	   		    			$('#myscenetag').select2("close");
	   		    	toastr.success(data.message);
	   		    	      	 jQuery('.showtags').append('<p id="tagp'+e.params.data.id+'">'+e.params.data.text+'</p>');
	   		    	  	    	 	jQuery('.edittags').css('display','none');
	   		    					jQuery('.showtags').css('display','block');
	   		    },
	   		});
	});

	//on remove tag
	$('#myscenetag').on("select2:unselecting", function(e){
	         
	         tag_id = e.params.args.data.id;
	         $.ajaxSetup({
	         		  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
	         		});
	         		$.ajax({
	         		    type:'POST',
	         		    url:'{{route("user.scenetag.remove")}}',
	         		    
	         		     data: {tag_id:tag_id,scene_id:$('#sceneid').val()},
	         		    beforeSend:function(){},
	         		    success:function(data) {
	         		    	$('#myscenetag').select2("close");
	         		    		$('#tagp'+tag_id).remove();
	         		    	 jQuery('.edittags').css('display','none');
	   		    					jQuery('.showtags').css('display','block');     	
	         		    	
	         		    },
	         		});
	    });

</script>
@endsection