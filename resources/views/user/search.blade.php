@extends('user.layouts.app')
@section('content')
<div class="sidelefrig_right">
	
	<div class="tablecovrbox">
		<div class="profmnubox">
			<a href="{{route('user.profile',['id'=>Auth::user()->id])}}"><img src="{{ asset('public/assets/user/svg/user.svg') }}" alt=""></a>
		</div>
		<table id="dataTable" class="display responsive nowrap" width="100%">
		    <thead>
	            <tr>
	                <th>Name</th>
	                <th>VFX Group</th>
	                <th>VFX type</th>
	                <th>Tags</th>
	                <th>Action</th>
	            </tr>
	        </thead>
	        <tbody>
	        	
	        	
	        
	            			            
	        </tbody>			       
		</table>
	<!-- 	<a href="{{route('user.vfxtypeform')}}" class="addnewbtn">Add New</a> -->
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	var table   =  $('#dataTable').DataTable({
			  
	            ajax: {
	                          type: "post",
	                          url: "{{route('user.searchlist') }}",
	                          data: function ( d ) {
	                              d._token = "{{ csrf_token() }}";
	                          }
	                           
	                      },
	                      columns:[
	                       
	                         { data:'search_name',name:'search_name'},
	                         { data:'vfx_group',name:'vfx_group'},
	                         { data:'vfx_type',name:'vfx_type'},
	                         { data:'tags',name:'tags'},
	                         { data:'action',name:'action'},
	                       ]
	            });

	//delete search
	table.on( 'draw', function () {
	  	jQuery('[data-toggle=confirmation]').confirmation({
	  	  rootSelector: '[data-toggle=confirmation]',
	  	  	onConfirm: function(value) {
	  	  		
	  	  		id = $(this).data('id');
	  	  		$.ajax({
	  	  		    url: '{{route("user.deletesearch")}}',
	  	  		    method: 'POST',
	  	  		    data: { 
	  	  		    	    id:id,
	  	  		    	    _token: "{{ csrf_token() }}"
	  	  		    	  }, 
	  	  		    }).done( function( result ){
	  	  		  
	  	  		   	toastr.success(result.message);
	  	  		   	table.ajax.reload();
	  	  		});
	  	  	},
	  	  	
	  	});
	} );
</script>	
@endsection