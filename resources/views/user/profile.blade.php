@extends('user.layouts.app')
@section('content')
<div class="sidelefrig_right">
	<div class="tagserchcover">

	<div class="profmnubox">
		<a href="{{route('user.profile',['id'=>Auth::user()->id])}}"><img src="{{ asset('public/assets/user/svg/user.svg') }}" alt=""></a>
	</div>
	<div class="allformcover">
		<div class="noteboxcover" style="margin-bottom: 10px;">
			<h3>Update Profile</h3>
			
		</div>
		<form id="updateuserform" method="post">
			@csrf
			<div class="formdetacover">
				<div class="inputselect">
					<div class="width50">
						<div class="form-group">
							<input type="hidden" name="id" value="{{$data->id}}">
							<label for="vfxtype">Name</label>
							<input type="text" name="name" id="" class="form-control" placeholder="Enter Name" value="{{isset($data->name) && $data->name ? $data->name : '' }}">
						</div> 
					</div> 
					<div class="width50">
						<div class="form-group">
							<label for="vfxtype">User Name</label>
							<input type="text" name="user_name" id="" class="form-control" placeholder="Enter User Name" value="{{isset($data->user_name) && $data->user_name ? $data->user_name : '' }}" readonly>
						</div>
					</div>
				</div>
				
				<div class="inputselect">
					<div class="width50">
						<div class="form-group">
							<label for="vfxtype">Role</label>
							<select class="js-example-basic-single updatescenerole" name="role_id" >
								<option></option>
					              @foreach($sceneroles as $role)
						             <option value="{{$role->id}}" 
							           @if($role->id == $data->role_id) 
                          	             selected                    
						              @endif
							>{{$role->role}}</option>
					@endforeach
				</select>
						</div>
					</div> 
					<div class="width50">
						<div class="form-group">
							<label for="vfxtype">Email</label>
							<input type="text" name="email" id="" class="form-control" placeholder="Enter Email" value="{{isset($data->email) && $data->email ? $data->email : '' }}" readonly>
						</div>
					</div>
				</div>
				<div class="inputselect">
					<div class="width50">
						<div class="form-group">
							<label for="vfxtype">Short Bio</label>
							
							<textarea name="short_bio" class="form-control">{{isset($data->short_bio) && $data->short_bio ? $data->short_bio : '' }}</textarea>
						</div>
					</div> 
					<div class="width50">
					</div>
				</div>
				

			
				<div class="width100">
					<div class="addbtnboxset">
						<!-- <a  class="btn btn-default" onclick="UpdateIndustry()">Update</a>  --> 
						<input type="submit" value="Update" class="btnsetform">
						<!-- <a href="javascript:void(0);" onclick="updateUser(event);">Update</a> -->
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="allformcover">
		<div class="noteboxcover" style="margin-bottom: 10px;">
			<h3>Change Password</h3>
			
		</div>
		<form id="changepasswordform"  method="post">
			@csrf
			<div class="formdetacover">
				<div class="inputselect">
					<div class="width50">
						<div class="form-group">
							<label for="vfxtype">Old Password</label>
								<input type="hidden" name="id" value="{{$data->id}}">
							<input type="password" name="old_password" id="" class="form-control" placeholder="Enter old Password" value="">
						</div> 
					</div> 
					
				</div>
				
				<div class="inputselect">
					<div class="width50">
						<label for="vfxtype">New Password</label>
						<input type="password" name="new_password" id="newpassword" class="form-control" placeholder="Enter User Name" value="">
					</div> 
				</div>
				<div class="inputselect">
					<div class="width50">
						<div class="form-group">
							<label for="vfxtype">Confirm Password</label>
							<input type="password" name="confirm_password" id="" class="form-control" placeholder="Enter Confirm Password" value="">
						</div>
					</div> 
					
				</div>
				

			
				<div class="width100">
					<div class="addbtnboxset">
						<!-- <a  class="btn btn-default" onclick="UpdateIndustry()">Update</a>  -->
						<input type="submit" value="Change Password" class="btnsetform"> 
						<!-- <a href="javascript:void(0);" onclick="addVFXType();">Change Password</a> -->
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>

@endsection
@section('js')
<script type="text/javascript">
	
	//Update User Profile

	$('#updateuserform').validate({ // initialize the plugin
	       rules: {
	           name: {
	               required:true,
	             
	           },
	           user_name: {
	               required:true,
	              
	           },
	           email:{
	           		required:true,
	           		email:true
	           },
	           phone:{
	           		required:true,
	           },
	          

	       },
	       submitHandler: function(form) {
	       		
	         		var formData = $('#updateuserform').serialize();
	         		 //var formData = $( "#category_form" ).serializeArray();
	         		     
	         		 $.ajaxSetup({
	         		   headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
	         		 });
	         		 $.ajax({
	         		     type:'POST',
	         		     url:'{{route("user.profile.update")}}',
	         		   
	         		     data: formData,

	         		     beforeSend:function(){},
	         		     success:function(data) {
	         		     	 toastr.success(data.message);
	         		     },
	         		 });
	        }
	   });

	//change password
	$('#changepasswordform').validate({ // initialize the plugin
	       rules: {
	           old_password: {
	               required:true,
	               minlength: 6,
	             
	           },
	           new_password: {
	               required:true,
	               minlength: 6,
	           },
	           confirm_password: {
	               required:true,
	               equalTo : "#newpassword",
	               minlength: 6, 

	           },

	           
	          

	       },
	       submitHandler: function(form) {
	       		
	         		var formData = $('#changepasswordform').serialize();
	         		 //var formData = $( "#category_form" ).serializeArray();
	         		     
	         		 $.ajaxSetup({
	         		   headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
	         		 });
	         		 $.ajax({
	         		     type:'POST',
	         		     url:'{{route("user.profile.changepassword")}}',
	         		   
	         		     data: formData,

	         		     beforeSend:function(){},
	         		     success:function(data) {
	         		     	if(data.status == false){
	         		     	 toastr.error(data.message);
	         		     	}
	         		     	else{
	         		     		toastr.success(data.message);
	         		     		 $('#changepasswordform')[0].reset();
	         		     	}
	         		     },
	         		 });
	        }
	   });

	//select role
	$(document).ready(function() {
	    $('.js-example-basic-single').select2({		
	       placeholder: 'Select Role',
	       
	     });
	});


	
</script>
@endsection	    