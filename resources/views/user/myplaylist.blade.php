<!DOCTYPE html>
<html lang="en">
<head>
	<title></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content=""/>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ asset('public/assets/user/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{ asset('public/assets/user/css/jquery.dataTables.min.css')}}">
	<link rel="stylesheet" href="{{ asset('public/assets/user/font-awesome/css/font-awesome.min.css')}}">
	<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="{{ asset('public/assets/user/css/styles.css')}}">
	<link rel="stylesheet" href="{{ asset('public/assets/user/css/toastr.min.css')}}">
	<link rel="stylesheet" href="https://kenwheeler.github.io/slick/slick/slick.css">
	<link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.min.css" rel="stylesheet"></link>
  
</head>
<body>

	<div class="sidelefrigcover container">
			


	
	<div class="tablecovrbox">
			<input type="hidden" name="myplaylisturl" value="{{request()->route('url')}}">
		<table id="dataTable" class="display responsive nowrap" width="100%">
		    <thead>
	            <tr>
			
	              
	                
	                <th>Scene</th>
	                <th>VFX Group</th>
	                <th>VFX type</th>
	                <th>Project</th>
	                <th>Year</th>
	                <th>Type</th>
	                <th>VFX Supe</th>
	                <th>Rating</th>
	                <th>Link</th>
	                <th>Tags</th>
	            </tr>
	        </thead>
	        <tbody>
	        	
	        	
	                     
	            			            
	        </tbody>			       
		</table>
		
	
</div>


</div>
<script type="text/javascript" src="{{ asset('public/assets/user/js/jquery.js')}}"></script>
<script src="{{ asset('public/assets/user/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('public/assets/user/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('public/assets/user/js/bootstrap-confirmation.js')}}"></script>
<script src="{{ asset('public/assets/user/js/toastr.min.js')}}"></script>
<script src="https://kenwheeler.github.io/slick/slick/slick.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js" integrity="sha256-sPB0F50YUDK0otDnsfNHawYmA5M0pjjUf4TvRJkGFrI=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js" integrity="sha256-vb+6VObiUIaoRuSusdLRWtXs/ewuz62LgVXg2f1ZXGo=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.min.js"></script>
<script>
	$(document).ready(function() {
	    $('#example').DataTable();
	} );
</script>
<script type="text/javascript">
	


	var table   =  $('#dataTable').DataTable({
			   
	            ajax: {
	                          type: "get",
	                          url: "{{route('user.myplaylist') }}",
	                          data: function ( d ) {
	                              d._token = "{{ csrf_token() }}",
	                              d.url = "{{request()->route('url')}}"
	                          },
	                          responsive: true,

	                           
	                      },
	                      columns:[
	                       
	                        
	                         { data:'scene',name:'scenes'},
	                         { data:'vfxgroup.name',name:'vfxgroup.name',"defaultContent":""},
	                         { data:'vfxtype.type',name:'vfxtype.type',"defaultContent":""},
	                         { data:'project',name:'project'},
	                         { data:'year',name:'year'},
	                         { data:'projecttype.type',name:'projecttype.type',"defaultContent":""},
	                         { data:'vfx_supe',name:'vfx_supe'},
	                         { data:'rating',name:'rating'},
	                         { data:'link',name:'link'},
	                         { data:'tags',name:'tags'},
	                       
	                        
	                        
	                       ]
	            });

	

	
	   

</script>
</body>
</html>