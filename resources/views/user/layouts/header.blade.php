<head>
	<title></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content=""/>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ asset('public/assets/user/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{ asset('public/assets/user/css/jquery.dataTables.min.css')}}">
	<link rel="stylesheet" href="{{ asset('public/assets/user/font-awesome/css/font-awesome.min.css')}}">
	<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="{{ asset('public/assets/user/css/styles.css')}}">
	<link rel="stylesheet" href="{{ asset('public/assets/user/css/toastr.min.css')}}">
	<link rel="stylesheet" href="https://kenwheeler.github.io/slick/slick/slick.css">
	<link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.min.css" rel="stylesheet"></link>
  
</head>