<!DOCTYPE html>
<html lang="en">
@include('user.layouts.header')
<body>

	<div class="sidelefrigcover">
		<div class="sidelefrig_left">
			@include('user.layouts.sidebar')
		</div>
		@yield('content')
	</div>

	@include('user.layouts.js')
	@yield('js')

</body>
</html>