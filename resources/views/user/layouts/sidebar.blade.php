<ul>
	<li class="logobox">
		<a href="{{route('user.home')}}">
			<img src="{{ asset('public/assets/user/images/Logo_image.png') }}" alt="">
		</a>
	</li>
	<li><a href="{{route('user.home')}}"><span>Home</span></a></li>
	<li><a href="{{route('user.playlist.index')}}"><span>Play List</span></a></li>
	<li><a href="{{route('user.search')}}"><span>Search List</span></a></li>
	<li><a class="dropdown-item" href="{{ route('logout') }}"
   onclick="event.preventDefault();
                 document.getElementById('logout-form').submit();">
    <span>{{ __('Logout') }}</span>
   </a></li>
</ul>


<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
<!-- <a href="javascript:void(0);" class="setbtnicon"><img src="{{ asset('public/assets/user/svg/tools.svg') }}" alt=""></a> -->
