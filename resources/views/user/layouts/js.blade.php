<script type="text/javascript" src="{{ asset('public/assets/user/js/jquery.js')}}"></script>
<script src="{{ asset('public/assets/user/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('public/assets/user/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('public/assets/user/js/bootstrap-confirmation.js')}}"></script>
<script src="{{ asset('public/assets/user/js/toastr.min.js')}}"></script>
<script src="https://kenwheeler.github.io/slick/slick/slick.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js" integrity="sha256-sPB0F50YUDK0otDnsfNHawYmA5M0pjjUf4TvRJkGFrI=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js" integrity="sha256-vb+6VObiUIaoRuSusdLRWtXs/ewuz62LgVXg2f1ZXGo=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.min.js"></script>
<script>
	$(document).ready(function() {
	    $('#example').DataTable();
	} );
</script>
  